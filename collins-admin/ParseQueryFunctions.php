<?php 

// CONFIGURATION
require_once 'parse-config/vendor/autoload.php';

// CLASSES
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseGeoPoint;
// use Parse/HttpClients/ParseCurlHttpClient.php;

parse_connect();

// CONNECT TO THE PARSE SERVER
function parse_connect(){
	try{
		Parse\ParseClient::initialize('collins-appId', '', 'collins-masterKey');
		// Parse\ParseClient::setServerURL('http://collinsmobile.herokuapp.com', 'parse');
		Parse\ParseClient::setServerURL('https://famousbrands-ph.com:1337', 'parse');
		// Parse\ParseClient::setServerURL('https://famousbrands-ph.com:1337/parse');
		// Parse\ParseClient::setServerURL('https://famousbrands-ph.com','parse');
		// Parse\ParseClient::setHttpClient(new ParseCurlHttpClient());

	} catch (ParseException $e) {
		echo $e->getMessage();
		// var_dump($e->getMessage());
	}
}

// healthCheck();
// CHECK CONNECTION TO PARSE SERVER
function healthCheck(){

	$health = Parse\ParseClient::getServerHealth();
	// if($health['status'] === 200) {
 //    	echo "--------------------------------------------good";
	// }else{
	// 	echo "----------------------------------------------bad";
	// }
	echo "<pre>";
	var_dump($health);
	echo "</pre>";
}

// ======================================================USERS===================================
// DISPLAY USERS DATA
function displayUserTable() {
	$query = new ParseQuery("_User");
	$query->descending("createdAt");

	// LOOP TO DISPLAY OBJECT CONTENT
	echo"<thead class='bg-primary'>
			<tr>
			<th></th>
			<th data-orderable='false'>PROFILE</th>
			<th>NAME</th>
			<th>USERNAME</th>
			<th>CONTACT NUMBER</th>
			<th>ADDRESS</th>
			<th>POINTS</th>
			<th data-orderable='false'>ACTIONS</th>
			</tr>
		</thead>
		<tfoot class='bg-primary'>
			<tr>
			<th></th>
			<th>PROFILE</th>
			<th>NAME</th>
			<th>USERNAME</th>
			<th>CONTACT NUMBER</th>
			<th>ADDRESS</th>
			<th>POINTS</th>
			<th>ACTIONS</th>
			</tr>
		</tfoot>
		<tbody>";

	$count = $query->find();
	for ($i = 0; $i < count($count); $i++) {
		$obj=$count[$i];
		
		// $dateCreated=$obj->getCreatedAt()->format('M d Y - H:i a');
		// $dateUpdated=$obj->getUpdatedAt()->format('M d Y - H:i a');
		
		$type=$obj->get('type');
		if($type=='facebook'){
			$icon='<i class="fa fa-facebook-square" style="font-size:20px;color:#6B7FB2"></i>&nbsp;';
		}else{
			$icon='';
		}

		$profile=$obj->get('profilePicture');
		$facebook=$obj->get('facebookImage');
	
		if($profile) {
			$picture = '<img width="50" height="50" src="'.$profile->getURL().'">';
		}else if($facebook){//if no profile
			$picture = '<img width="50" height="50" src="'.$facebook.'">';
		}else{ // if no picture
			$picture = '<img width="50" height="50" src="http://collinsmobile.herokuapp.com/parse/files/collins-appId/4e99cd1e474d39635dc92cd4b23c0418_default_profile.jpg" alt="no image" >';
		}

		$getPoints = $obj->get('points');
		if($getPoints){
			$points=$getPoints;
		}else{
			$points="0";
		}
	
		echo'
				<tr>
				<td></td>
				<td>'.$picture.'</td>
				<td>'.$obj->get('firstName')." ".$obj->get('lastName').'</td>
				<td>'.$icon.$obj->get('username').'</td>
				<td>'.$obj->get('contactNumber').'</td>
				<td>'.$obj->get('address').'</td>
				<td>'.$points.'</td>';
		echo'<td class="two-action">
				<button viewUserAttr="'.$obj->getObjectId().'" viewUserPointsAttr="'.$points.'" class="btn btn-primary edit-user-points" >
					<span data-toggle="tooltip" data-placement="top" title="Edit points" class="glyphicon glyphicon-certificate"></span> 
				</button>
				<button viewUserAttr="'.$obj->getObjectId().'" class="btn btn-warning view-user-details" >
					<span data-toggle="tooltip" data-placement="top" title="User\'s details" class="glyphicon glyphicon-list-alt"></span> 
				</button>
				<button viewUserHistoryAttr="'.$obj->getObjectId().'" class="btn btn-success view-user-history">
					<span data-toggle="tooltip" data-placement="top" title="User\'s history" class="fa fa-history"></span>
				</button>
				<button viewUserProductsAttr="'.$obj->getObjectId().'" class="btn btn-info view-user-products">
					<span data-toggle="tooltip" data-placement="top" title="User\'s products" class="fa fa-cube"></span>
				</button>
			</td>
			</tr>';
	}
	echo "</tbody>";
}

// DISPLAY USER HISTORY
function viewUserHistoryTable($id){
	// $pointer = new ParseQuery("_User");
	$query = new ParseQuery("History");
	$query->descending("date");
	// GET THE parent user 
	$query->equalTo("user",['__type' => "Pointer", 'className'=> "_User", 'objectId' => $id]);
	$result=$query->find();

	echo "<table class='table table-bordered table-striped table-hover'>
			<tr>
				<th>DATE</th>
				<th>POINTS</th>
				<th>STATUS</th>
				<th>TYPE</th>
				<th>TRANSACTION</th>
			</tr>";

	if(count($result)==0){
		echo "<tr class='text-center'>
				<td colspan='5'>No history found</td>
			</tr>";
		
	}else{
		echo "<h5>Total items: <span class='badge'>".count($result)."</span></h5>";
		for ($i=0; $i < count($result); $i++) { 
			$obj = $result[$i];
			$date=$obj->get("date")->format('M d Y - H:i a');
			
			$transaction=$obj->get("transaction");
			if($transaction=="Deducted by admin" || $transaction=="Added by admin"){
				$transaction="<strong><i>".$transaction."</i></strong>";
			}
			echo "<tr>
					<td>".$date."</td>
					<td>".$obj->get("points")."</td>
					<td>".$obj->get("status")."</td>
					<td>".$obj->get("type")."</td>
					<td>".$transaction."</td>
				</tr>";
		}
		
	}
	echo "</table>";
}	

// updateUserPoints("tgIZeRZN1P","111");
function updateUserPoints($id,$points,$type) {

	$send = ParseCloud::run("manipulateUserPoints", ["user" => $id, "points" => (int)$points, "type" => $type]);
	// call cloud code function that will add or deduct points
	// type: add/delete
	if($send=="Points successfully saved"){
		$result['status']='success';
	}
	else{
		$result['status']='failed';
	}
	return $result;
	// var_dump($result);
}


// DISPLAY USER PRODUCTS
function viewUserProductsTable($id){
	// $pointer = new ParseQuery("_User");
	$query = new ParseQuery("Product");
	// GET THE parent user 
	$query->includeKey("store");
	$query->includeKey("branch");
	$query->includeKey("brand");
	$query->includeKey("model");
	$query->equalTo("user",['__type' => "Pointer", 'className'=> "_User", 'objectId' => $id]);
	$query->descending("createdAt");
	$result=$query->find();

	echo "<table class='table table-bordered table-striped table-hover'>
			<tr>
				<th>PURCHASE DATE</th>
				<th>INVOICE</th>
				<th>STORE NAME</th>
				<th>BRANCH NAME</th>
				<th>BRAND NAME</th>
				<th>MODEL NAME</th>
				<th>TRANSACTION NUMBER</th>
				<th>SERIAL NUMBER</th>
				<th>QUANTITY</th>
				<th>STATUS</th>
				<th>CATEGORY</th>
			</tr>";

	if(count($result)==0){
		echo "<tr class='text-center'>
				<td colspan='11'>No registered products found</td>
			</tr>";
		
	}else{
		echo "<h5>Total items: <span class='badge'>".count($result)."</span></h5>";
		for ($i=0; $i < count($result); $i++) { 
			$obj = $result[$i];
			$date=$obj->get("purchaseDate")->format('M d Y - H:i a');
			$image=$obj->get("invoice");

			if($image){
				$invoice=$image->getURL();
			}
			else{
				$invoice='';
			}
			echo "<tr>
					<td>".$date."</td>
					<td> <img src='".$invoice."' alt='no image' width='50' height='50'></td>
					<td>".$obj->get("store")->get("name")."</td>
					<td>".$obj->get("branch")->get("name")."</td>
					<td>".$obj->get("brand")->get("name")."</td>
					<td>".$obj->get("model")->get("name")."</td>
					<td>".$obj->get("transactionNumber")."</td>
					<td>".$obj->get("serialNumber")."</td>
					<td>".$obj->get("quantity")."</td>
					<td>".$obj->get("status")."</td>
					<td>".$obj->get("category")."</td>
				</tr>";
		}
	}
	echo "</table>";
}

// VIEW SELECTED USER
function viewUser($objectId) {
	$query = new ParseQuery("_User");
	$query->equalTo("objectId", $objectId);
	$results = $query->find();
	
	for ($i = 0; $i < count($results); $i++) {
		$object = $results[$i];
  		$dateCreated=$object->getCreatedAt()->format('M d Y - H:i a');
		$dateUpdated=$object->getUpdatedAt()->format('M d Y - H:i a');
		
		$profile=$object->get('profilePicture');
		$facebook=$object->get('facebookImage');
	
		if($profile) {
			$image = $profile->getURL();
		}else if($facebook){//if no profile
			$image = $facebook;
		}else{ // if no picture
			$image = "http://collinsmobile.herokuapp.com/parse/files/collins-appId/4e99cd1e474d39635dc92cd4b23c0418_default_profile.jpg";
		}

  	  	$data['dateCreated']=$dateCreated;
  	  	$data['dateUpdated']=$dateUpdated;
  	  	$data['image']=$image;
  	  	$data['username']=$object->get('username');  	

  	  	$getFname=$object->get('firstName');
  	  	if($getFname){
  	  		$fname=$getFname;
  	  	}else{
  	  		$fname="";
  	  	}
  	  	$data['firstname']=$fname;

  	  	$getLname=$object->get('lastName');
  	  	if($getLname){
  	  		$lname=$getLname;
  	  	}else{
  	  		$lname="";
  	  	}
  	  	$data['lastname']=$lname;

  	  	$getBirth=$object->get('birthMonth');
  	  	if($getBirth){
  	  		$birth=$getBirth;
  	  	}else{
  	  		$birth="";
  	  	}
  	  	$data['birth']=$birth;

  	  	$getAge=$object->get('ageBracket');
  	  	if($getAge){
  	  		$age=$getAge;
  	  	}else{
  	  		$age="";
  	  	}
  	  	$data['age']=$age;

  	  	$getGender=$object->get('gender');
  	  	if($getGender){
  	  		$gender=$getGender;
  	  	}else{
  	  		$gender="";
  	  	}
  	  	$data['gender']=$gender;
  	  	// $getEmail=$object->get('email');
  	  	// if ($getEmail) {
  	  		$data['email']="";
  	  		// $data['email']=$object->get('email');
  	  	// }else{
  	  	// 	$data['email']="";
  	  	// }
  	  	$getAddress=$object->get('address');
  	  	if($getAddress){
  	  		$address=$getAddress;
  	  	}else{
  	  		$address="";
  	  	}
  	  	$data['address']=$address; 

  	  	$getProvince=$object->get('province');
  	  	if($getProvince){
  	  		$province=$getProvince;
  	  	}else{
  	  		$province="";
  	  	}
  	  	$data['province']=$province;

  	  	$getCity=$object->get('city');
  	  	if($getCity){
  	  		$city=$getCity;
  	  	}else{
  	  		$city="";
  	  	}	
  	  	$data['city']=$city;

  	  	$getBarangay=$object->get('barangay');
  	  	if($getBarangay){
  	  		$barangay=$getBarangay;
  	  	}else{
  	  		$barangay="";
  	  	}
  	  	$data['barangay']=$barangay;

  	  	$getPoints=$object->get('points');
  	  	if($getPoints){
  	  		$points=$getPoints;
  	  	}else{
  	  		$points="0";
  	  	}
  	  	$data['points']=$points;

  	  	$getNumber=$object->get('contactNumber');
  	  	if($getNumber){
  	  		$number=$getNumber;
  	  	}else{
  	  		$number="";
  	  	}
  	  	$data['number']=$number;

  	  	$getNumItems=$object->get('numberOfItems');
  	  	if($getNumItems){
  	  		$numItems=$getNumItems;
  	  	}else{
  	  		$numItems="0";
  	  	}
  	  	$data['items']=$numItems;
  	  	
  	  	$getRedeemPoints=$object->get('pendingRedeemPoints');
  	  	if($getRedeemPoints){
  	  		$redeemPoints=$getRedeemPoints;
  	  	}else{
  	  		$redeemPoints="0";
  	  	}
  	  	$data['redeem']=$redeemPoints;

	}
	return $data;
}


// ===============================================BRAND=================================
// DISPLAY BRAND
function displayBrandTable() {
	$query = new ParseQuery("Brand");
	$query->descending("createdAt");
	$query->equalTo("isDeleted",false);
	$result=$query->find();
	// LOOP TO DISPLAY OBJECT CONTENT
	echo"<thead class='bg-primary'>
		<tr>
			<th></th>
			<th>BRAND NAME</th>
			<th>CATEGORIES</th>
			<th data-orderable='false'>ACTIONS</th>
		</tr>
		</thead>
		<tbody>";
	for ($i=0; $i < count($result); $i++) { 
		$obj=$result[$i];

		$categories_array=$obj->get('categories');
		$dateCreated=$obj->getCreatedAt()->format('M d Y - H:i a');
		$dateUpdated=$obj->getUpdatedAt()->format('M d Y - H:i a');

		echo'<tr>
				<td></td>
			
				<td>'.$obj->get('name').'</td><td>';
			// foreach( $categories_array as $newArray ){
			// 	$category.= $newArray.', ';
			// }
 			echo implode($categories_array,',');
		echo'</td><td class="one-action">
			<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success update-brand">
				<span class="glyphicon glyphicon-edit" ></span>
			</button>&nbsp;
			<button deleteAttr="'.$obj->getObjectId().'" class="btn btn-danger delete-brand">
				<span class="glyphicon glyphicon-trash" ></span>
			</button>
			</td>
			</tr>';
	}
	echo "</tbody>";
}

// CREATE BRAND
// pointer();
// function pointer($id) {
// 	// COMPARE TO THE DATABASE IF ALREADY EXIST
// 	$search = new ParseQuery("Brand");
// 	$search->equalTo("objectId", $id);
// 	$count = $search->find();
// 	// COUNT THE NUMBER OF FOUND OBJECT
// 	if(count($count)==1){
// 		$result['status']='found';
// 	}else{
// 		$result['status']='success';
// 	}
// 	return $result;
// }
// create_new_brand("asaaa");
function addBrand($brand,$category) {
	// COMPARE TO THE DATABASE IF ALREADY EXIST
	$newCategory=array_map('trim',explode(',',$category));

	$search = new ParseQuery("Brand");
	$search->equalTo("name", $brand);
	$count = $search->find();
	// COUNT THE NUMBER OF FOUND OBJECT
	if(count($count)==1){
		$result['status']='found';
	}else{
		$query = new ParseObject("Brand");
		$query->set("name", $brand);
		$query->addUnique("categories", $newCategory);
		$query->set("isDeleted",false);
		// $query->setArray("categories", ["sdw","awdw"]);
		$query->save();
		$result['status']='success';
	}
	return $result;
}

// VIEW SELECTED BRAND
function viewBrand($id) {
	$query = new ParseQuery("Brand");
	$query->equalTo("objectId", $id);
	$count = $query->find();
	
	for ($i = 0; $i < count($count); $i++) {
		$object = $count[$i];
  	  	 
  	  	$categories_array=$object->get('categories');
  	  	foreach( $categories_array as $newArray ){
  	  		$cat.= $newArray.',';
 		}	
 		$result['name']=$object->get('name');
  	  	$result['category']=rtrim($cat,', '); 	
	}
	return $result;
}

// UPDATE BRAND
function updateBrand($id,$name,$category) {
	$newCategory=array_map('trim',explode(',',$category));
	$query = new ParseQuery("Brand");
	$query->equalTo("name", $name);
	$count = $query->find();
	// FIND SAME NAME
	if(count($count)==1){
		for ($i = 0; $i < count($count); $i++) {
			// GET THE OBJECT ID OF THIS OBJECT
	  		$foundId=$count[$i]->getObjectId();
	  		// NO SAME NAME FOUND
			if($foundId==$id){
				$count[$i]->set("name", $name);
				$count[$i]->addUnique("categories", $newCategory);
				$count[$i]->save();
				$result['status']='success';
			}
			// NAME ALREADY EXIST IN THE DATABASE
			else{
				$result['status']='exist';
			}
		}
	}else{
		$query = new ParseQuery("Brand");
		$query->equalTo("objectId", $id);
		$count = $query->find();
		for ($i = 0; $i < count($count); $i++) {
			$count[$i]->set("name", $name);
			$count[$i]->save();
			$result['status']='success';
		}
	}
	return $result;
}


// DELETE BRAND
function deleteBrand($id) {
	// CHECK ASSOCIATED BRAND PRODUCT
	$checkActive = new ParseQuery("BrandProduct");
	$checkActive->equalTo("brand",['__type' => "Pointer", 'className'=> "Brand", 'objectId' => $id]);
	$countActive=$checkActive->find();
	// DELETE IF NO ASSOCIATED CHILD CLASS
	if(count($countActive)==0){
		$query = new ParseQuery("Brand");
		$query->equalTo("objectId",$id);
		$count=$query->find();
		for ($i=0; $i <count($count); $i++) { 
			$obj=$count[$i];
			$obj->set("isDeleted",true);
			$obj->save();
			$result['status']='success';
		}
	// ACTIVE FOUND
	}else{
		$result['status']='found';
	}
return $result;
}


// =============================================BRAND PRODUCT=====================
// DISPLAY BRAND PRODUCT
function displayBrandProductTable() {
	$query = new ParseQuery("BrandProduct");
	$query->includeKey("brand"); // GET PARENT OBJECT FROM POINTER
	$query->descending("createdAt");
	$query->equalTo("isDeleted",false);
	// LOOP TO DISPLAY OBJECT CONTENT
	echo"<thead class='bg-primary'>
			<tr>
			<th></th>
			<th data-orderable='false'>IMAGE</th>
			<th>BRAND PRODUCT NAME</th>
			<th>BRAND NAME</th>
			<th>ITEM CODE</th>
			<th>POINTS</th>
			<th>PRICE</th>
			<th>CATEGORY</th>
			<th data-orderable='false'>ACTIONS</th>
			</tr>
		</thead>
		<tbody>";
	$count = $query->find();
	for ($i = 0; $i < count($count); $i++) {
		$obj=$count[$i];

		$categories_array=$obj->get('categories');
		$dateCreated=$obj->getCreatedAt()->format('M d Y - H:i a');
		$dateUpdated=$obj->getUpdatedAt()->format('M d Y - H:i a');

		$image=$obj->get('images');
		
		$thumbnail= (array)$image;
		
		if($image){
			$getThumbnail = "<img src='".$thumbnail['thumbnail'] ."' width='100' height='100'>";
		}else{
			$getThumbnail = "<img src='' alt='no image'>";
		}

		echo'<tr>
				<td></td>
				<td>'.$getThumbnail.'</td>
				<td>'.$obj->get('name').'</td>
				<td>'.$obj->get('brand')->get("name").'</td>
				<td>'.$obj->get('itemCode').'</td>
				<td>'.$obj->get('points').'</td>
				<td>'.$obj->get('price').'</td>
				<td>'.$obj->get('category').'</td>
				<td>
				<button viewAttr="'.$obj->getObjectId().'" class="btn btn-warning view-brand-product">
					<span class="glyphicon glyphicon-eye-open" ></span>
				</button>
				<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success update-brand-product">
					<span class="glyphicon glyphicon-edit" ></span>
				</button>
				<button deleteAttr="'.$obj->getObjectId().'" class="btn btn-danger delete-brand-product">
					<span class="glyphicon glyphicon-trash" ></span>
				</button>
				</td>
				</tr>';
	}
	echo "</tbody>";
}

// Filter brand name
function filterBrandName(){
	$query = new ParseQuery("Brand");
	$query->notEqualTo("isDeleted", true);
	$count=$query->find();
	echo '<option value="">All Brand Name</option>';
	for ($i=0; $i <count($count) ; $i++) {
		$obj=$count[$i];
		echo "<option value='".$obj->get('name')."''>".$obj->get("name")."</option>";
	}
}

// VIEW SELECTED BRAND PRODUCT
function viewBrandProduct($objectId) {
	$query = new ParseQuery("BrandProduct");
	$query->includeKey("brand");
	$query->equalTo("objectId", $objectId);
	$results = $query->find();
	
	for ($i = 0; $i < count($results); $i++) {
		$object = $results[$i];
  		$dateCreated=$object->getCreatedAt()->format('M d Y - H:i a');
		$dateUpdated=$object->getUpdatedAt()->format('M d Y - H:i a');
		
		$image=$object->get('images');
		
		$thumbnail= (array)$image;
		
		if($image){
			$data['image'] = $thumbnail['normal'];
		}else{
			$data['image'] = "";
		}

  	  	// $data['objectId']=$object->getObjectId();
  	  	$data['dateCreated']=$dateCreated;
  	  	$data['dateUpdated']=$dateUpdated;
  	  	
  	  	$data['item']=$object->get('itemCode');
  	  	$data['name']=$object->get('name');
  	  	$data['description']=$object->get('description');
  	  	$data['colors']=$object->get('colors');
  	  	$data['brand']=$object->get('brand')->get("name");
  	  	$data['category']=$object->get('category');
  	  	$data['price']=$object->get('price');
  	  	$data['points']=$object->get('points');
  	  	$data['quantity']=$object->get('isQuantity');
	}
	return $data;

}

// BRAND NAME fOR UPDATE
function displayBrandNameUpdate($id,$category) {

	$brandProd = new ParseQuery("BrandProduct");
	$brandProd->equalTo("objectId", $id);
	$brandProd->includeKey("brand");
	$brandCount=$brandProd->find();

	for ($i=0; $i < count($brandCount) ; $i++) { 
		$obj=$brandCount[$i];
		$brandId=$obj->get("brand")->getObjectId();
		$brandName=$obj->get("brand")->get("name");
	}

	$query = new ParseQuery("Brand");
	$query->notEqualTo("objectId",$brandId);
	$query->notEqualTo("isDeleted", true);
	$count=$query->find();

	echo "<select class='form-control update-select-brand'>";
	echo "<option value='".$brandId."'>".$brandName."</option>";
	for ($i=0; $i <count($count) ; $i++) {
		$obj=$count[$i];
		echo "<option value='".$obj->getObjectId()."''>".$obj->get("name")."</option>";
	}
	echo "</select>";

	// SELECT OPTION FOR BRAND CATEGORY
	echo "<label>Category:</label>";
	echo "<div class='load-category'>";
	displayBrandCategoryUpdate($brandId,$category);
	echo "</div>";

}

// WHEN SELECTED BRAND NAME IS SELECTED FOR UPDATE
function displayBrandCategoryUpdate($id,$category){

	$brandCat = new ParseQuery("Brand");
	$brandCat->equalTo("objectId", $id);
	$countBrandCat=$brandCat->find();
	echo "<select class='form-control update-select-category'>";
	// echo "<option value='".$category."'>".$category."</option>";
	for ($i=0; $i < count($countBrandCat); $i++) {
		$object=$countBrandCat[$i];

		// $getName=$object->get("name");

		// if($getName==$name){
			$getCategory=$object->get("categories");
				// IF CATEGORY IS NOT ON THE LIST
				if(!in_array($category, $getCategory)){
					echo "<option value='".$category."'>".$category."</option>";
					foreach ($getCategory as $option) {
						echo "<option value='".$option."'>".$option."</option>";
					}
				// IF CATEGORY IS ON THE LIST
				}else{
					foreach ($getCategory as $option) {
						if($category==$option){
							echo "<option value='".$option."' selected>".$option."</option>";
						}else{
							echo "<option value='".$option."'>".$option."</option>";
						}
					}
				}
		// }else {
		// 	echo "<option>wala</option>";
		// }

	}// for
	echo "</select>";
}

// BRAND NAME AND CATEGORY LIST
function displayBrandName() {
	$query = new ParseQuery("Brand");
	$query->notEqualTo("isDeleted", true);
	$count=$query->find();

	echo "<select class='form-control select-brand'>";
	echo "<option value='select'>Please select a brand</option>";
	for ($i=0; $i <count($count) ; $i++) {
		$obj=$count[$i];
		echo "<option value='".$obj->getObjectId()."''>".$obj->get("name")."</option>";
	}
	echo "</select>";
}

// WHEN SELECTED BRAND NAME IS SELECTED
function displayBrandCategory($id){
	$query = new ParseQuery("Brand");
	$query->equalTo("objectId", $id);
	$count=$query->find();

	echo "<select class='form-control select-category'>";
	echo "<option value='select'>Please select a category</option>";
	for ($i=0; $i < count($count); $i++) {
		$obj=$count[$i];
		$category=$obj->get("categories");
		foreach ($category as $option) {
			echo "<option value='".$option."'>".$option."</option>";
		} 
	}
	echo "</select>";
}

// ADD BRAND PRODUCT
function addBrandProduct($id,$name,$code,$description,$colors,$price,$category,$points,$quantity,$image) {
	$addColors=array_map('trim',explode(',',$colors));
	$numPoints = (int)$points;
	$isQuantity = (boolean)$quantity;
	$search = new ParseQuery("BrandProduct");
	$search->equalTo("name", $name);
	$count = $search->find();

	require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attachment_id = media_handle_upload( $image, 55 );
    // if ( is_wp_error( $attachment_id ) ) {
        // $image_url='error';
    // } else {
    // GET THUMBNAIL URL
	$thumbnail_attributes = wp_get_attachment_image_src( $attachment_id ,'thumbnail', false);
	$thumbnail_url = $thumbnail_attributes[0];
	// GET ORIGINAL IMAGE URL
    $normal_attributes = wp_get_attachment_image_src( $attachment_id ,'full', false);
	$normal_url = $normal_attributes[0];

    $image_object = ["thumbnail" => $thumbnail_url,	"normal" => $normal_url];

	if(count($count)==1){
		$result['status']='found';
	}else{
		$pointer = new ParseObject("Brand", $id); //TO WHAT ID

		$query = new ParseObject("BrandProduct");
		$query->set("name", $name);
		$query->set("itemCode", $code);
		$query->set("description", $description);
		$query->set("price", $price);
		$query->addUnique("colors", $addColors);
		$query->set("brand", $pointer);
		$query->set("category", $category);
		$query->set("points", $numPoints);
		$query->set("isQuantity", $isQuantity);
		$query->setAssociativeArray("images",$image_object);
		$query->set("isDeleted",false);
		$query->save();
		$result['status']='success';
	}
	return $result;
}

// UPDATE BRAND PRODUCT
function updateBrandProduct($id,$name,$code,$description,$colors,$price,$brand,$category,$points,$quantity,$image) {
	$addColors=array_map('trim',explode(',',$colors));
	$numPoints = (int)$points;
	$isQuantity = (boolean)$quantity;
	$pointer = new ParseObject("Brand", $brand);
	$query = new ParseQuery("BrandProduct");
	$query->equalTo("name", $name);
	$count = $query->find();

	require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attachment_id = media_handle_upload( $image, 55 );
	$thumbnail_attributes = wp_get_attachment_image_src( $attachment_id ,'thumbnail', false);
	$thumbnail_url = $thumbnail_attributes[0];
	// GET ORIGINAL IMAGE URL
    $normal_attributes = wp_get_attachment_image_src( $attachment_id ,'full', false);
	$normal_url = $normal_attributes[0];

    $image_object = ["thumbnail" => $thumbnail_url,	"normal" => $normal_url];
	// FIND SAME NAME
	if(count($count)==1){
		for ($i = 0; $i < count($count); $i++) {
	  		$foundId=$count[$i]->getObjectId();
	  		// NO SAME NAME FOUND
			if($foundId==$id){
				if($image=="no_image"){
					$count[$i]->set("name", $name);
					$count[$i]->set("itemCode",$code);
					$count[$i]->set("points",$numPoints);
					$count[$i]->addUnique("colors", $addColors);
					$count[$i]->set("brand",$pointer);
					$count[$i]->set("category",$category);
					$count[$i]->set("price",$price);
					$count[$i]->set("isQuantity",$isQuantity);
					$count[$i]->set("description",$description);
					$count[$i]->save();
					$result['status']='success';
				}else{
					$count[$i]->set("name", $name);
					$count[$i]->set("itemCode",$code);
					$count[$i]->set("points",$numPoints);
					$count[$i]->addUnique("colors", $addColors);
					$count[$i]->setAssociativeArray("images",$image_object);
					$count[$i]->set("brand",$pointer);
					$count[$i]->set("category",$category);
					$count[$i]->set("price",$price);
					$count[$i]->set("isQuantity",$isQuantity);
					$count[$i]->set("description",$description);
					$count[$i]->save();
					$result['status']='success';
				}
				
			}
			// NAME ALREADY EXIST IN THE DATABASE
			else{
				$result['status']='exist';
			}
		}
	}else{
		$pointer = new ParseObject("Brand", $brand);
		$query = new ParseQuery("BrandProduct");
		$query->equalTo("objectId", $id);
		$count = $query->find();
		for ($i = 0; $i < count($count); $i++) {
			if($image=="no_image"){
				$count[$i]->set("name", $name);
				$count[$i]->set("itemCode",$code);
				$count[$i]->set("points",$numPoints);
				$count[$i]->addUnique("colors", $addColors);
				$count[$i]->set("brand",$pointer);
				$count[$i]->set("category",$category);
				$count[$i]->set("price",$price);
				$count[$i]->set("isQuantity",$isQuantity);
				$count[$i]->set("description",$description);
				$count[$i]->save();
				$result['status']='success';
			}else{
				$count[$i]->set("name", $name);
				$count[$i]->set("itemCode",$code);
				$count[$i]->set("points",$numPoints);
				$count[$i]->addUnique("colors", $addColors);
				$count[$i]->setAssociativeArray("images",$image_object);
				$count[$i]->set("brand",$pointer);
				$count[$i]->set("category",$category);
				$count[$i]->set("price",$price);
				$count[$i]->set("isQuantity",$isQuantity);
				$count[$i]->set("description",$description);
				$count[$i]->save();
				$result['status']='success';
			}
			
		}
	}
	return $result;
}


// DELETE SINGlE BRAND PRODUCT
function deleteBrandProduct($id) {
	$query = new ParseQuery("BrandProduct");
	$query->equalTo("objectId",$id);
	$count=$query->find();

	for ($i=0; $i <count($count); $i++) { 
		$obj=$count[$i];
		$obj->set("isDeleted",true);
		$obj->save();
		$result['status']='success';
	}
	
	return $result;
}

// =============================================STORE======================
// DISPLAY STORE TABLE
function displayStoreTable() {
	$query = new ParseQuery("Store");
	$query->descending("createdAt");
	$query->equalTo("isDeleted", false);
	$result=$query->find();
	// LOOP TO DISPLAY OBJECT CONTENT
	echo"<thead class='bg-primary'>
			<tr>
			<th></th>
			
			<th>STORE NAME</th>
			<th data-orderable='false'>ACTIONS</th>
			</tr>
		</thead>
		<tbody>";
	for ($i=0; $i < count($result); $i++) { 
		$obj=$result[$i];

		$dateCreated=$obj->getCreatedAt()->format('M d Y - H:i a');
		$dateUpdated=$obj->getUpdatedAt()->format('M d Y - H:i a');
		echo'<tr>
				<td></td>
				
				<td>'.$obj->get('name').'</td>';
		echo'<td class="one-action">
			<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success update-store">
				<span class="glyphicon glyphicon-edit" ></span>
			</button>&nbsp;
			<button deleteAttr="'.$obj->getObjectId().'" class="btn btn-danger delete-store">
				<span class="glyphicon glyphicon-trash" ></span>
			</button>
			</td>
			</tr>';
	}
	echo "</tbody>";
}

// ADD STORE NAME
function addStore($store) {
	$search = new ParseQuery("Store");
	$search->equalTo("name", $store);
	// $search->notEqualTo("isDeleted" , true);
	$count = $search->find();

	if(count($count)==1){
		$result['status']='found';
	}else{
		$query = new ParseObject("Store");
		$query->set("name", $store);
		$query->set("isDeleted", false);
		$query->save();
		$result['status']='success';
	}
	return $result;
}

// VIEW STORE
function viewStore($id) {
	$query = new ParseQuery("Store");
	$query->equalTo("objectId", $id);
	$count = $query->find();
	
	for ($i = 0; $i < count($count); $i++) {
		$object = $count[$i];
  	  	$result['name']=$object->get('name'); 	
	}
	return $result;
}

// UPDATE STORE
function updateStore($id,$name) {
	$query = new ParseQuery("Store");
	$query->equalTo("name", $name);
	$count = $query->find();
	// FIND SAME NAME
	if(count($count)==1){
		for ($i = 0; $i < count($count); $i++) {
	  		$foundId=$count[$i]->getObjectId();
	  		// NO SAME NAME FOUND
			if($foundId==$id){
				$count[$i]->set("name", $name);
				$count[$i]->save();
				$result['status']='success';
			}
			// NAME ALREADY EXIST IN THE DATABASE
			else{
				$result['status']='exist';
			}
		}
	}else{
		$query = new ParseQuery("Store");
		$query->equalTo("objectId", $id);
		$count = $query->find();
		for ($i = 0; $i < count($count); $i++) {
			$count[$i]->set("name", $name);
			$count[$i]->save();
			$result['status']='success';
		}
	}
	return $result;
}

// DELETE STORE
function deleteStore($id) {
	// CHECK ASSOCIATED BRAND PRODUCT
	$checkActive = new ParseQuery("StoreBranch");
	$checkActive->equalTo("store",['__type' => "Pointer", 'className'=> "Store", 'objectId' => $id]);
	$countActive=$checkActive->find();
	// DELETE IF NO ASSOCIATED CHILD CLASS
	if(count($countActive)==0){
		$query = new ParseQuery("Store");
		$query->equalTo("objectId",$id);
		$count=$query->find();
		for ($i=0; $i <count($count); $i++) { 
			$obj=$count[$i];
			$obj->set("isDeleted",true);
			$obj->save();
			$result['status']='success';
		}
	// ACTIVE FOUND
	}else{
		$result['status']='found';
	}
return $result;
}

// =============================================STORE BRANCH======================
// DISPLAY STORE BRANCH TABLE
function displayStoreBranchTable() {
	$query = new ParseQuery("StoreBranch");
	$query->includeKey("store");
	$query->descending("createdAt");
	$query->equalTo("isDeleted", false);
	$result=$query->find();
	// LOOP TO DISPLAY OBJECT CONTENT
	echo"<thead class='bg-primary'>
			<tr>
			<th></th>
			<th>STORE NAME</th>
			<th>BRANCH NAME</th>
			<th data-orderable='false'>ACTIONS</th>
			</tr>
		</thead>
		<tbody>";
	for ($i=0; $i < count($result); $i++) { 
		$obj=$result[$i];
	
		$dateCreated=$obj->getCreatedAt()->format('M d Y - H:i a');
		$dateUpdated=$obj->getUpdatedAt()->format('M d Y - H:i a');
		echo'<tr>
				<td></td>
				<td>'.$obj->get('store')->get("name").'</td>
				<td>'.$obj->get('name').'</td>';
		echo'<td class="one-action">
			<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success update-store-branch">
				<span class="glyphicon glyphicon-edit" ></span>
			</button>&nbsp;
			<button deleteAttr="'.$obj->getObjectId().'" class="btn btn-danger delete-store-branch">
				<span class="glyphicon glyphicon-trash" ></span>
			</button>
			</td>
			</tr>';
	}
	echo "</tbody>";
}

// Filter store name
function filterStoreName(){
	$query = new ParseQuery("Store");
	$query->notEqualTo("isDeleted", true);
	$count=$query->find();
	echo '<option value="">All Store Name</option>';
	for ($i=0; $i <count($count) ; $i++) {
		$obj=$count[$i];
		echo "<option value='".$obj->get('name')."''>".$obj->get("name")."</option>";
	}
}

// DISPLAY STORE NAME FROM STORE CLASS
function displayStoreName() {
	$query = new ParseQuery("Store");
	$query->notEqualTo("isDeleted", true);
	$count=$query->find();

	echo "<select class='form-control select-store'>";
	for ($i=0; $i <count($count) ; $i++) { 
		$obj=$count[$i];
		echo "<option value='".$obj->getObjectId()."'>".$obj->get("name")."</option>";

	}
	echo "</select>";
}

// DISPLAY STORE NAME FOR UPDATE
function displayStoreNameUpdate($id) {
	$store = new ParseQuery("StoreBranch");
	$store->equalTo("objectId", $id);
	$store->includeKey("store");
	$storeCount=$store->find();

	for ($i=0; $i < count($storeCount) ; $i++) { 
		$obj=$storeCount[$i];
		$storeId=$obj->get("store")->getObjectId();
		$storeName=$obj->get("store")->get("name");
	}

	$query = new ParseQuery("Store");
	$query->notEqualTo("objectId",$storeId);
	$query->notEqualTo("isDeleted", true);
	$count=$query->find();

	echo "<select class='form-control update-select-store'>";
	echo "<option value='".$storeId."'>".$storeName."</option>";
	for ($i=0; $i <count($count) ; $i++) {
		$obj=$count[$i];
		echo "<option value='".$obj->getObjectId()."''>".$obj->get("name")."</option>";
	}
	echo "</select>";
}

// VIEW STORE BRANCH
function viewStoreBranch($id) {
	$query = new ParseQuery("StoreBranch");
	$query->equalTo("objectId", $id);
	$query->includeKey("store");
	$count = $query->find();
	
	for ($i = 0; $i < count($count); $i++) {
		$object = $count[$i];
  	  	$result['name']=$object->get('name'); 	
  	  	$result['store']=$object->get('store')->get('name'); 	
  	  	$result['storeId']=$object->get('store')->getObjectId(); 	
	}
	return $result;
}

// ADD STORE BRANCH NAME
function addStoreBranch($id,$name) {
	$search = new ParseQuery("StoreBranch");
	$search->equalTo("name", $name);
	$count = $search->find();

	if(count($count)==1){
		$result['status']='found';
	}else{
		$pointer = new ParseObject("Store", $id); //TO WHAT ID
		// POINT BY
		$query = new ParseObject("StoreBranch");
		$query->set("store", $pointer);
		$query->set("name", $name);
		$query->set("isDeleted", false);
		$query->save();
		$result['status']='success';
	}
	return $result;
}

// UPDATE STORE BRANCH
function updateStoreBranch($id,$name,$store) {
	$pointer = new ParseObject("Store", $store);
	$query = new ParseQuery("StoreBranch");
	$query->equalTo("name", $name);
	$count = $query->find();
	// FIND SAME NAME
	if(count($count)==1){
		for ($i = 0; $i < count($count); $i++) {
	  		$foundId=$count[$i]->getObjectId();
	  		// NO SAME NAME FOUND
			if($foundId==$id){
				$count[$i]->set("name", $name);
				$count[$i]->set("store",$pointer);
				$count[$i]->save();
				$result['status']='success';
			}
			// NAME ALREADY EXIST IN THE DATABASE
			else{
				$result['status']='exist';
			}
		}
	}else{
		$pointer = new ParseObject("Store", $store); //TO WHAT ID
		$query = new ParseQuery("StoreBranch");
		$query->equalTo("objectId", $id);
		$count = $query->find();
		for ($i = 0; $i < count($count); $i++) {
			$count[$i]->set("name", $name);
			$count[$i]->set("store",$pointer);
			$count[$i]->save();
			$result['status']='success';
		}
	}
	return $result;
}

// DELETE STORE BRANCH
function deleteStoreBranch($id) {
	$query = new ParseQuery("StoreBranch");
	$query->equalTo("objectId",$id);
	$count=$query->find();

	for ($i=0; $i <count($count); $i++) { 
		$obj=$count[$i];
		$obj->set("isDeleted",true);
		$obj->save();
		$result['status']='success';
	}

	return $result;
}

// =================================PRODUCT================================
// DISPLAY PRODUCT TABLE
function displayProduct() {
	$query = new ParseQuery("Product");
	// $query->equalTo("isDeleted",false);
	$query->includeKey("user");
	$query->includeKey("store");
	$query->includeKey("branch");
	$query->includeKey("brand");
	$query->includeKey("model");
	$query->descending("createdAt");
	// LOOP TO DISPLAY OBJECT CONTENT
	echo"<thead class='bg-primary'>	
			<tr>
			<th></th>
			<th>USER</th>
			<th>STORE NAME</th>
			<th>BRANCH NAME</th>
			<th>BRAND NAME</th>
			<th>MODEL NAME</th>
			<th>STATUS</th>
			<th>CATEGORY</th>
			<th data-orderable='false'>ACTION</th>
			</tr>
		</thead>
		<tbody>";
	$count=$query->find();
	for ($i=0; $i <count($count) ; $i++) {
		$obj = $count[$i];
		$dateCreated=$obj->getCreatedAt()->format('M d Y - H:i a');
		$dateUpdated=$obj->getUpdatedAt()->format('M d Y - H:i a');
		$datePurchase=$obj->get('purchaseDate')->format('M d Y - H:i a');
		$image=$obj->get("invoice")->getURL();
		echo'<tr>
				<td></td>
				<td><a viewUserAttr="'.$obj->get("user")->getObjectId().'" class="view-user-details" href="#">'.$obj->get("user")->get("firstName")." ".$obj->get("user")->get("lastName").'</a></td>
				<td>'.$obj->get("store")->get("name").'</td>
				<td>'.$obj->get("branch")->get("name").'</td>
				<td>'.$obj->get("brand")->get("name").'</td>
				<td>'.$obj->get("model")->get("name").'</td>
				<td>'.$obj->get('status').'</td>
				<td>'.$obj->get('category').'</td>';
		echo'<td>
			<button viewAttr="'.$obj->getObjectId().'" class="btn btn-warning view-product">
				VIEW
			</button>
			</td>
			</tr>';
	}
	echo "</tbody>";
}

// VIEW PRODUCT
function viewProduct($id) {
	$query = new ParseQuery("Product");
	$query->includeKey("user");
	$query->includeKey("store");
	$query->includeKey("branch");
	$query->includeKey("brand");
	$query->includeKey("model");
	$query->equalTo("objectId", $id);
	$count = $query->find();
	
	for ($i = 0; $i < count($count); $i++) {
		$object = $count[$i];

		$invoice=$object->get('invoice');
		if($invoice) {
			$picture = $invoice->getURL();
		}else{ // if no picture
			$picture = "";
		}

  	  	$result['image']=$picture; 	 	
  	  	$result['userFname']=$object->get('user')->get('firstName'); 	 	
  	  	$result['userLname']=$object->get('user')->get('lastName'); 	 	
  	  	$result['store']=$object->get('store')->get('name'); 	 	
  	  	$result['branch']=$object->get('branch')->get('name'); 	 	
  	  	$result['brand']=$object->get('brand')->get('name'); 	 	
  	  	$result['model']=$object->get('model')->get('name'); 	 	
  	  	$result['transaction']=$object->get('transactionNumber'); 	 	
  	  	$result['date']=$object->get('purchaseDate')->format('M d Y - H:i a');	 	
  	  	$result['category']=$object->get('category'); 

  	  	$serial=$object->get('serialNumber');
		if($serial){
			$sn=$serial;
		}else{
			$sn="0";
		}  	  	
  	  	$result['serial']=$sn;

  	  	$quantity=$object->get('quantity'); 
  	  	if($quantity){
  	  		$qnty=$quantity;
  	  	}else{
  	  		$qnty="0";
  	  	}
  	  	$result['quantity']=$qnty;

  	  	$result['status']=$object->get('status'); 	 	
	}
	return $result;
}

// ================================SERVICE CENTER======================
// DISPLAY SERVICE CENTER TABLE
function displayServiceCenterTable() {
	$query = new ParseQuery("ServiceCenter");
	$query->descending("createdAt");
	$query->equalTo("isDeleted",false);
	$result=$query->find();
	// LOOP TO DISPLAY OBJECT CONTENT
	echo"<thead class='bg-primary'>
			<tr>
			<th></th>
			<th>NAME</th>
			<th>ADDRESS</th>
			<th>CONTACT NUMBER</th>
			<th>EMAIL</th>
			<th>WORKING DAYS</th>
			<th>WORKING HOURS</th>
			<th data-orderable='false'>ACTIONS</th>
			</tr>
		</thead>
		<tbody>";
	for ($i=0; $i < count($result); $i++) { 
		$obj=$result[$i];

		$dateCreated=$obj->getCreatedAt()->format('M d Y - H:i a');
		$dateUpdated=$obj->getUpdatedAt()->format('M d Y - H:i a');
		echo'<tr>
				<td></td>
				<td>'.$obj->get("name").'</td>
				<td>'.$obj->get('address').'</td>
				<td>'.$obj->get('contactNumber').'</td>
				<td>'.$obj->get('email').'</td>
				<td>'.$obj->get('workingDays').'</td>
				<td>'.$obj->get('workingHours').'</td>';
		echo'<td class="two-action">
			<button viewAttr="'.$obj->getObjectId().'" class="btn btn-warning view-service-center">
				<span class="glyphicon glyphicon-eye-open" ></span>
			</button>&nbsp;
			<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success update-service-center">
				<span class="glyphicon glyphicon-edit" ></span>
			</button>&nbsp;
			<button deleteAttr="'.$obj->getObjectId().'" class="btn btn-danger delete-service-center">
				<span class="glyphicon glyphicon-trash" ></span>
			</button>
			</td>
			</tr>';
	}
	echo "</tbody>";
}

// ADD SERVICE CENTER
// addServiceCenter("namasde","wdf","wdt","whf","wht","address","number","email","14.8527393","-120.81603760000007");
function addServiceCenter($name,$wdf,$wdt,$whf,$wht,$address,$number,$email,$latitude,$longitude) {
	// convert to float and save to geopoint
	$addressLocation = new ParseGeoPoint((float)$latitude, (float)$longitude);
	
	$workingDays = $wdf." - ".$wdt;
	$workingHours = str_replace(' ','', $whf)." - ".str_replace(' ','', $wht);
	$search = new ParseQuery("ServiceCenter");
	$search->equalTo("name", $name);
	$count = $search->find();

	if(count($count)==1){
		$result['status']='found';
	}else{
		$query = new ParseObject("ServiceCenter");
		$query->set("name", $name);
		$query->set("workingDays",$workingDays);
		$query->set("workingHours",$workingHours);
		$query->set("address",$address);
		$query->set("contactNumber",$number);
		$query->set("email",$email);
		$query->set("location",$addressLocation);
		$query->set("isDeleted",false);
		$query->save();
		$result['status']='success';
	}
	return $result;
	// var_dump($result);
}

// VIEW SERVICE CENTER
// viewServiceCenter("1bWhgogidQ");
function viewServiceCenter($id) {
	$query = new ParseQuery("ServiceCenter");
	$query->equalTo("objectId", $id);
	$count = $query->find();
	
	for ($i = 0; $i < count($count); $i++) {
		$object = $count[$i];
  	  	$result['name']=$object->get('name'); 	
  	  	$result['address']=$object->get('address'); 	
  	  	$result['number']=$object->get('contactNumber'); 	
  	  	$result['email']=$object->get('email'); 	
  	  	$latitude=$object->get('location'); 
  	  	$longitude=$object->get('location'); 
  	  	$result['latitude']=$latitude->getLatitude(); // get latitude
  	  	$result['longitude']=$longitude->getLongitude(); // get longitude
  	  	$workingDays=$object->get('workingDays'); 
  	  	// separate string
		$wd = explode(" - ", $workingDays);
		$result['wdf']=$wd[0];//Monday
		$result['wdt']=$wd[1];//Sunday
		//separate string
  	  	$workingHours=$object->get('workingHours'); 
  	  	$wh = explode(" - ", $workingHours);//A B
  	  	//put space
  	  	$from=substr($wh[0],4,2); //last 2 letter
  	  	$to=substr($wh[1],4,2);
  	  	$whf=str_replace($from,' '.$from, $wh[0]); // space before AM/PM
  	  	$wht=str_replace($to,' '.$to, $wh[1]);
  	  	$result['whf']=$whf;
  	  	$result['wht']=$wht;
	}
	return $result;
	// var_dump($result);
}

// UPDATE SERVICE CENTER
function updateServiceCenter($id,$name,$wdf,$wdt,$whf,$wht,$address,$number,$email,$latitude,$longitude) {
	$addressLocation = new ParseGeoPoint((float)$latitude, (float)$longitude);
	$workingDays = $wdf." - ".$wdt;
	$workingHours = str_replace(' ','', $whf)." - ".str_replace(' ','', $wht);
	$query = new ParseQuery("ServiceCenter");
	$query->equalTo("name", $name);
	$count = $query->find();
	// FIND SAME NAME
	if(count($count)==1){
		for ($i = 0; $i < count($count); $i++) {
	  		$foundId=$count[$i]->getObjectId();
	  		// NO SAME NAME FOUND
			if($foundId==$id){
				$count[$i]->set("name", $name);
				$count[$i]->set("workingDays",$workingDays);
				$count[$i]->set("workingHours",$workingHours);
				$count[$i]->set("address",$address);
				$count[$i]->set("contactNumber",$number);
				$count[$i]->set("email",$email);
				$count[$i]->set("location",$addressLocation);
				$count[$i]->save();
				$result['status']='success';
			}
			// NAME ALREADY EXIST IN THE DATABASE
			else{
				$result['status']='exist';
			}
		}
	}else{
		
		$query = new ParseQuery("ServiceCenter");
		$query->equalTo("objectId", $id);
		$count = $query->find();
		for ($i = 0; $i < count($count); $i++) {
			$count[$i]->set("name", $name);
			$count[$i]->set("workingDays",$workingDays);
			$count[$i]->set("workingHours",$workingHours);
			$count[$i]->set("address",$address);
			$count[$i]->set("contactNumber",$number);
			$count[$i]->set("email",$email);
			$count[$i]->save();
			$result['status']='success';
		}
	}
	return $result;
}

// DELETE SERVICE CENTER
function deleteServiceCenter($id) {
	$query = new ParseQuery("ServiceCenter");
	$query->equalTo("objectId",$id);
	$count=$query->find();

	for ($i=0; $i <count($count); $i++) { 
		$obj=$count[$i];
		$obj->set("isDeleted",true);
		$obj->save();
		$result['status']='success';
	}

	return $result;
}

// ================================SURVEY======================
// DISPLAY SURVEY TABLE
function displaySurveyTable() {
	$query = new ParseQuery("Survey");
	$query->descending("createdAt");
	$query->equalTo("isDeleted",false);
	$result=$query->find();
	// LOOP TO DISPLAY OBJECT CONTENT
	echo"<thead class='bg-primary'>
			<tr>
			<th></th>
			<th>SURVEY TITLE</th>
			<th>PARTICIPANTS</th>
			<th>POINTS</th>
			<th>STATUS</th>
			<th class='hidden'>ACTIVE</th>
			<th data-orderable='false'>ACTIONS</th>
			</tr>
		</thead>
		<tbody>";
	for ($i=0; $i < count($result); $i++) { 
		$obj=$result[$i];

		$dateCreated=$obj->getCreatedAt()->format('M d Y - H:i a');
		$dateUpdated=$obj->getUpdatedAt()->format('M d Y - H:i a');
		$getQuestion=$obj->get('questions');
		$getParticipants=$obj->get('participants');

		$active=$obj->get("isActive");
		if($active==1){
			$status="Active";
			$active="Yes";
		}else{
			$status="Inactive";
			$active="No";
		}

		if($getParticipants){
			$countParticipants=count($getParticipants);
			foreach ($getParticipants as $participants) {
				$p.=$participants.",";
			}
			$partNum='<a getSurveyIdAttr="'.$obj->getObjectId().'" viewPartAttr="'.rtrim($p,',').'" href="#" class="view-participants">'.$countParticipants.' participants</a>';
		}else{
			$partNum="No participants";
		}
			
		echo'<tr>
				<td></td>
				<td>'.$obj->get("title").'</td>
				<td>'.$partNum.'</td>
				<td>'.$obj->get("points").'</td>
				<td>'.$status.'</td>
				<td class="hidden">'.$active.'</td>';

		echo'</td><td class="two-action">
			<button viewAttr="'.$obj->getObjectId().'" class="btn btn-warning view-survey">
				<span class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="View survey questions"></span>
			</button>&nbsp;
			<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success update-survey">
				<span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit survey"></span>
			</button>&nbsp;
			<button deleteAttr="'.$obj->getObjectId().'" class="btn btn-danger delete-survey">
				<span class="glyphicon glyphicon-trash" ></span>
			</button>
			</td>
			</tr>';
	}
	echo "</tbody>";
}

// DISPLAY SURVEY TITLE FOR ADDING QUESTION
// function viewSurveyTitle() {
// 	$query = new ParseQuery("Survey");
// 	$result=$query->find();

// 	echo "<select class='form-control survey-id'>";
// 	for ($i=0; $i < count($result); $i++) {
// 		$obj=$result[$i];
// 		echo "<option value='".$obj->getObjectId()."'>".$obj->get("title")."</option>";
// 	}
// 	echo "</select>";
// }

// DISPLAY SURVEY PARTICIPANTS
function viewSurveyParticipantsTable($surveyId,$participants){

	echo "<table class='table table-bordered table-striped table-hover'>
			<tr>
				<th>USER</th>
				<th>ACTIONS</th>
			</tr>";

	$arrayParticipants = explode(',', $participants);
	foreach($arrayParticipants as $id){
		$query = new ParseQuery("_User");
		$query->equalTo("objectId", $id);
		$result=$query->find();

		for ($i=0; $i < count($result); $i++) { 
			$obj=$result[$i];

			echo "<tr>
					<td>".$obj->get('firstName')." ".$obj->get('lastName')."</td>";
			echo'<td>
				<button viewUserAttr="'.$obj->getObjectId().'" class="btn btn-warning view-user-details">
					DETAILS
				</button>&nbsp;
				<button viewSurveyIdAttr="'.$surveyId.'" viewUserAttr="'.$obj->getObjectId().'" class="btn btn-success view-user-answers">
					ANSWERS
				</button>
				</td>
				</tr>';
		}

	}
	
	echo "</table>";
}	

// VIEW SURVEY
function viewSurvey($id) {
	$query = new ParseQuery("Survey");
	$query->equalTo("objectId", $id);
	$count = $query->find();
	
	for ($i = 0; $i < count($count); $i++) {
		$object = $count[$i];
  	  	$result['title']=$object->get('title'); 	
  	  	$result['points']=$object->get('points'); 		
  	  	$result['active']=$object->get('isActive'); 		
	}
	return $result;
}

// DISPLAY SURVEY ANSWER
// viewSurveyAnswerTable("waLV93759B");
function viewSurveyAnswerTable($userId,$surveyId){
	$query = new ParseQuery("SurveyAnswer");
	// GET THE parent user 
	$query->equalTo("user",['__type' => "Pointer", 'className'=> "_User", 'objectId' => $userId]);
	$query->equalTo("survey",['__type' => "Pointer", 'className'=> "Survey", 'objectId' => $surveyId]);
	$query->includeKey("user");
	$query->includeKey("survey");
	$result=$query->find();

	if(count($result)==0){
		echo "<h2>No question answered</h2>";
	}else{
		echo "<h5>Survey answered: <span class='badge'>".count($result)."</span></h5>";
		for ($i=0; $i < count($result); $i++) { 
			$obj = $result[$i];
			$answer=$obj->get("answer");
			$getQuestion=$obj->get("survey")->get("questions");
			$encode= json_encode($getQuestion);
			$decode = json_decode($encode,true); //true if associative array
			echo "<hr size='5' color='black'>";
			echo "<hr size='5' color='black'>";
			$num=0; //assign number
			foreach($decode as $decoded) {
				$c=count($decoded['choices']);
				$add=$c+1;// rowspan number
				$num++;
				// echo"<tr>
				// 		<td rowspan='".$add."'>".$num.".) ".$decoded['question']."</td>
				// 	</tr>";
				echo "<h3>".$num.". ".$decoded['question']."</h3>";
		        foreach($decoded['choices'] as $choices) {
		        	$checked="";
					if ($answer[$num-1]==$choices['value']) {
						 $checked="checked='checked'";
					}
					echo "<p><input type='checkbox' ".$checked." disabled='disabled'> ".$choices['label']."</p>";
		        }
	    	}

		}
	} //else

}	

// ADD SURVEY
function addSurvey($title,$points) {
	$search = new ParseQuery("Survey");
	$search->equalTo("title", $title);
	$count = $search->find();

	if(count($count)==1){
		$result['status']='found';
	}else{
		$query = new ParseObject("Survey");
		$query->set("title",$title);
		$query->set("points",(int)$points);
		$query->set("isActive",false);
		$query->set("isDeleted",false);
		$query->save();
		$result['status']='success';
	}
	return $result;
}

// UPDATE SURVEY
function updateSurvey($id,$title,$points,$active){
	
	$query = new ParseQuery("Survey");
	$query->equalTo("title", $title);
	$count = $query->find();
	// FIND SAME NAME
	if(count($count)==1){
		for ($i = 0; $i < count($count); $i++) {
	  		$foundId=$count[$i]->getObjectId();
	  		// NO SAME NAME FOUND
			if($foundId==$id){
				$count[$i]->set("title", $title);
				$count[$i]->set("points", (int)$points);
				$count[$i]->set("isActive", (boolean)$active);
				$count[$i]->save();
				$result['status']='success';
			}
			// NAME ALREADY EXIST IN THE DATABASE
			else{
				$result['status']='exist';
			}
		}
	}else{
		
		$query = new ParseQuery("Survey");
		$query->equalTo("objectId", $id);
		$count = $query->find();
		for ($i = 0; $i < count($count); $i++) {
			$count[$i]->set("title", $title);
			$count[$i]->set("points", (int)$points);
			$count[$i]->set("isActive", (boolean)$active);
			$count[$i]->save();
			$result['status']='success';
		}
	}
	return $result;
}

// ADD QUESTION TO A SURVEY
function addSurveyQuestion($surveyId,$question,$label,$value) {
	// $ll = array_filter($label, 'strlen');
	// $vv = array_filter($value, 'strlen');

	$query = new ParseQuery("Survey");
	$query->equalTo("objectId", $surveyId);
	$count = $query->find();

	$choices = array();
	for ($i=0; $i < count($label); $i++) { 
		// push array inside array
		array_push($choices, array("label"=>$label[$i], "value"=>$value[$i]));
	}		

	$survey = array("question"=>$question, "choices"=>$choices);
	// $all =array();
	$json = json_encode(array($survey));

	for ($x=0; $x < count($count); $x++) { 
		$obj=$count[$x];
		// GET EXISTING ARRAY
		$getQuestion=$obj->get('questions');
		
		$encode= json_encode($getQuestion);//array number of specific question
		$decoded = json_decode($encode,true); //true if associative array
		
		$questionsArray = array();
		foreach ($decoded as $decode) {
			// put put $decode arrays in arra()
			array_push($questionsArray,$decode['question']);
		}
		// check if question is already exist
		if(in_array($question ,$questionsArray)){
			$result['status']='exist';
		}else{
			// CHECK IF ARRAY IS NULL/UNDEFINED
			if(empty($getQuestion)){
				$addArray = json_decode($json,true);
			}else{
				// APPEND NEW ARRAY TO EXISTING ARRAY
				$addArray = array_merge($getQuestion,json_decode($json,true));
			}
			$count[$x]->setArray("questions",$addArray);
			$count[$x]->save();
			$result['status']='success';
		}

	}

	return $result;

}

// countAnswers("kEcjh5h04g");
function countAnswers($id){
	// $countAns = new ParseQuery("");
	$countAns = new ParseQuery("SurveyAnswer");
	$countAns->equalTo("survey",['__type' => "Pointer", 'className'=> "Survey", 'objectId' => $id]);
	$resultAns=$countAns->find();

	for ($i=0; $i < count($resultAns); $i++) { 
		$obj=$resultAns[$i];

		$ans = $obj->get('answer');


		echo "--------------".$ans[1];

		// foreach ($ans as $x) {
		// 	echo "----------------".$x;
		// }


		// echo "<pre>";
		// var_dump($ans[0]);
		// echo "</pre>";
	}

}

// DISPLAY SURVEY QUESTION
function viewSurveyQuestionTable($surveyId){

	$query = new ParseQuery("Survey");
	// GET THE parent user 
	$query->equalTo("objectId", $surveyId);
	$result=$query->find();
	echo "<button class='btn btn-info btn-back'><span class='glyphicon glyphicon-arrow-left'></span> Back</button>&nbsp;";
	echo "<button type='button' class='btn btn-info btn-md' data-toggle='modal' data-target='#addQuestionModal'><span class='glyphicon glyphicon-plus'></span> ADD NEW QUESTION</button>";
	for ($i=0; $i < count($result); $i++) {
		$obj=$result[$i];
		// DISPLAY SELECTED TITLE
		$title=$obj->get("title");
		$participants=$obj->get('participants');
		$status='Inactive';
		if($obj->get('isActive')){
			$status='Active';
		}
		$points=$obj->get('points');
		echo "<h3><b>Survey title:</b> ".$title."</h3>";
		echo "<h3><b>Participants:</b> ".count($participants)."</h3>";
		echo "<h3><b>Status:</b> ".$status."</h3>";
		echo "<h3><b>Points:</b> ".$points."</h3>";
	}
	echo "
		<table class='table table-striped table-hover table-fixed'>
			<thead>
				<tr class='bg-primary'>
	    			<th>QUESTIONS</th>
	    			<th data-orderable='false'>ACTIONS</th>
	  			</tr>
			</thead>
			<tbody>";

  	for ($r=0; $r < count($result); $r++) {
  		$obj=$result[$r];
		$getQuestion=$obj->get('questions');
		$getParticipants=$obj->get('participants');

		$encode= json_encode($getQuestion);
		$decode = json_decode($encode,true); //true if associative array
		
		if(empty($getQuestion)){
			echo "<tr class='text-center'><td colspan='2'>No Questions Found</td></tr>";
		}else{
			$num=0; //assign number
			foreach($decode as $decoded) {
				$c=count($decoded['choices']);
				$add=$c+1;// rowspan number
				$num++;
				echo"<tr>
						<td><b>".$num.".)</b> ".$decoded['question']."</td>
						<td class='two-action'>
							<button getQuestionArrayNum='".$num."' getSurveyId='".$surveyId."' class='btn btn-warning view-survey-choices'>
								<span class='glyphicon glyphicon-eye-open' data-toggle='tooltip' data-placement='top' title='View choices'></span>
							</button>&nbsp;
							<button getQuestionArrayNum='".$num."' getSurveyId='".$surveyId."' class='btn btn-success update-survey-question-choices'>
								<span class='glyphicon glyphicon-edit' data-toggle='tooltip' data-placement='top' title='Edit question and choices'></span>
							</button>&nbsp;
							<button getQuestionArrayNum='".$num."' getSurveyId='".$surveyId."' class='btn btn-danger delete-survey-question'>
								<span class='glyphicon glyphicon-trash' ></span>
							</button>
						</td>		
					</tr>";
		    //     foreach($decoded['choices'] as $choices) {
		        	
		    //     	echo"<tr>
						// 	<td>".$choices['label']."</td>
						// 	<td>".$choices['value']."</td>
						// </tr>";

		    //     }

	    	}//foreach

		}//else

  	}//for
	echo "</tbody></table>";
}	

// VIEW CHOICES OF SELECTED QUESTION
function viewSurveyChoices($array,$id) {
	$query = new ParseQuery("Survey");
	// GET THE parent user 
	$query->equalTo("objectId", $id);
	$result=$query->find();

  	for ($r=0; $r < count($result); $r++) {
  		$obj=$result[$r];
		$getQuestion=$obj->get('questions');
		
		$encode= json_encode($getQuestion[(int)$array-1]);//array number of specific question
		$decoded = json_decode($encode,true); //true if associative array
		if(empty($getQuestion)){
			echo "<tr class='text-center'><td colspan='2'>No Choices Found</td></tr>";
		}else{
			echo "<h3><b>Question: </b></br>".$decoded['question']."</h3>";
			$num=0; //assign number
			// foreach($decode as $decoded) {
				$c=count($decoded['choices']);
				$add=$c+1;// rowspan number
				$num++;
				echo "<table class='table table-bordered'>
						<tr class='active'>
							<th colspan='2'>CHOICES</th>
						</tr>
			  			<tr class='active'>
			    			<th>LABEL</th>
			    			<th>VALUE</th>
			  			</tr>";
		        foreach($decoded['choices'] as $choices) {
		        	echo"<tr>
							<td>".$choices['label']."</td>
							<td>".$choices['value']."</td>
						</tr>";
		        }
		        echo "</table>";
	    	// }//foreach
		}//else

  	}//for
	
}

// UPDATE CHOICES AND QUESTION OF SELECTED QUESTION
function updateSurveyQuestionChoices($array,$id) {
	$questionIndex = (int)$array-1;
	$query = new ParseQuery("Survey");
	// GET THE parent user 
	$query->equalTo("objectId", $id);
	$result=$query->find();

  	for ($r=0; $r < count($result); $r++) {
  		$obj=$result[$r];
		$getQuestion=$obj->get('questions');
		
		$encode= json_encode($getQuestion[$questionIndex]);//array number of specific question
		$decoded = json_decode($encode,true); //true if associative array
		if(empty($getQuestion)){
			// 
		}else{
			echo "<input type='hidden' class='question-survey-id' value='".$id."'>";
			echo "<input type='hidden' class='question-index' value='".$questionIndex."'>";
			echo "<label>Survey question:</label>";
			echo "<input type='text' class='form-control update-survey-question' value='".$decoded['question']."'></br>";
			// foreach($decode as $decoded) {
				echo "<table class='update-append-question-choices'>
		  			 <tr>
						<th>Label</th>
						<th>Value</th>
						<th>Action</th>
		  			 </tr>";
		  			 $btn_num=2;
		  			 $row_num=1;
		  			 $count=0;
		        foreach($decoded['choices'] as $choices) {
		        	$count++;
		        	if ($choices == reset($decoded['choices'])) {
		        		$action = "<button class='btn btn-primary update-add-survey-choices'><span class='glyphicon glyphicon-plus'></span> Add</button>";
		        	}
		        	else{
		        		$action = "<button id='".$count."' class='btn btn-danger update-remove-survey-choices'><span class='glyphicon glyphicon-remove'></span> Remove</button>";
		        	}

		        	echo "<tr id='row_tr".$count."'>
			        	<td>
							<input type='text' name='update-survey-label[]' class='form-control' value='".$choices['label']."' placeholder='label'>
						</td>
						<td>
							<input type='text' name='update-survey-value[]' class='form-control' value='".$choices['value']."' placeholder='value'>
						</td>
						<td>".$action."</td>
					</tr>";	
		        }
		        // echo "<input type='hidden' id='count' value='".$count."'>";
		        echo "</table>";
	    	// }//foreach
		}//else

  	}//for
	
}


// saveSurveyQuestionChoices('vwS6BGQcxT','0','update-question','aupdate-label','aupdage-value');
function saveSurveyQuestionChoices($surveyId,$questionIndex,$question,$label,$value){
	$arrayIndex = (int)$questionIndex;
	$query = new ParseQuery("Survey");
	$query->equalTo("objectId", $surveyId);
	$count = $query->find();

	$choices = array();
	for ($i=0; $i < count($label); $i++) { 
		// push array inside array
		array_push($choices, array("label"=>$label[$i], "value"=>$value[$i]));
	}		

	$survey = array("question"=>$question, "choices"=>$choices);
	// $all =array();
	$json = json_encode($survey);

	for ($x=0; $x < count($count); $x++) { 
		$obj=$count[$x];
		// GET EXISTING ARRAY

		$getQuestion=$obj->get('questions');

		// get array of selected survey question
		$encode= json_encode($getQuestion);//array number of specific question
		$decoded = json_decode($encode,true);
		$decoded[$arrayIndex]=json_decode($json,true);

		$obj->setArray("questions",$decoded);
		$obj->save();
		$result['status']='success';
	}
	return $result;

};

// Delete question in a survey
// deleteSurveyQuestionChoices('vwS6BGQcxT','0');
function deleteSurveyQuestionChoices($surveyId,$questionIndex){
	
	$answer = new ParseQuery("SurveyAnswer");
	$answer->equalTo("survey",['__type' => "Pointer", 'className'=> "Survey", 'objectId' => $surveyId]);
	$countAns=$answer->find();

	if(count($countAns)==0){
		$arrayIndex = (int)$questionIndex;
		$query = new ParseQuery("Survey");
		$query->equalTo("objectId", $surveyId);
		$count = $query->find();

		for ($x=0; $x < count($count); $x++) { 
			$obj=$count[$x];
			// GET EXISTING ARRAY
			$getQuestion=$obj->get('questions');

			// get array of selected survey question
			$encode= json_encode($getQuestion);//array number of specific question
			$decoded = json_decode($encode,true);
			// $decoded[$arrayIndex]=json_decode($json,true);
			unset($decoded[$arrayIndex]);// remove an array
			// var_dump($decoded);
			$obj->setArray("questions",$decoded);
			$obj->save();
			$result['status']='success';
		}
	}else{
		$result['status']='failed';
	}
	
	return $result;

};


// DELETE SURVEY

// deleteSurvey("8DVvODvCBL");
function deleteSurvey($id) {

	$query = new ParseQuery("Survey");
	$query->equalTo("objectId",$id);
	$count=$query->find();

	for ($i=0; $i <count($count); $i++) { 
		$obj=$count[$i];

		$participants=$obj->get('participants');
		$question=$obj->get('questions');
		if((count($participants)==0) && (count($question)==0)){
			$obj->set("isDeleted",true);
			$obj->save();
			$result['status']='success';
		}
		else{
			$result['status']='failed';
		}

	}

	return $result;
}

// ================================ARTICLE======================
// DISPLAY ARTICLE TABLE
function displayArticleTable() {
	$query = new ParseQuery("Article");
	$query->descending("createdAt");
	$query->equalTo("isDeleted",false);
	$result=$query->find();
	// LOOP TO DISPLAY OBJECT CONTENT
	echo"<thead class='bg-primary'>
			<tr>
			<th></th>
			<th data-orderable='false'>IMAGE</th>
			<th>TITLE</th>
			<th>DATE</th>
			<th>VIEWS</th>
			<th data-orderable='false'>ACTIONS</th>
			</tr>
		</thead>
		<tbody>";
	for ($i=0; $i < count($result); $i++) { 
		$obj=$result[$i];
		// $dateCreated=$obj->getCreatedAt()->format('M d Y - H:i a');
		// $dateUpdated=$obj->getUpdatedAt()->format('M d Y - H:i a');
		$date=$obj->get("date")->format('M d Y - H:i a');
		$image=$obj->get('images');
		
		$thumbnail= (array)$image;
		
		if($image){
			$getThumbnail = "<img src='".$thumbnail['thumbnail'] ."' width='100' height='100'>";
		}else{
			$getThumbnail = "<img src='' alt='no image'>";
		}

		$getViews=$obj->get("views");
		if($getViews){
			$views=$getViews;
		}else{
			$views="0";
		}
		echo'<tr>
				<td></td>
				<td>'.$getThumbnail.'</td>
				<td>'.$obj->get("title").'</td>
				<td>'.$date.'</td>
				<td>'.$views.'</td>';
		echo'<td class="two-action">
			<button viewAttr="'.$obj->getObjectId().'" class="btn btn-warning view-article">
				<span class="glyphicon glyphicon-eye-open" ></span>
			</button>&nbsp;
			<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success update-article">
				<span class="glyphicon glyphicon-edit" ></span>
			</button>&nbsp;
			<button deleteAttr="'.$obj->getObjectId().'" class="btn btn-danger delete-article">
				<span class="glyphicon glyphicon-trash" ></span>
			</button>
			</td>
			</tr>';
	}
	echo "</tbody>";
}

// DISPLAY ARTICLE CONTENT
// displayArticleContent('WnJnwBxell');
// function displayArticleContent($id){
// 	$query = new ParseQuery("Article");
// 	$query -> equalTo("objectId",$id);
// 	$result = $query->find();

// 	for ($i=0; $i < count($result); $i++) { 
// 		$obj=$result[$i];
// 		$article_content=$obj->get("content");
// 	}
// 	// return $content;
// 	// var_dump($content);
// 	$settings = array( 'media_buttons' => false,'editor_height' => 325 ); //no media
// 	$content =$article_content; //no content
// 	$editor_id = 'updateArticleContent'; //element id

// 	$asd = wp_editor( $content, $editor_id ,$settings);

// 	return $asd;
// }

// ADD ARTICLE
function addArticle($title,$image,$content) {
	$addDate = new DateTime(date("Y-m-d h:i:sa"));
	$search = new ParseQuery("Article");
	$search->equalTo("title", $title);
	$count = $search->find();

	require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attachment_id = media_handle_upload( $image, 55 );
    // GET THUMBNAIL URL
	$thumbnail_attributes = wp_get_attachment_image_src( $attachment_id ,'thumbnail', false);
	$thumbnail_url = $thumbnail_attributes[0];
	// GET ORIGINAL IMAGE URL
    $normal_attributes = wp_get_attachment_image_src( $attachment_id ,'full', false);
	$normal_url = $normal_attributes[0];

    $image_object = ["thumbnail" => $thumbnail_url,	"normal" => $normal_url];

	if(count($count)==1){
		$result['status']='found';
	}else{
		$query = new ParseObject("Article");
		$query->set("title",$title);
		$query->setAssociativeArray("images",$image_object);
		$query->set("date",$addDate);
		$query->set("content",$content);
		$query->set("isDeleted",false);
		$query->save();
		$result['status']='success';
	}
	return $result;
}

// VIEW ARTICLE
function viewArticle($id) {
	$query = new ParseQuery("Article");
	$query->equalTo("objectId", $id);
	$count = $query->find();
	
	for ($i = 0; $i < count($count); $i++) {
		$object = $count[$i];

		$image=$object->get('images');
		$thumbnail= (array)$image;
		if($image){
			$result['image'] = $thumbnail['normal'];
		}else{
			$result['image'] = "";
		}

  	  	$result['title']=$object->get('title'); 	 	
  	  	$result['date']=$object->get('date')->format('M d Y - H:i a');
  	  	$result['date2']=$object->get('date')->format('m/d/Y H:i A'); 	 	
  	  	$result['content']=$object->get('content');
  	  	$getView=$object->get('views'); 
  	  	if($getView){
  	  		$view=$getView;
  	  	}
  	  	else{
  	  		$view="0";
  	  	} 	 	
  	  	$result['views']=$view;	 	
	}
	return $result;
}

// UPDATE ARTICLE
// updateArticle("WnJnwBxell","www2",'',"asd","no_image");
function updateArticle($id,$title,$content,$image) {
	// $updateDate = new DateTime($date);
	$query = new ParseQuery("Article");
	$query->equalTo("title", $title);
	$count = $query->find();

	require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attachment_id = media_handle_upload( $image, 55 );
	$thumbnail_attributes = wp_get_attachment_image_src( $attachment_id ,'thumbnail', false);
	$thumbnail_url = $thumbnail_attributes[0];
	// GET ORIGINAL IMAGE URL
    $normal_attributes = wp_get_attachment_image_src( $attachment_id ,'full', false);
	$normal_url = $normal_attributes[0];

    $image_object = ["thumbnail" => $thumbnail_url,	"normal" => $normal_url];
	// FIND SAME NAME
	if(count($count)==1){
		for ($i = 0; $i < count($count); $i++) {
	  		$foundId=$count[$i]->getObjectId();
	  		// NO SAME NAME FOUND
			if($foundId==$id){
				if($image=="no_image"){
					$count[$i]->set("title", $title);
					// $count[$i]->set("date",$updateDate);
					$count[$i]->set("content",$content);
					$count[$i]->save();
					$result['status']='success';
				}else{
					$count[$i]->set("title", $title);
					// $count[$i]->set("date",$updateDate);
					$count[$i]->setAssociativeArray("images",$image_object);
					$count[$i]->set("content",$content);
					$count[$i]->save();
					$result['status']='success';
				}
				
			}
			// NAME ALREADY EXIST IN THE DATABASE
			else{
				$result['status']='exist';
			}
		}
	}else{
		$query = new ParseQuery("Article");
		$query->equalTo("objectId", $id);
		$count = $query->find();
		for ($i = 0; $i < count($count); $i++) {
			if($image=="no_image"){
				$count[$i]->set("title", $title);
				// $count[$i]->set("date",$updateDate);
				$count[$i]->set("content",$content);
				$count[$i]->save();
				$result['status']='success';
			}else{
				$count[$i]->set("title", $title);
				// $count[$i]->set("date",$updateDate);
				$count[$i]->setAssociativeArray("images",$image_object);
				$count[$i]->set("content",$content);
				$count[$i]->save();
				$result['status']='success';
			}
		}
	}
	return $result;
}

// DELETE ARTICLE
function deleteArticle($id) {
	$query = new ParseQuery("Article");
	$query->equalTo("objectId",$id);
	$count=$query->find();

	for ($i=0; $i <count($count); $i++) { 
		$obj=$count[$i];
		$obj->set("isDeleted",true);
		$obj->save();
		$result['status']='success';
	}
	return $result;
}

// ================================SERVICE CENTER APPOINTMENTS======================
// DISPLAY SERVICE CENTER APPOINTMENTS TABLE
function displayServiceCenterAppointmentTable() {
	$query = new ParseQuery("ServiceAppointment");
	$query->includeKey("serviceCenter");
	$query->includeKey("user");
	$query->includeKey("product");
	$query->descending("createdAt");
	$query->equalTo("isDeleted",false);
	$result=$query->find();
	// LOOP TO DISPLAY OBJECT CONTENT
	echo"<thead class='bg-primary'>
			<tr>
			<th></th>
			<th>SERVICE CENTER</th>
			<th>USER</th>
			<th>PRODUCT</th>
			<th>USER EMAIL</th>
			<th>USER NUMBER</th>
			<th>STATUS</th>
			<th>POINTS</th>
			<th data-orderable='false'>ACTIONS</th>
			</tr>
		</thead>
		<tbody>";
	for ($i=0; $i < count($result); $i++) { 
		$obj=$result[$i];

		$dateCreated=$obj->getCreatedAt()->format('M d Y - H:i a');
		$dateUpdated=$obj->getUpdatedAt()->format('M d Y - H:i a');
		$getStatus=$obj->get("status");
		$action='';
		if($getStatus=="pending"){
			$action = '<p><button updateAttr="'.$obj->getObjectId().'" statusAttr="accepted" class="btn btn-success update-appointment">
						<span data-toggle="tooltip" data-placement="top" title="Accept" class="glyphicon glyphicon-ok-sign"></span>
					   </button>&nbsp;
					   <button updateAttr="'.$obj->getObjectId().'" statusAttr="declined" class="btn btn-danger update-appointment">
						<span data-toggle="tooltip" data-placement="top" title="Decline" class="glyphicon glyphicon-remove-sign"></span>
					   </button></p>';
		}

		echo'<tr>
				<td></td>
				<td><a viewAttr="'.$obj->get("serviceCenter")->getObjectId().'" class="view-service-center" href="#">'.$obj->get("serviceCenter")->get("name").'</a></td>
				<td><a viewUserAttr="'.$obj->get("user")->getObjectId().'" class="view-user-details" href="#">'.$obj->get("user")->get("firstName")." ".$obj->get("user")->get("lastName").'</a></td>
				<td><a viewProductAttr="'.$obj->get("product")->getObjectId().'" class="view-product-appointment" href="#">'.$obj->get("product")->getObjectId().'</a></td>
				<td>'.$obj->get("userEmail").'</td>
				<td>'.$obj->get("userContactNumber").'</td>
				<td>'.$obj->get("status").'</td>
				<td>'.$obj->get("points").'</td>';
		echo'<td>
				'.$action.'
				<button viewAttr="'.$obj->getObjectId().'" class="btn btn-warning view-appointment">
					<span class="glyphicon glyphicon-eye-open" ></span>
				</button>&nbsp;
				<button deleteAttr="'.$obj->getObjectId().'" class="btn btn-danger delete-appointment">
					<span class="glyphicon glyphicon-trash" ></span>
				</button>
			</td>
			</tr>';
	}
	echo "</tbody>";
}

// Filter service center
function filterServiceCenter(){
	$query = new ParseQuery("ServiceCenter");
	$query->notEqualTo("isDeleted", true);
	$count=$query->find();
	echo '<option value="">All Service Center</option>';
	for ($i=0; $i <count($count) ; $i++) {
		$obj=$count[$i];
		echo "<option value='".$obj->get('name')."''>".$obj->get("name")."</option>";
	}
}

// VIEW SERVICE CENTER APPOINTMENT
function viewServiceCenterAppointent($id) {
	$query = new ParseQuery("ServiceAppointment");
	$query->includeKey("serviceCenter");
	$query->includeKey("user");
	$query->equalTo("objectId", $id);
	$count = $query->find();
	
	for ($i = 0; $i < count($count); $i++) {
		$object = $count[$i];
  	  	$result['name']=$object->get('serviceCenter')->get('name'); 	
  	  	$result['user']=$object->get('user')->get('firstName')." ".$object->get('user')->get('lastName'); 	
  	  	$result['email']=$object->get('userEmail'); 	
  	  	$result['number']=$object->get('userContactNumber'); 	
  	  	$result['status']=$object->get('status'); 	
  	  	$result['points']=$object->get('points'); 				
  	  	$result['problem']=$object->get('problem'); 				
	}
	return $result;
}

// VIEW USER PRODUCT 
function viewProductAppointment($id){

	$query = new ParseQuery("Product");
	$query->equalTo("objectId", $id);
	$query->includeKey("store");
	$query->includeKey("branch");
	$query->includeKey("brand");
	$query->includeKey("model");
	$result=$query->find();

	echo "<ul class='list-group'>";

	if(count($result)==0){
		echo "<li class='list-group-item'>
			  	<span class='display-info'>No Product Found</span>
			</li>";
		
	}else{
		for ($i=0; $i < count($result); $i++) { 
			$obj = $result[$i];
			$date=$obj->get("purchaseDate")->format('M d Y - H:i a');
			$image=$obj->get("invoice");

			if($image){
				$invoice=$image->getURL();
			}
			else{
				$invoice='';
			}
			echo "
					<li class='text-center'> <img class='img-rounded img-thumbnail' src='".$invoice."' alt='no image' width='200' height='200'></li>
					<li class='list-group-item'><span class='display-info'>Date purchased: </span><b>".$date."</b></li>
					<li class='list-group-item'><span class='display-info'>Store name: </span><b>".$obj->get("store")->get("name")."</b></li>
					<li class='list-group-item'><span class='display-info'>Branch name: </span><b>".$obj->get("branch")->get("name")."</b></li>
					<li class='list-group-item'><span class='display-info'>Brand name: </span><b>".$obj->get("brand")->get("name")."</b></li>
					<li class='list-group-item'><span class='display-info'>Model name: </span><b>".$obj->get("model")->get("name")."</b></li>
					<li class='list-group-item'><span class='display-info'>Transaction number: </span><b>".$obj->get("transactionNumber")."</b></li>
					<li class='list-group-item'><span class='display-info'>Serial number: </span><b>".$obj->get("serialNumber")."</b></li>
					<li class='list-group-item'><span class='display-info'>Quantity: </span><b>".$obj->get("quantity")."</b></li>
					<li class='list-group-item'><span class='display-info'>Color: </span><b>".$obj->get("color")."</b></li>
					<li class='list-group-item'><span class='display-info'>Status: </span><b>".$obj->get("status")."</b></li>
					<li class='list-group-item'><span class='display-info'>Category: </span><b>".$obj->get("category")."</b></li>
				";
		}
	}
	echo "</ul>";
}

// UPDATE STORE
function updateServiceCenterAppointment($id,$status,$points) {
	$query = new ParseQuery("ServiceAppointment");
	$query->equalTo("objectId", $id);
	$count = $query->find();
	for ($i = 0; $i < count($count); $i++) {
		if($status=="accepted"){
			$count[$i]->set("status", $status);
			$count[$i]->set("points", (int)$points);
			$count[$i]->save();
			$result['status']='success';
		}else if($tatus="declined"){
			$count[$i]->set("status", $status);
			$count[$i]->save();
			$result['status']='success';
		}else{
			$result['status']='failed';
		}
		
	}
	return $result;
}


// DELETE SERVICE CENTER APPOINTMENT
function deleteServiceCenterAppointent($id) {
	$query = new ParseQuery("ServiceAppointment");
	$query->equalTo("objectId",$id);
	$count=$query->find();

	for ($i=0; $i <count($count); $i++) { 
		$obj=$count[$i];
		$obj->set("isDeleted",true);
		$obj->save();
		$result['status']='success';
	}
	return $result;
}

// ================================REWARD PRODUCT======================
// DISPLAY REWARD PRODUCT TABLE
function displayRewardProductTable() {
	$query = new ParseQuery("RewardProduct");
	$query->descending("createdAt");
	$query->equalTo("isDeleted",false);
	$result=$query->find();
	// LOOP TO DISPLAY OBJECT CONTENT
	echo"<thead class='bg-primary'>
			<tr>
			<th></th>
		
			<th>IMAGE</th>
			<th>NAME</th>
			<th>POINTS</th>
			<th>TYPE</th>
			<th>BRAND</th>
			<th>VOUCHER VALUE</th>
			<th>STATUS</th>
			<th class='hidden'>AVAILABLE</th>
			<th data-orderable='false'>ACTIONS</th>
			</tr>
		</thead>
		<tbody>";

	for ($i=0; $i < count($result); $i++) { 
		$obj=$result[$i];
	
		$dateCreated=$obj->getCreatedAt()->format('M d Y - H:i a');
		$dateUpdated=$obj->getUpdatedAt()->format('M d Y - H:i a');

		$available=$obj->get("isAvailable");
		if($available==1){
			$state="available";
			$isAvailable="yes";
		}else{
			$state="unavailable";
			$isAvailable="no";
		}

		$image=$obj->get('images');
		
		$thumbnail= (array)$image;
		
		if($image){
			$getThumbnail = "<img src='".$thumbnail['thumbnail'] ."' width='100' height='100'>";
		}else{
			$getThumbnail = "<img src='' alt='no image'>";
		}
		echo'<tr>
				<td></td>
				<td>'.$getThumbnail.'</td>
				<td>'.$obj->get("name").'</td>
				<td>'.$obj->get("points").'</td>
				<td>'.$obj->get("type").'</td>
				<td>'.$obj->get("brand").'</td>
				<td>'.$obj->get("voucherValue").'</td>
				<td>'.$state.'</td>
				<td class="hidden">'.$isAvailable.'</td>';
		echo'<td>
			<button viewAttr="'.$obj->getObjectId().'" class="btn btn-warning view-reward-product">
				<span class="glyphicon glyphicon-eye-open" ></span>
			</button>&nbsp;
			<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success update-reward-product">
				<span class="glyphicon glyphicon-edit" ></span>
			</button>&nbsp;
			<button deleteAttr="'.$obj->getObjectId().'" class="btn btn-danger delete-reward-product">
				<span class="glyphicon glyphicon-trash" ></span>
			</button>
			</td>
			</tr>';
	}
	echo "</tbody>";
}

// ADD REWARD PRODUCT
function addRewardProduct($name,$points,$type,$description,$reward,$available,$image) {
	
	$search = new ParseQuery("RewardProduct");
	$search->equalTo("name", $name);
	$count = $search->find();

	require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attachment_id = media_handle_upload( $image, 55 );
    // if ( is_wp_error( $attachment_id ) ) {
        // $image_url='error';
    // } else {
    // GET THUMBNAIL URL
	$thumbnail_attributes = wp_get_attachment_image_src( $attachment_id ,'thumbnail', false);
	$thumbnail_url = $thumbnail_attributes[0];
	// GET ORIGINAL IMAGE URL
    $normal_attributes = wp_get_attachment_image_src( $attachment_id ,'full', false);
	$normal_url = $normal_attributes[0];

    $image_object = ["thumbnail" => $thumbnail_url,	"normal" => $normal_url];

	if(count($count)==1){
		$result['status']='found';
	}else{
		$query = new ParseObject("RewardProduct");
		$query->set("name", $name);
		$query->set("points", (int)$points);
		$query->set("type", $type);
		$query->set("description", $description);
		$query->setAssociativeArray("images",$image_object);
		$query->set("isAvailable", (boolean)$available);
		if($type=="product"){
			$query->set("brand",$reward);
		}else{
			$query->set("voucherValue",(int)$reward);
		}
		$query->set("isDeleted", false);
		$query->save();
		$result['status']='success';
	}
	return $result;
}

// VIEW REWARD
function viewRewardProduct($id) {
	$query = new ParseQuery("RewardProduct");
	$query->equalTo("objectId", $id);
	$count = $query->find();
	
	for ($i = 0; $i < count($count); $i++) {
		$object = $count[$i];

		$image=$object->get('images');
		
		$thumbnail= (array)$image;
		
		if($image){
			$result['image'] = $thumbnail['normal'];
		}else{
			$result['image'] = "";
		}
  	  	$result['name']=$object->get('name'); 	 	 	
  	  	$result['description']=$object->get('description'); 	 	 	
  	  	$result['points']=$object->get('points'); 	 	
  	  	$result['type']=$object->get('type'); 	 	
  	  	$result['brand']=$object->get('brand'); 	 	
  	  	$result['voucher']=$object->get('voucherValue'); 	 	
  	  	$result['available']=$object->get('isAvailable'); 	 	
	}
	return $result;
}

// UPDATE REWARD PRODUCT
// updateRewardProduct("DrNyBVGmPG","names","121","voucher","dscsp","222","1");
function updateRewardProduct($id,$name,$points,$type,$description,$reward,$available,$image) {
	$query = new ParseQuery("RewardProduct");
	$query->equalTo("name",$name);
	$count = $query->find();
	require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attachment_id = media_handle_upload( $image, 55 );
	$thumbnail_attributes = wp_get_attachment_image_src( $attachment_id ,'thumbnail', false);
	$thumbnail_url = $thumbnail_attributes[0];
	// GET ORIGINAL IMAGE URL
    $normal_attributes = wp_get_attachment_image_src( $attachment_id ,'full', false);
	$normal_url = $normal_attributes[0];

    $image_object = ["thumbnail" => $thumbnail_url,	"normal" => $normal_url];
	// FIND SAME NAME
	if(count($count)==1){
		for ($i = 0; $i < count($count); $i++) {
	  		$foundId=$count[$i]->getObjectId();
	  		// NO SAME NAME FOUND
			if($foundId==$id){
				if($image=="no_image"){
					$count[$i]->set("name", $name);
					$count[$i]->set("description",$description);
					$count[$i]->set("points",(int)$points);
					$count[$i]->set("isAvailable",(boolean)$available);
					$count[$i]->set("type",$type);
					if($type=="product"){
						$count[$i]->set("brand",$reward);
					}else{
						$count[$i]->set("voucherValue",(int)$reward);
					}
					$count[$i]->save();
					$result['status']='success';
				}else{
					$count[$i]->set("name", $name);
					$count[$i]->set("description",$description);
					$count[$i]->set("points",(int)$points);
					$count[$i]->setAssociativeArray("images",$image_object);
					$count[$i]->set("isAvailable",(boolean)$available);
					$count[$i]->set("type",$type);
					if($type=="product"){
						$count[$i]->set("brand",$reward);
					}else{
						$count[$i]->set("voucherValue",(int)$reward);
					}
					$count[$i]->save();
					$result['status']='success';
				}
				
			}
			// NAME ALREADY EXIST IN THE DATABASE
			else{
				$result['status']='exist';
			}
		}
	}else{
		$query = new ParseQuery("RewardProduct");
		$query->equalTo("objectId", $id);
		$count = $query->find();
		for ($i = 0; $i < count($count); $i++) {
			if($image=="no_image"){
				$count[$i]->set("name", $name);
				$count[$i]->set("description",$description);
				$count[$i]->set("points",(int)$points);
				$count[$i]->set("isAvailable",(boolean)$available);
				$count[$i]->set("type",$type);
				if($type=="product"){
					$count[$i]->set("brand",$reward);
				}else{
					$count[$i]->set("voucherValue",(int)$reward);
				}
				$count[$i]->save();
				$result['status']='success';
			}else{
				$count[$i]->set("name", $name);
				$count[$i]->set("description",$description);
				$count[$i]->set("points",(int)$points);
				$count[$i]->set("isAvailable",(boolean)$available);
				$count[$i]->setAssociativeArray("images",$image_object);
				$count[$i]->set("type",$type);
				if($type=="product"){
					$count[$i]->set("brand",$reward);
				}else{
					$count[$i]->set("voucherValue",(int)$reward);
				}
				$count[$i]->save();
				$result['status']='success';
			}
			
		}
	}
	// var_dump($result);
	return $result;
}

// DELETE REWARD
function deleteRewardProduct($id) {
	$query = new ParseQuery("RewardProduct");
	$query->equalTo("objectId",$id);
	$count=$query->find();

	for ($i=0; $i <count($count); $i++) { 
		$obj=$count[$i];
		$obj->set("isDeleted",true);
		$obj->save();
		$result['status']='success';
	}
	return $result;
}

// ================================REDEMPTION=================
// DISPLAY REDEMPTION TABLE
function displayRedemptionTable() {
	$query = new ParseQuery("Redemption");
	$query->includeKey("user");
	$query->includeKey("product");
	$query->descending("createdAt");
	$query->equalTo("isDeleted",false);
	$result=$query->find();
	// LOOP TO DISPLAY OBJECT CONTENT
	echo"<thead class='bg-primary'>
			<tr>
			<th></th>
			<th>USER</th>
			<th>ADDRESS</th>
			<th>PRODUCT</th>
			<th>TYPE</th>
			<th>VOUCHER VALUE</th>
			<th>STATUS</th>
			<th>POINTS</th>
			<th data-orderable='false'>ACTIONS</th>
			</tr>
		</thead>
		<tbody>";
	for ($i=0; $i < count($result); $i++) { 
		$obj=$result[$i];
	
		$dateCreated=$obj->getCreatedAt()->format('M d Y - H:i a');
		$dateUpdated=$obj->getUpdatedAt()->format('M d Y - H:i a');
		$getVoucher=$obj->get("voucherValue");
		if($getVoucher){
			$voucher=$getVoucher;
		}else{
			$voucher="0";
		}

		$getType=$obj->get("type");
		$getStatus=$obj->get('status');
		if($getStatus=="pending"){
			$action = '<button viewRedemptionAttr="'.$obj->getObjectId().'" class="btn btn-warning view-redemption">
						<span data-toggle="tooltip" data-placement="top" title="View Redemption" class="glyphicon glyphicon-eye-open"></span>
					   </button>&nbsp;
					   <button updateAttr="'.$obj->getObjectId().'" typeAttr="'.$getType.'" redemptionStatusAttr="accepted" class="btn btn-success update-redemption">
						<span data-toggle="tooltip" data-placement="top" title="Accept" class="glyphicon glyphicon-ok-sign"></span>
					   </button>&nbsp;
					   <button updateAttr="'.$obj->getObjectId().'" typeAttr="'.$getType.'" redemptionStatusAttr="declined" class="btn btn-danger update-redemption">
						<span data-toggle="tooltip" data-placement="top" title="Decline" class="glyphicon glyphicon-remove-sign"></span>
					   </button>';
		}else{
			$action = '';
		}
		
		// $productType=$obj->get("type");
		// if($productType=="product"){
		// 	$product="<a href='#'>".$obj->get("product")->get("name")."</a>";
		// }else{
		// 	$product=$obj->get("product")->get("name");
		// }
		echo'<tr>
				<td></td>
				<td><a viewUserAttr="'.$obj->get("user")->getObjectId().'" class="view-user-details" href="#">'.$obj->get("user")->get("firstName")." ".$obj->get("user")->get("lastName").'</a></td>
				<td>'.$obj->get("address").'</td>
				<td><a viewAttr="'.$obj->get('product')->getObjectId().'" class="view-reward-product" href="#">'.$obj->get("product")->get("name").'</a></td>
				<td>'.$obj->get("type").'</td>
				<td>'.$voucher.'</td>
				<td>'.$obj->get("status").'</td>
				<td>'.$obj->get('points').'</td>';
		echo'<td class="text-right">'.$action.'</td>
			</tr>';
	}
	echo "</tbody>";
}

// VIEW REDEMPTION
function viewRedemption($id) {
	$query = new ParseQuery("Redemption");
	$query->equalTo("objectId", $id);
	$count = $query->find();
	
	for ($i = 0; $i < count($count); $i++) {
		$object = $count[$i];
  	  	$result['type']=$object->get('type'); 	 	 	
  	  	$result['status']=$object->get('status'); 	 	 	
  	  	$result['points']=$object->get('points'); 	 	 	  	 	 	
	}
	return $result;
}

// UPDATE REDEMPTION
function updateRedemption($id,$type,$status) {
	$query = new ParseQuery("Redemption");
	$query->equalTo("objectId", $id);
	$count = $query->find();
	for ($i = 0; $i < count($count); $i++) {
		$count[$i]->set("status", $status);
		$count[$i]->save();
		$success=true;
	}

	if($success){
		// run sendVoucherCode function only if accepted and voucher type
		if($status=="accepted" && $type=="voucher"){
				//call cloud code function 
				$send = ParseCloud::run("sendVoucherCode", ["reward" => $id ]);
				if($send=="Voucher successfully added"){
					$result['status']='success';
				}else{
					$result['status']='failed';
				}
		}else{
			$result['status']='success';
		}
	}else{
		$result['status']='failed';
	}
	return $result;
}

// DELETE REDEMPTION
function deleteRedemption($id) {
	$query = new ParseQuery("Redemption");
	$query->equalTo("objectId",$id);
	$count=$query->find();

	for ($i=0; $i <count($count); $i++) { 
		$obj=$count[$i];
		$obj->set("isDeleted",true);
		$obj->save();
		$result['status']='success';
	}
	return $result;
}

// ===============================================SETTING=================================
// DISPLAY SETTING
function displaySettings() {
	$query = new ParseQuery("Settings");
	// LOOP TO DISPLAY OBJECT CONTENT
	$query->each(function($obj) {

		echo'<div class="well">
				<h1>Latest Version&nbsp;
				<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success edit-version-settings">
					<span class="glyphicon glyphicon-edit" ></span> EDIT
				</button></h1>
				<hr>				
				<p><h3>'.$obj->get('latestVersion').'</h3></p>
			</div>
			<div class="well">
				<h1>App Store URL&nbsp;
				<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success edit-appstore-settings">
					<span class="glyphicon glyphicon-edit" ></span> EDIT
				</button></h1>
				<hr>				
				<p><h3><a target="_blank" href='.$obj->get('appStoreUrl').'>'.$obj->get('appStoreUrl').'</a></h3></p>
			</div>
			<div class="well">
				<h1>Google Play URL&nbsp;
				<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success edit-googleplay-settings">
					<span class="glyphicon glyphicon-edit" ></span> EDIT
				</button></h1>
				<hr>				
				<p><h3><a target="_blank" href='.$obj->get('googlePlayUrl').'>'.$obj->get('googlePlayUrl').'</a></h3></p>
			</div>
			<div class="well">
				<h1>Contact Number&nbsp;
				<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success edit-contact-settings">
					<span class="glyphicon glyphicon-edit" ></span> EDIT
				</button></h1>
				<hr>				
				<h3>'.$obj->get('contactNumber').'</h3>
			</div>
			<div class="well">
				<h1>About The Application&nbsp;
				<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success edit-about-settings">
					<span class="glyphicon glyphicon-edit" ></span> EDIT
				</button></h1>
				<hr>				
				'.$obj->get('aboutTheApp').'
			</div>
			<div class="well">
				<h1>How It Works&nbsp;
				<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success edit-how-settings">
					<span class="glyphicon glyphicon-edit" ></span> EDIT
				</button></h1>
				<hr>				
				'.$obj->get('howItWorks').'
			</div>
			<div class="well">
				<h1>Terms And Conditions&nbsp;
				<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success edit-terms-settings">
					<span class="glyphicon glyphicon-edit" ></span> EDIT
				</button></h1>
				<hr>	
				'.$obj->get('termsAndCondition').'
			</div>
			<div class="well">			
				<h1>Privacy Policy&nbsp;
				<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success edit-policy-settings">
					<span class="glyphicon glyphicon-edit" ></span> EDIT
				</button></h1>
				<hr>	
				'.$obj->get('privacyPolicy').'
			</div>
			<div class="well">
				<h1>Warranty&nbsp;
				<button updateAttr="'.$obj->getObjectId().'" class="btn btn-success edit-warranty-settings">
					<span class="glyphicon glyphicon-edit" ></span> EDIT
				</button></h1>
				<hr>				
				'.$obj->get('warranty').'
			</div>';
	});
}

// DISPLAY ABOUT APP FOR WYSIWYG
function displayAboutAppSettings(){
	$query = new ParseQuery("Settings");
	$result=$query->find();

	for ($i=0; $i < count($result); $i++) { 
		$obj=$result[$i];
		$about = $obj->get('aboutTheApp');
	}
	return $about;

}

// DISPLAY HOW IT WORKS FOR WYSIWYG
function displayHowItWorksSettings(){
	$query = new ParseQuery("Settings");
	$result=$query->find();

	for ($i=0; $i < count($result); $i++) { 
		$obj=$result[$i];
		$how = $obj->get('howItWorks');
	}
	return $how;

}

// DISPLAY TERMS AND CONDITION FOR WYSIWYG
function displayTermsConditionsSettings(){
	$query = new ParseQuery("Settings");
	$result=$query->find();

	for ($i=0; $i < count($result); $i++) { 
		$obj=$result[$i];
		$terms = $obj->get('termsAndCondition');
	}
	return $terms;

}

// DISPLAY PRIVACY POLICY FOR WYSIWYG
function displayPrivacyPolicySettings(){
	$query = new ParseQuery("Settings");
	$result=$query->find();

	for ($i=0; $i < count($result); $i++) { 
		$obj=$result[$i];
		$policy = $obj->get('privacyPolicy');
	}
	return $policy;

}

// DISPLAY WARRANTY FOR WYSIWYG
function displayWarrantySettings(){
	$query = new ParseQuery("Settings");
	$result=$query->find();

	for ($i=0; $i < count($result); $i++) { 
		$obj=$result[$i];
		$warranty = $obj->get('warranty');
	}
	return $warranty;
}


// ADD SETTINGS
// function addSettings($terms,$policy,$warranty) {
// 	$search = new ParseQuery("Settings");
// 	$search->equalTo("termsAndCondition", $terms);
// 	$count = $search->find();

// 	if(count($count)==1){
// 		$result['status']='found';
// 	}else{
// 		$query = new ParseObject("Settings");
// 		$query->set("termsAndCondition", $terms);
// 		$query->set("privacyPolicy", $policy);
// 		$query->set("warranty", $warranty);
// 		$query->set("isDeleted",false);
// 		$query->save();
// 		$result['status']='success';
// 	}
// 	return $result;
// }

// VIEW SETTINGS
function viewSettings($id) {
	$query = new ParseQuery("Settings");
	$query->equalTo("objectId", $id);
	$count = $query->find();
	
	for ($i = 0; $i < count($count); $i++) {
		$object = $count[$i];
  	  	$result['version']=$object->get('latestVersion'); 	 	 	
  	  	$result['appstore']=$object->get('appStoreUrl'); 	 	 	
  	  	$result['googleplay']=$object->get('googlePlayUrl'); 	 	 	
  	  	$result['about']=$object->get('aboutTheApp'); 	 	 	
  	  	$result['terms']=$object->get('termsAndCondition'); 	 	 	
  	  	$result['policy']=$object->get('privacyPolicy'); 	 	 	
  	  	$result['warranty']=$object->get('warranty');
  	  	$result['number']=$object->get('contactNumber');
	}
	return $result;
}

// UPDATE VERSION SETTINGS
function updateVersionSettings($id,$version){
		
	$query = new ParseQuery("Settings");
	$query->equalTo("objectId", $id);
	$count = $query->find();
	for ($i = 0; $i < count($count); $i++) {
		$count[$i]->set("latestVersion", $version);
		$count[$i]->save();
		$result['status']='success';
	}
	return $result;
}

// UPDATE CONTENT SETTINGS
function updateContentSettings($id,$content,$field){
	$query = new ParseQuery("Settings");
	$query->equalTo("objectId", $id);
	$count = $query->find();
	for ($i = 0; $i < count($count); $i++) {
		$count[$i]->set($field, $content);
		$count[$i]->save();
		$result['status']='success';
	}
	return $result;
}

// DELETE SETTINGS
// function deleteSettings($id) {
// 	$query = new ParseQuery("Settings");
// 	$query->equalTo("objectId",$id);
// 	$count=$query->find();

// 	for ($i=0; $i <count($count); $i++) { 
// 		$obj=$count[$i];
// 		$obj->set("isDeleted",true);
// 		$obj->save();
// 		$result['status']='success';
// 	}
// 	return $result;
// }


?>
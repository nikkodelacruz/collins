<?php get_header('admin'); ?>
<div class="container">
  <!--ADD SURVEY MODAL -->
  <div class="modal fade" id="addSurveyModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h3 class="modal-title">ADD SURVEY</h3>
        </div>
        <div class="modal-body">
        	<p><span class="loading add-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
			<div class="alert alert-danger alert-dismissable fade in add-failed">
	    		<a href="#" class="close close-alert" >&times;</a>
	    		<strong>Failed! </strong> <span class="error-message"></span>
	  		</div>
			<form action="" method="post">
				<div class="form-group">
					<label>Survey title:</label>
					<input type="text" class="form-control add-survey-title">
				</div>
				<div class="form-group">
					<label>Points:</label>
					<input type="number" class="form-control add-survey-points">
				</div>
				
				<!-- <div class="form-group">
					<label>Active:</label>
				     <p><input type="checkbox" class="add-survey-active"> <span class="add-survey-label"></span></p>
				</div> -->
				<input type="submit" class="btn btn-info add-survey" value="ADD">
			</form>
        </div>
      </div>
    </div>
  </div><!--ADD QUESTION Modal --> 

  <!--ADD QUESTION MODAL -->
  <div class="modal fade" id="addQuestionModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h3 class="modal-title">ADD QUESTION</h3>
        </div>
        <div class="modal-body">
        	<p><span class="loading add-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
			<div class="alert alert-danger alert-dismissable fade in add-failed">
	    		<a href="#" class="close close-alert" >&times;</a>
	    		<strong>Failed! </strong> <span class="error-message"></span>
	  		</div>
			<form action="" method="post">
				<input type="hidden" class="survey-id">
				<!-- <div class="form-group">
					<label>Choose survey title:</label>
					<?php //viewSurveyTitle(); ?>
				</div> -->
				<div class="form-group">
					<label>Survey question:</label>
					<input type="text" class="form-control add-survey-question">
					<br>
					<label>Question choices:</label>
					<p>
						<button class="btn btn-success add-survey-choices"><span class="glyphicon glyphicon-plus"></span> Add choices</button>	
						<button class='btn btn-danger remove-survey-choices'><span class='glyphicon glyphicon-remove'></span> Remove choices</button>
					</p>
					<div class="form-group">
						<table class="append-question-choices">
							<tr>
								<td width='5'>
									1.&nbsp;
								</td>
								<td>
									<input type="text" name="add-survey-label[]" class="form-control add-survey-label" placeholder="label">
								</td>
								<td>
									<input type="text" name="add-survey-value[]" class="form-control add-survey-value" placeholder="value">
								</td>
							</tr>
						</table>	
					</div>
				</div>
				<input type="submit" class="btn btn-info add-question" value="ADD">
			</form>
        </div>
      </div>
    </div>
  </div><!--ADD QUESTION Modal -->
  

   <!-- UPDATE MODAL -->
  <div class="modal fade" id="updateSurveyModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE INFORMATION</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<p><span class="loading update-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<div class="form-group">
				  <input type="hidden" class="object-id">
				  <label>Survey title:</label>
				  <input type="text" class="form-control update-survey-title">
				</div>
				<div class="form-group">
				  <label>Points:</label>
				  <input type="number" class="form-control update-survey-points" >
				</div>
				<div class="form-group">
				<label>Status:</label>
				    <p><input type="checkbox" class="update-survey-active"> <span class="update-survey-label"></span></p>
				</div>
				<input type="submit" class="btn btn-info save-survey" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div><!--Update Modal -->

	<!-- VIEW SURVEY MODAL -->
 <!--  <div class="modal modal-history fade" id="viewSurveyQuestionModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-warning">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">SURVEY INFORMATION</h2>
        </div>
        <div class="modal-body survey-question table-responsive">
		 --><!-- CONTENT -->
       <!-- 
        </div>
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div> -->
  <!--View SURVEY MODAL -->

	<!-- VIEW SURVEY PARTICIPANTS MODAL -->
  <div class="modal fade" id="viewSurveyParticipantsModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">SURVEY PARTICIPANTS</h2>
        </div>
        <div class="modal-body survey-participants table-responsive">
		<!-- CONTENT -->
       
        </div>
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!-- VIEW SURVEY PARTICIPANTS MODAL -->

<!-- VIEW USER DETAILS MODAL -->
  <div class="modal fade" id="viewUserModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-warning">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">USER'S DETAILS</h2>
        </div>
        <div class="modal-body">
			<!-- CONTENT -->
        
			<ul class="list-group">
			  <li class="text-center">
			 	<img class="display-image img-rounded img-thumbnail" src="" alt="no image" width="200" height="200">
			  </li>
			  <br>
			  <li class="list-group-item">
			  	<span class="display-info" >Username:</span> <b class="display-username"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Firstname:</span> <b class="display-firstname"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Lastname:</span> <b class="display-lastname"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Birth month:</span> <b class="display-birth"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Age:</span> <b class="display-age"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Gender:</span> <b class="display-gender"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Email:</span> <b class="display-email"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Address:</span> <b class="display-address"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Contact number:</span> <b class="display-number"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Province:</span> <b class="display-province"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">City:</span> <b class="display-city"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Barangay:</span> <b class="display-barangay"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Points:</span> <b class="display-points"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Number of items:</span> <b class="display-items"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Pending Redeem Points:</span> <b class="display-redeem"></b>
			  </li>
			</ul>
        </div>
        
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View USER DETAILS Modal -->	

  <!-- VIEW SURVEY ANSWER MODAL -->
  <div class="modal fade" id="viewSurveyAnswerModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">SURVEY ANSWERS</h2>
        </div>
        <div class="modal-body survey-answer table-responsive">
		<!-- CONTENT -->
       
        </div>
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View SURVEY ANSWER MODAL -->

  	<!-- VIEW SURVEY QUESTION MODAL -->
  <div class="modal fade" id="viewSurveyQuestionModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-warning">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h3 class="modal-title">QUESTION INFORMATION</h3>
        </div>
        <div class="modal-body">
			<div class="form-group display-survey-question">
				  
			</div>			
        </div>
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--VIEW SURVEY QUESTION MODAL -->  	

  <!-- UPDATE CHOICES MODAL -->
  <div class="modal fade" id="updateSurveyQuestionChoicesModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE QUESTION</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<p><span class="loading update-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<div class="form-group display-survey-question-choices">
				</div>
				<input type="submit" class="btn btn-info save-survey-question-choices" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div><!--Update Choices Modal -->

</div>
<!-- DISPLAY TABLE CONTENT -->
<div class="container-fluid">
	<p class="wait" >
		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span> Please wait...</span>
	</p>
	<h2>Surveys&nbsp;<button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addSurveyModal"><span class="glyphicon glyphicon-plus"></span> Add New</button></h2>
	<br>
	<p>
		<label>Filter:</label>
		<select class="filterSurveyStatus">
			<option value="">All status</option>
			<option value="yes">Active</option>
			<option value="no">Inactive</option>
		</select>
	</p>
	<br>
	<div class="table-responsive">
		<table class="table table-striped table-hover table-fixed text-center">
			<?php displaySurveyTable(); ?>
	 	</table>
	</div>
	<!-- TABLE QUESTION CONTENT -->
	<div class="table-question">
		
	</div>
</div>
<?php get_footer('admin'); ?>


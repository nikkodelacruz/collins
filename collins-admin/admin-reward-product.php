<?php get_header('admin'); ?>
<div class="container">
	<!-- ADD MODAL -->
  <div class="modal fade" id="addRewardProductModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">ADD REWARD PRODUCT</h4>
        </div>
        <div class="modal-body">
			<p><span class="loading add-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
			<div class="alert alert-danger alert-dismissable fade in add-failed">
	    		<a href="#" class="close close-alert">&times;</a>
	    		<strong>Failed! </strong> <span class="error-message"></span>
	  		</div>
			<form action="" method="post" >
				<div class="form-group">
				  <label>Reward product name:</label>
				  <input type="text" class="form-control add-reward-product-name">
				</div>
				<div class="form-group">
				  <label>Description:</label>
				  <textarea class="form-control add-reward-product-description" rows="3"></textarea>
				</div>
				<div class="form-group">
				  <label>Type:</label>
				  <select class="form-control reward-product-type">
				  	<option value="select">Please select a reward type</option>
				  	<option value="product">Product</option>
				  	<option value="voucher">Voucher</option>
				  </select>
				</div>
				<div class="form-group reward-type">
				</div>
				<div class="form-group">
				  <label>Points:</label>
				  <input type="number" class="form-control add-reward-product-points">
				</div>
				<div class="form-group">
				  <label>Image:</label>
				  <input type="file" id="add_brand_product_image" class="form-control add-reward-product-image" >
				</div>
				<div class="form-group">
				<label>Status:</label>
				  <!-- <label class="checkbox-inline"> -->
				      <p><input type="checkbox" class="add-reward-product-available"> <span class="available-label"></span></p>
				  <!-- </label> -->
				</div>
				<input type="submit" class="btn btn-info add-reward-product" value="ADD">
			</form>
        </div>
        
      </div>
    </div>
  </div><!--ADD Modal -->

  <!-- VIEW MODAL -->
  <div class="modal fade" id="viewRewardProductModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-warning">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">Reward Product Information</h2>
        </div>
        <div class="modal-body">
			<!-- CONTENT -->
			<ul class="list-group">
			  <li class="list-group-item text-center">
			 	<img class="display-image img-responsive" src="" alt="no image">
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Reward product name:</span> <b class="display-name"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Description:</span> <p class="display-description"></p>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Reward Type:</span> <b class="display-type"></b>
			  </li>
			  <li class="list-group-item">
			  	<div class="display-reward-type">
			  		
			  	</div>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Points:</span> <b class="display-points"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Status:</span> <b class="display-available"></b>
			  </li>
			</ul>
        </div>
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View Modal -->

  <!-- UPDATE MODAL -->
  <div class="modal fade" id="updateRewardProductModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE INFORMATION</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<p><span class="loading update-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<div class="form-group">
					<input type="hidden" class="object-id">
				  <label>Reward product name:</label>
				  <input type="text" class="form-control update-reward-product-name">
				</div>
				<div class="form-group">
				  <label>Description:</label>
				  <textarea class="form-control update-reward-product-description" rows="3"></textarea>
				</div>
				<div class="form-group">
				  <label>Type:</label>
				  <select class="form-control update-reward-product-type">
				  	<option value="product">Product</option>
				  	<option value="voucher">Voucher</option>
				  </select>
				</div>
				<div class="form-group show-reward-type">
				</div>
				<div class="form-group">
				  <label>Points:</label>
				  <input type="number" class="form-control update-reward-product-points">
				</div>
				<div class="form-group">
				  <label>Image:</label>
				  <input type="file" id="update_reward_product_image" class="form-control update-reward-product-image" >
				</div>
	
				<div class="form-group">
				<label>Status:</label>
				  <!-- <label class="checkbox-inline"> -->
				      <p><input type="checkbox" class="update-reward-product-available"> <span class="update-available-label"></span></p>
				  <!-- </label> -->
				</div>
				<input type="submit" class="btn btn-info save-reward-product" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div><!--UPDATE Modal -->
</div>

<!-- DISPLAY TABLE CONTENT -->
<div class="container-fluid">
	<div class="wait" >
		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span> Please wait...</span>
	</div>
	<h2>Reward Products&nbsp;<button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addRewardProductModal"><span class="glyphicon glyphicon-plus addd"></span> Add New</button></h2>
	<br>
	<p>
		<label>Filter:</label>
		<select class="filterType">
			<option value="">All Types</option>
			<option value="product">Product</option>
			<option value="voucher">Voucher</option>
		</select>
		<select class="filterAvailable">
			<option value="">All Status</option>
			<option value="yes">Available</option>
			<option value="no">Unavailable</option>
		</select>
	</p>
	<br>
	<div class="table-responsive">
		<table class="table table-striped table-hover table-fixed text-center">
			<?php displayRewardProductTable(); ?>
	 	</table>
	</div>
</div>
<?php get_footer('admin'); ?>

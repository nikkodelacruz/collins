<?php get_header('admin'); ?>
<div class="container">

 <!-- update MODAL -->
  <div class="modal fade" id="updateSCAModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE SERVICE CENTER APPOINTMENT</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post" >
				<p><span class="loading update-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<div class="form-group">
				  <input type="hidden" class="object-id">
				  <label>Status:</label>
				  <select class="form-control update-sca-status">
		 			<option value="pending">Pending</option>
		 			<option value="accepted">Accepted</option>
		 			<option value="declined">Declined</option>
				  </select>
				</div>
				<div class="form-group">
				  <input type="hidden" class="object-id">
				  <label>Points:</label>
				  <input type="number" class="form-control update-sca-points">
				</div>
				
				<input type="submit" class="btn btn-info save-sca" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div><!--update Modal -->

  <!-- VIEW APPOINTMENT MODAL -->
  <div class="modal fade" id="viewServiceCenterAppointmentModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-warning">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">Service Center Appointment Information</h2>
        </div>
        <div class="modal-body">
			<!-- CONTENT -->
			<ul class="list-group">
			  <li class="list-group-item">
			  	<span class="display-info">Service center:</span> <b class="display-name"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">User:</span> <b class="display-user"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">User email:</span> <b class="display-email"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Contact number:</span> <b class="display-number"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Status:</span> <b class="display-status"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Points:</span> <b class="display-points"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Problem:</span> <p class="display-problem"></p>
			  </li>
			</ul>
        </div>
        
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View APPOINTMENT Modal -->

  <!-- VIEW USER DETAILS MODAL -->
  <div class="modal fade" id="viewUserModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-warning">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">USER'S DETAILS</h2>
        </div>
        <div class="modal-body">
			<!-- CONTENT -->
        
			<ul class="list-group">
			  <li class="text-center">
			 	<img class="display-image img-rounded img-thumbnail" src="" alt="no image" width="200" height="200">
			  </li>
			  <br>
			  <li class="list-group-item">
			  	<span class="display-info" >Username:</span> <b class="display-username"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Firstname:</span> <b class="display-firstname"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Lastname:</span> <b class="display-lastname"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Birth month:</span> <b class="display-birth"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Age:</span> <b class="display-age"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Gender:</span> <b class="display-gender"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Email:</span> <b class="display-email"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Address:</span> <b class="display-address"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Contact number:</span> <b class="display-number"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Province:</span> <b class="display-province"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">City:</span> <b class="display-city"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Barangay:</span> <b class="display-barangay"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Points:</span> <b class="display-points"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Number of items:</span> <b class="display-items"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Pending Redeem Points:</span> <b class="display-redeem"></b>
			  </li>
			</ul>
        </div>
        
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View USER DETAILS Modal -->

  <!-- VIEW SERVICE CENTER MODAL -->
  <div class="modal fade" id="viewServiceCenterModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-warning">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">Service Center Information</h2>
        </div>
        <div class="modal-body">
			<!-- CONTENT -->
			<ul class="list-group">
			  <li class="list-group-item">
			  	<span class="display-info">Service center name:</span> <b class="display-name"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Address:</span> <b class="display-address"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Contact number:</span> <b class="display-number"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Email:</span> <b class="display-email"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Working days:</span> <b class="display-wd"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Working hours:</span> <b class="display-wh"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Location:</span>
			  	<!-- <input type="text" class="form-control display-map-address"> <br> -->
			  	<div class="display-map" class="form-control" style="height: 400px;"></div>
				Longitude: <input type="text" class="form-control display-latitude" readonly>
				Latitude: <input type="text" class="form-control display-longitude" readonly>
			  </li>
			</ul>
        </div>
        
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View SERVICE CENTER Modal -->

	<!-- VIEW PRODUCT MODAL -->
  <div class="modal fade" id="viewProductAppointmentModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-info">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">PRODUCT INFORMATION</h2>
        </div>
        <div class="modal-body product-appointment	">
		<!-- CONTENT -->
        </div>
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View PRODUCT Modal -->

    <!-- ADD SCORE MODAL -->
  <div class="modal fade" id="addScoreModal" role="dialog" >
    <div class="modal-dialog modal-sm" style="margin-top: 10%;">
      <div class="modal-content panel-default">
        <div class="modal-body">
			<form action="" method="post">
				<p><span class="loading update-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<div class="form-group">
					<input type="hidden" class="appointment-object-id">
				  	<label>Points:</label>
				  	<input type="number" class="form-control add-points-accept">
				</div>
				<input type="submit" class="btn btn-info save-points-accept" value="ADD">
				<input type="submit" class="btn btn-danger" data-dismiss="modal" value="CLOSE">
			</form>
        </div>
      </div>
    </div>
  </div> <!-- ADD SCORE MODAL -->

</div>
<!-- DISPLAY TABLE CONTENT -->
<div class="container-fluid">
	<!-- <p><button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addServiceCenterModal"><span class="glyphicon glyphicon-plus addd"></span> ADD NEW SERVICE CENTER</button></p> -->
	<div class="wait" >
		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span> Please wait...</span>
	</div>
	<h2>Service Center Appointments</h2>
	<br>
	<div class="table-responsive">
		<p>
			<label>Filter:</label>
			<select class="filterServiceCenter">
	 			<?php filterServiceCenter(); ?>
			</select>	
			<select class="filterAppointmentStatus">
				<option value="">All Status</option>
	 			<option selected="selected" value="pending">Pending</option>
	 			<option value="accepted">Accepted</option>
	 			<option value="declined">Declined</option>
			</select>
		</p>
		<br>
		<table class="table table-striped table-hover table-fixed text-center">
			<?php displayServiceCenterAppointmentTable(); ?>
	 	</table>
	</div>
</div>
<?php get_footer('admin'); ?>


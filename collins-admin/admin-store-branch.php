<?php get_header('admin'); ?>
<div class="container">
  <!-- ADD MODAL -->
  <div class="modal fade" id="addStoreBranchModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">ADD STORE BRANCH</h4>
        </div>
        <div class="modal-body">
        	<p><span class="loading add-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
			<div class="alert alert-danger alert-dismissable fade in add-failed">
	    		<a href="#" class="close close-alert" >&times;</a>
	    		<strong>Failed! </strong> <span class="error-message"></span>
	  		</div>
			<form action="" method="post">
				<div class="form-group">
					<label>Store branch name:</label>
					<input type="text" class="form-control add-store-branch-name">
				</div>
				<div class="form-group">
					<label>Store name:</label>
					<?php displayStoreName(); ?>
				</div>
				<input type="submit" class="btn btn-info add-store-branch" value="ADD">
			</form>
        </div>
      </div>
    </div>
  </div><!--ADD Modal -->
  <!-- UPDATE MODAL -->
  <div class="modal fade" id="updateStoreBranchModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE STORE</h4>
        </div>
        <div class="modal-body">
			<p><span class="loading update-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
			<div class="alert alert-danger alert-dismissable fade in update-failed">
	    		<a href="#" class="close close-alert" >&times;</a>
	    		<strong>Failed! </strong> <span class="error-message"></span>
	  		</div>
			<form action="" method="post">
				<div class="form-group">
					<input type="hidden" class="object-id">
					<label>Store branch name:</label>
					<input type="text" class="form-control update-store-branch-name">
				</div>
				<div class="form-group">
					<label>Store name:</label>
					<i class="fa fa-spinner fa-spin wait-category"></i>
					<div class="update-store-name">
						
					</div>
				</div>
				<input type="submit" class="btn btn-info save-store-branch" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div><!--UPDATE Modal -->

</div>

<!-- DISPLAY TABLE CONTENT -->
<div class="container-fluid">
	<div class="wait" >
		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span> Please wait...</span>
	</div>
	<h2>Store Branches&nbsp;<button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addStoreBranchModal"><span class="glyphicon glyphicon-plus addd"></span> Add New</button></h2>
	<br>
	<p>
		<label>Filter:</label>
		<select class="filterStoreName">
			<?php filterStoreName(); ?>
		</select>	
	</p>
	<br>
	<div class="table-responsive">
		<table class="table table-striped table-hover table-fixed text-center">
			<?php displayStoreBranchTable(); ?>
	 	</table>
	</div>
</div>
<?php get_footer('admin'); ?>

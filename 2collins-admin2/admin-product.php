
<br>
<div class="container">

 <!-- VIEW MODAL -->
  <div class="modal fade" id="viewProductModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-warning">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">Product Information</h2>
        </div>
        <div class="modal-body">
			<!-- CONTENT -->
			<ul class="list-group">
			  <li class="text-center">
			 	<img class="display-image img-rounded img-thumbnail" src="" alt="no image" width="200" height="200">
			  </li>
			  <br>
			  <li class="list-group-item">
			  	<span class="display-info">User:</span> <b class="display-user"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Store name:</span> <b class="display-store"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Branch name:</span> <b class="display-branch"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Brand name:</span> <b class="display-brand"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Model name:</span> <b class="display-model"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Transaction model:</span> <b class="display-transaction"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Purchase date:</span> <b class="display-purchase"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Category:</span> <b class="display-category"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Serial number:</span> <b class="display-serial"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Quantity:</span> <b class="display-quantity"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Status:</span> <b class="display-status"></b>
			  </li>
			</ul>
        </div>
        
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View Modal -->

  <!-- VIEW DETAILS MODAL -->
  <div class="modal fade" id="viewUserModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-warning">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">USER'S DETAILS</h2>
        </div>
        <div class="modal-body">
			<!-- CONTENT -->
        
			<ul class="list-group">
			  <li class="text-center">
			 	<img class="display-image img-rounded img-thumbnail" src="" alt="no image" width="200" height="200">
			  </li>
			  <br>
			  <li class="list-group-item">
			  	<span class="display-info" >Username:</span> <b class="display-username"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Firstname:</span> <b class="display-firstname"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Lastname:</span> <b class="display-lastname"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Birth month:</span> <b class="display-birth"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Age:</span> <b class="display-age"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Gender:</span> <b class="display-gender"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Email:</span> <b class="display-email"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Address:</span> <b class="display-address"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Contact number:</span> <b class="display-number"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Province:</span> <b class="display-province"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">City:</span> <b class="display-city"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Barangay:</span> <b class="display-barangay"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Points:</span> <b class="display-points"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Number of items:</span> <b class="display-items"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Pending Redeem Points:</span> <b class="display-redeem"></b>
			  </li>
			</ul>
        </div>
        
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View DETAILS Modal -->

</div>

<!-- DISPLAY TABLE CONTENT -->
<div class="container-fluid">
	<!-- <p><button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#addProductModal"><span class="glyphicon glyphicon-plus addd"></span> ADD NEW PRODUCT</button></p> -->
	<div class="wait" >
		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span> Please wait...</span>
	</div>
	<p>
		<label>Filter:</label>
		<select class="filterStoreNameProduct">
			<?php filterStoreName(); ?>
		</select>
		<select class="filterBrandName">
			<?php filterBrandName(); ?>
		</select>	
		<select class="filterProductStatus">
			<option value="">All Status</option>
			<option selected="selected" value="pending">Pending</option>
		 	<option value="accepted">Accepted</option>
		 	<option value="declined">Declined</option>
		</select>
	</p>
	<div class="table-responsive">
		<table class="table table-striped table-hover table-fixed">
			<?php displayProduct(); ?>
	 	</table>
	</div>
</div>

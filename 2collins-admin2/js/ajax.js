jQuery(document).ready(function($){

    //TOOLTIP BS
    $('[data-toggle="tooltip"]').tooltip();
    //TIMEPICKER
    $('.add-sc-whf').ptTimeSelect();
    $('.add-sc-wht').ptTimeSelect();
    $('.update-sc-whf').ptTimeSelect();
    $('.update-sc-wht').ptTimeSelect();

    // DATE TIME PICKER
    // $('#datetimepicker1').datetimepicker();
    // $('#datetimepicker2').datetimepicker();

    // DATA TABLES
    // $('.tables').DataTable();
     var t = $('.table').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0 //unable search and order in col numbers
        } ]
        // "order": [[ 1, 'asc' ]] // order specific col

    } );
    
    // Col number
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    // FILTERING

    // FILTER STATUS APPOINTMENTS
    // $('.filterStatus').val('pending');
    // var pendingAppointment = $('.filterStatus').val();
    // t.columns(5).search( pendingAppointment ).draw();
    

    // FILTER BRAND NAME
    $('.filterBrandName').on('change', function () {
        t.columns(4).search( this.value ).draw();
    } );

    // FILTER STORE NAME
    $('.filterStoreName').on('change', function () {
        t.columns(1).search( this.value ).draw();
    } );

    // FILTTER SERVICE CENTER IN APPOINTMENTS
    $('.filterServiceCenter').on('change', function () {
        t.columns(1).search( this.value ).draw();
    } ); 
    // STATUS
    var filterAppointmentStatus = $('.filterAppointmentStatus').val();
    if(filterAppointmentStatus){
        t.columns(6).search( filterAppointmentStatus ).draw();
    }
    $('.filterAppointmentStatus').on('change', function () {
        t.columns(6).search( this.value ).draw();
    } ); 

    // FILTER PRODUCT STATUS
    var filterProductStatus = $('.filterProductStatus').val();
    if(filterProductStatus){
        t.columns(6).search( filterProductStatus ).draw();
    }

    $('.filterProductStatus').on('change', function () {
        t.columns(6).search( this.value ).draw();
    } );

    // FILTER REDEMPTION STATUS
    var filterRedemptStatus = $('.filterRedemptStatus').val();
    if(filterRedemptStatus){
        t.columns(6).search( filterRedemptStatus ).draw();
    }

    $('.filterRedemptStatus').on('change', function () {
        t.columns(6).search( this.value ).draw();
    } );

    // FILTER STORE NAME OF PRODUCT
    $('.filterStoreNameProduct').on('change', function () {
        t.columns(2).search( this.value ).draw();
    } ); 

    // FILTER TYPE
    $('.filterType').on('change', function () {
        t.columns(4).search( this.value ).draw();
    } );

    // FILTER AVAILABILITY
    $('.filterAvailable').on('change', function () {
        t.columns(8).search( this.value ).draw();
    } );

    // FILTER SURVEY
    $('.filterSurveyStatus').on('change', function () {
        t.columns(5).search( this.value ).draw();
    } );


    $(".add-load").fadeOut();
    $(".update-load").fadeOut();
    $(".wait").fadeOut();
    $(".wait-category").fadeOut();
    $(".add-failed").hide();
    $(".update-failed").hide();
    $(".close-alert").click(function(){
        $(".alert-danger").fadeOut();
    });

    // LOCATION PICKER
    $('#map').locationpicker({
    location: {
        latitude: 14.5995124,
        longitude: 120.9842195
    },
    radius: 0,
    zoom: 10,
    inputBinding: {
        latitudeInput: $('#map-latitude'),
        longitudeInput: $('#map-longitude'),
        // radiusInput: $('#us2-radius'),
        locationNameInput: $('#map-address')
    },
    enableAutocomplete: true
    });
    // INITIALIZE MAP
    $('#addServiceCenterModal').on('shown.bs.modal', function () {
        $('#map').locationpicker('autosize');
    });

    
   
// ========================================================USERS===================================

// VIEW USER RECORD
    $('.table tbody').on('click', '.view-user-details',function(){
        $(".wait").fadeIn();
        var objectId = $(this).attr("viewUserAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'objectId' : objectId,
                'action' : 'view_user'
            },
            success: function( data ) {
                // console.log( data.firstname );
                $(".wait").fadeOut();
                $('.display-username').text(data.username);
                $('.display-firstname').text(data.firstname);
                $('.display-image').attr('src',data.image);
                $('.display-lastname').text(data.lastname);
                $('.display-birth').text(data.birth);
                $('.display-age').text(data.age);
                $('.display-gender').text(data.gender);
                $('.display-email').text(data.email);
                $('.display-address').text(data.address);
                $('.display-province').text(data.province);
                $('.display-city').text(data.city);
                $('.display-barangay').text(data.barangay);
                $('.display-points').text(data.points);
                $('.display-number').text(data.number);
                $('.display-items').text(data.items);
                $('.display-redeem').text(data.redeem);
                $('#viewUserModal').modal('show');
            }
        });
        return false;
    });

// VIEW USER HISTORY
    $('.table tbody').on('click', '.view-user-history',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("viewUserHistoryAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'html',
            data: {
                'id' : id,
                'action' : 'view_user_history'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $(".user-history").html(data);
                $('#viewUserHistoryModal').modal('show');
                // userPoints();
            }
        });
        return false;
    });

    $('.table tbody').on('click', '.edit-user-points', function(){
        var id = $(this).attr("viewUserAttr");
        var points = $(this).attr("viewUserPointsAttr");
    
        $('.object-id').val(id);
        $('.user-points').val(points);
        $('#updateUserPointsModal').modal('show');
        
    });

    // Manipulate user points
    // add points
    $(".add-user-points").click(function(){
        var id = $('.object-id').val();
        var points = $('.edit-user-points').val();
        var type ="add";
            if(!points){
                $(".update-failed").fadeOut();
                $(".update-failed").fadeIn();
                $(".error-message").text("Points must not be empty");
            }
            else{
                $(".update-failed").hide();
                $(".wait").fadeIn();
                if(confirm("Are you sure you want to add this points?")){
                    $.ajax({
                        url: ajax_object.ajax_url,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'id' : id,
                            'points' : points,
                            'type' : type,
                            'action' : 'update_user_points'
                        },
                        success: function( data ) {
                            console.log( data.message );
                            $(".wait").fadeOut();
                            if(data.message=="success"){
                                $(".update-failed").fadeOut();
                                $(".wait").fadeOut(function(){
                                    alert("Successfully added points")
                                });
                                location.reload();
                            }else if(data.message=="failed"){
                                $(".update-failed").fadeOut();
                                $(".wait").fadeOut(function(){
                                    alert("Failed to add points")
                                });
                            }
                            else{
                                $(".wait").fadeOut(function(){
                                    alert("Failed to add points");
                                });
                            }
                        }
                    });
                }else{
                    $(".wait").hide();
                }

            }
            return false;  //PREVENT FORM TO LOAD
        }); 

    // deduct points
    $(".deduct-user-points").click(function(){
        var id = $('.object-id').val();
        var current_points = $('.user-points').val();
        var points = $('.edit-user-points').val();
        var type ="delete";
        // alert(typeof points);
        // alert(typeof current_points);
            if(!points){
                $(".update-failed").fadeOut();
                $(".update-failed").fadeIn();
                $(".error-message").text("Points must not be empty");
            }else if(points<0){
                $(".update-failed").fadeOut();
                $(".update-failed").fadeIn();
                $(".error-message").text("Points must not contain negative");
            }else if(parseInt(points) > parseInt(current_points)){//convert from string to int
                $(".update-failed").fadeOut();
                $(".update-failed").fadeIn();
                $(".error-message").text("Points to deduct must not higher than the current points");
            }else{
                $(".update-failed").hide();
                $(".wait").fadeIn();
                if(confirm("Are you sure you want to deduct this points?")){

                    $.ajax({
                        url: ajax_object.ajax_url,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'id' : id,
                            'points' : points,
                            'type' : type,
                            'action' : 'update_user_points'
                        },
                        success: function( data ) {
                            console.log( data.message );
                            $(".wait").fadeOut();
                            if(data.message=="success"){
                                $(".update-failed").fadeOut();
                                $(".wait").fadeOut(function(){
                                    alert("Successfully deducted points")
                                });
                                location.reload();
                            }else if(data.message=="failed"){
                                $(".update-failed").fadeOut();
                                $(".wait").fadeOut(function(){
                                    alert("Failed to deduct points")
                                });
                            }
                            else{
                                $(".wait").fadeOut(function(){
                                    alert("Failed to deduct points");
                                });
                            }
                        }
                    });

                }else{
                    $(".wait").hide();
                }    
            }
            return false;  //PREVENT FORM TO LOAD
        });
        

    // VIEW USER PRODUCTS
    $('.table tbody').on('click', '.view-user-products',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("viewUserProductsAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'html',
            data: {
                'id' : id,
                'action' : 'view_user_products'
            },
            success: function( data ) {
                // console.log( data.firstname );
                $(".wait").fadeOut();
                $(".user-products").html(data);
                $('#viewUserProductsModal').modal('show');
            }
        });
        return false;
    });


// ========================================================BRAND=================================
	// ADD NEW BRAND
	$(".add-brand").click( function() {
        var name = $('.add-brand-name').val();
		var category = $('.add-brand-category').val();
        if(!name || !category){
            $(".add-failed").fadeOut();
            $(".add-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            $(".add-failed").hide();
            // $(".add-load").fadeIn();
            $(".wait").fadeIn();
            // $(".add-brand").addClass("disabled");
            // $(".add-brand").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'name' : name,
                    'category' : category,
                    'action' : 'add_brand'
                },
                success: function( data ) {
                    console.log( data.message );
                    $(".wait").fadeOut();

                    // $(".add-load").hide();
                    // $(".add-brand").removeClass("disabled");
                    // $(".add-brand").prop('disabled', false);
                    if(data.message=="found"){
                        $(".add-failed").fadeOut();
                        $(".add-failed").fadeIn();
                        $(".error-message").text("Brand name already exists!")
                    }else if(data.message=="success"){
                        $(".add-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully added");
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to add");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
   	});
	
    // UPDATE BRAND
    $('.table tbody').on('click', '.update-brand',function(){
        $(".wait").fadeIn();           
        var id = $(this).attr("updateAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_brand'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('.update-brand-name').val(data.name);
                $('.update-brand-category').val(data.category);
                $('#updateBrandModal').modal('show');
            }
        });
        return false;
    });

    // SAVE BRAND
    $(".save-brand").click(function(){
        var id = $('.object-id').val();
        var name = $('.update-brand-name').val();
        var category = $('.update-brand-category').val();
        if(!name || !category){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            $(".update-failed").hide();
            // $(".update-load").fadeIn();
            $(".wait").fadeIn();
            // $(".save-brand").addClass("disabled");
            // $(".save-brand").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'name' : name,
                    'category' : category,
                    'action' : 'update_brand'
                },
                success: function( data ) {
                    console.log( data.message );
                    $(".wait").fadeOut();
                    // $(".update-load").hide();
                    // $(".save-brand").removeClass("disabled");
                    // $(".save-brand").prop('disabled', false);
                    if(data.message=="exist"){
                        $(".update-failed").fadeOut();
                        $(".update-failed").fadeIn();
                        $(".error-message").text("Brand name already exists!")
                    }else if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

	// DELETE BRAND RECORD
	$('.table tbody').on('click', '.delete-brand',function(){
		$(".wait").fadeIn();
		var id = $(this).attr("deleteAttr");
		if(confirm("Are you sure you want to delete this row?")){
			$.ajax({
	            url: ajax_object.ajax_url,
	            type: 'POST',
	            dataType: 'json',
	            data: {
	            	'id' : id,
	                'action' : 'delete_brand'
	            },
	            success: function( data ) {
	            	console.log( data.message );
                    $(".wait").fadeOut();
	            	if (data.message=='success') {
                        $(".wait").fadeOut(function(){
                            alert("Successfully deleted");
                        });
	            		location.reload();
	            	}else if(data.message=='found'){
                        $(".wait").fadeOut(function(){
                            alert("This record cannot be deleted.\nIt is associated with another record.");
                        });
                    }else{
	            		$(".wait").fadeOut(function(){
                            alert("Failed to delete");
                        });
	            	}
	            }
	        });
	        return false;
		}else{
            $(".wait").hide();
        }
		
	});

    // ============================================BRAND PRODUCT==============
    
     // VIEW BRAND PRODUCT
    $('.table tbody').on('click', '.view-brand-product',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("viewAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_brand_product'
            },
            success: function( data ) {
                // console.log( data.firstname );
                if (data.quantity==true) {
                     $('.display-quantity').text("quantity purchase");
                } else {
                     $('.display-quantity').text("single purchase");
                }

                $(".wait").fadeOut();
                $('.display-image').attr('src',data.image);
                $('.display-item').text(data.item);
                $('.display-description').text(data.description);
                $('.display-colors').text(data.colors);
                $('.display-brand').text(data.brand);
                $('.display-name').text(data.name);
                $('.display-category').text(data.category);
                $('.display-price').text(data.price);
                $('.display-points').text(data.points);
                $('#viewBrandProductModal').modal('show');
            }
        });
        return false;
    });

    // SELECT CATEGORY BASED ON SELECTED BRAND FOR ADDING
    $('.select-brand').change(function () {
        var id = $('.select-brand').val();
        $('.wait-category').fadeIn();
        $(".select-brand-category").hide();
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'html',
            data: {
                'id' : id,
                'action' : 'view_brand_category'
            },
            success: function( data ) {
                $('.wait-category').fadeOut();
                $(".select-brand-category").show();
                $(".select-brand-category").html(data);
            }
        });
    });

    // ADD NEW BRAND PRODUCT
    $(".add-brand-product").click( function() {

        var name = $('.add-brand-product-name').val();
        var code = $('.add-brand-product-code').val();
        var description = $('.add-brand-product-description').val();
        var colors = $('.add-brand-product-colors').val();
        var image = document.getElementById('add-brand-product-image').files[0];
        var price = $('.add-brand-product-price').val();
        var brand = $('.select-brand').val();// id of brand name 
        var category = $('.select-category').val(); 
        var points = $('.add-brand-product-points').val();   
        var quantity = $("input[name='quantity']:checked").val();
        
        var form_data = new FormData();
        form_data.append('name',name);
        form_data.append('code',code);
        form_data.append('description',description);
        form_data.append('colors',colors);
        form_data.append('price',price);
        form_data.append('brand',brand);
        form_data.append('category',category);
        form_data.append('points',points);
        form_data.append('quantity',quantity);
        form_data.append('image',image);
        form_data.append('action','add_brand_product');
        // var image = document.getElementById('file').files[0];

        // var category = $('.add-brand-category').val();
        if(!name || !code || !description || !price || brand=="select" || category=="select" || !points || !quantity || !image || !colors){
            $(".add-failed").fadeOut();
            $(".add-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            $(".add-failed").hide();
            $(".wait").fadeIn();
            // $(".add-brand-product").addClass("disabled");
            // $(".add-brand-product").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData:false, 
                data: form_data,
                success: function( data ) {
                    console.log( data.message );
                    // alert(data.message);
                    // $(".add-load").hide();
                    $(".wait").fadeOut();
                    // $(".add-brand-product").addClass("disabled");
                    // $(".add-brand-product").prop('disabled', false);
                    if(data.message=="not_image"){
                        alert("Upload image only")
                    }
                    else if(data.message=="found"){
                        $(".add-failed").fadeOut();
                        $(".add-failed").fadeIn();
                        $(".error-message").text("Brand product already exists!")
                    }else if(data.message=="success"){
                        $(".add-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully added");
                        });
                        // alert(data.file);
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to add");
                        });
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // UPDATE BRAND NAME AND BRAND PRODUCT
    $('.table tbody').on('click', '.update-brand-product',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("updateAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_brand_product'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('.update-brand-product-name').val(data.name);
                $('.update-brand-product-code').val(data.item);
                $('.update-brand-product-description').val(data.description);
                $('.update-brand-product-colors').val(data.colors);
                $('.show-category').val(data.category);
                $('.update-brand-product-price').val(data.price);
                $('.update-brand-product-points').val(data.points);

                if(data.quantity==true){
                    $('input:radio[name="update-quantity"][value="1"]').attr('checked', true);
                }else {
                    $('input:radio[name="update-quantity"][value="0"]').attr('checked', true);
                }

                $('#updateBrandProductModal').modal('show');
                $('.update-brand-name-and-category').hide();
                brandNameCategory();
            }
        });
        return false;
    });

    // UPDATE BRAND PRODUCT CATEGORY
    // $('.table tbody').on('click', '.update-brand-product',function(){
    function brandNameCategory(){
        var id = $('.object-id').val();
        var category = $('.show-category').val();
        $(".wait-category").fadeIn();
        $(".wait").fadeIn();
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'html',
            data: {
                'id' : id,
                'category' : category,
                'action' : 'view_brand_name_and_category'
            },
            success: function( data ) {
                $(".wait-category").fadeOut();
                $(".wait").fadeOut();
                $('.update-brand-name-and-category').show();
                $('.update-brand-name-and-category').html(data);
            }
        });
    }
    //     return false;
    // });

    // SELECT CATEGORY BASED ON SELECTED BRAND FOR UPDATING
    $('.update-brand-name-and-category').on('change', '.update-select-brand',function () {
        var id = $('.update-select-brand').val();
        var category = $('.show-category').val();
        $('.wait-category').fadeIn();
        $(".load-category").hide();
        $(".wait").fadeIn();
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'html',
            data: {
                'id' : id,
                'category' : category,
                'action' : 'view_brand_name_category'
            },
            success: function( data ) {
                $('.wait-category').fadeOut();
                $(".load-category").show();
                $(".wait").fadeOut();
                $(".load-category").html(data);
            }
        });
    });

    // SAVE BRAND PRODUCT
    $(".save-brand-product").click(function(){
        var id = $('.object-id').val();
        var name = $('.update-brand-product-name').val();
        var code = $('.update-brand-product-code').val();
        var description = $('.update-brand-product-description').val();
        var colors = $('.update-brand-product-colors').val();
        var price = $('.update-brand-product-price').val();
        var brand = $('.update-select-brand').val();
        var category = $('.update-select-category').val();
        var points = $('.update-brand-product-points').val();
        var quantity = $("input[name='update-quantity']:checked").val();
        var image = document.getElementById('update-brand-product-image').files[0];
        var data = $("#update-brand-product-image").val();
        
        var form_data = new FormData();
        form_data.append('id',id);
        form_data.append('name',name);
        form_data.append('code',code);
        form_data.append('description',description);
        form_data.append('colors',colors);
        form_data.append('price',price);
        form_data.append('brand',brand);
        form_data.append('category',category);
        form_data.append('points',points);
        form_data.append('quantity',quantity);
        form_data.append('image',image);
        form_data.append('data',data);
        form_data.append('action','update_brand_product');

        if(!name || !code || !description || !price || !points || !quantity || !colors ){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            $(".update-failed").hide();
            $(".wait").fadeIn();
            // $(".update-load").fadeIn();
            // $(".save-brand-product").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                contentType: false,
                cache: false, 
                processData:false,
                data: form_data,
                success: function( data ) {
                    console.log( data.message );
                    $(".wait").fadeOut();
                    // $(".update-load").hide();
                    // $(".save-brand-product").prop('disabled', false);
                    if (data.message=="not_image") {
                        alert("Upload image only");
                    }else if(data.message=="exist"){
                        $(".update-failed").fadeOut();
                        $(".update-failed").fadeIn();
                        $(".error-message").text("Store name already exists!")
                    }else if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // DELETE BRAND PRODUCT RECORD
    $('.table tbody').on('click', '.delete-brand-product',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("deleteAttr");
        if(confirm("Are you sure you want to delete this row?")){
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'action' : 'delete_brand_product'
                },
                success: function( data ) {
                    console.log( data.message );
                    if (data.message=='success') {
                        $(".wait").fadeOut(function(){
                            alert("Successfully deleted");
                        });
                        location.reload();
                    }else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to delete");
                        });
                    }
                }
            });
            return false;
        }else{
            $(".wait").hide();
        }
    });



    // ===============================================STORE======================
    // ADD NEW STORE

    $(".add-store").click(function(){
        var store = $(".add-store-name").val();

        if(!store){
            $(".add-failed").fadeOut();
            $(".add-failed").fadeIn();
            $(".error-message").text("Please fill out this field");
        }else{
            $(".alert-danger").hide();
            // $(".add-load").fadeIn();
            // $(".add-store").prop('disabled', true);
            $(".wait").fadeIn();
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'name' : store,
                    'action' : 'add_store'
                },
                success: function( data ) {
                    console.log( data.message );
                    // $(".add-load").hide();
                    // $(".add-store").prop('disabled', false);
                    $(".wait").fadeOut();
                    if(data.message=="found"){
                        $(".add-failed").fadeOut();
                        $(".add-failed").fadeIn();
                        $(".error-message").text("Store name already exist!")
                    }else if(data.message=="success"){
                        $(".add-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully added");
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to add");
                        });
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });
    // UPDATE BRAND
    $('.table tbody').on('click', '.update-store',function(){
        $(".wait").fadeIn();           
        var id = $(this).attr("updateAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_store'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('.update-store-name').val(data.name);
                $('#updateStoreModal').modal('show');
            }
        });
        return false;
    });
    // SAVE STORE
    $(".save-store").click(function(){
        var id = $('.object-id').val();
        var brand = $('.update-store-name').val();
        // var category = $('.add-brand-category').val();
        if(!brand){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            $(".update-failed").hide();
            // $(".update-load").fadeIn();
            // $(".save-store").prop('disabled', true);
            $(".wait").fadeIn();
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'name' : brand,
                    'action' : 'update_store'
                },
                success: function( data ) {
                    console.log( data.message );
                    // $(".update-load").hide();
                    // $(".save-store").prop('disabled', false);
                    $(".wait").fadeOut();
                    if(data.message=="exist"){
                        $(".update-failed").fadeOut();
                        $(".update-failed").fadeIn();
                        $(".error-message").text("Store name already exists!")
                    }else if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

	// DELETE STORE
    $('.table tbody').on('click', '.delete-store',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("deleteAttr");
        if(confirm("Are you sure you want to delete this row?")){
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'action' : 'delete_store'
                },
                success: function( data ) {
                    console.log( data.message );
                    if (data.message=='success') {
                        $(".wait").fadeOut(function(){
                            alert("Successfully deleted");
                        });
                        location.reload();
                    }else if(data.message=='found'){
                        $(".wait").fadeOut(function(){
                            alert("This record cannot be deleted.\nIt is associated with another record.");
                        });
                    }else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to delete");
                        });
                    }
                }
            });
            return false;
        }else{
            $(".wait").hide();
        }
        
    });

    // =====================================STORE BRANCH==================
    // ADD NEW STORE BRANCH

    $(".add-store-branch").click(function(){
        var id =$(".select-store").val();
        var name = $(".add-store-branch-name").val();
        // alert(id+" "+store);

        if(!name){
            $(".add-failed").fadeOut();
            $(".add-failed").fadeIn();
            $(".error-message").text("Please fill out this field");
        }else{
            $(".alert-danger").hide();
            // $(".add-load").fadeIn();
            // $(".add-store-branch").prop('disabled', true);
            $(".wait").fadeIn();
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'name' : name,
                    'action' : 'add_store_branch'
                },
                success: function( data ) {
                    console.log( data.message );
                    // $(".add-load").hide();
                    // $(".add-store-branch").prop('disabled', false);
                    $(".wait").fadeOut();
                    if(data.message=="found"){
                        $(".add-failed").fadeOut();
                        $(".add-failed").fadeIn();
                        $(".error-message").text("Store branch name already exists!")
                    }else if(data.message=="success"){
                        $(".add-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully added");
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to add");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // UPDATE STORE BRANCH
    $('.table tbody').on('click', '.update-store-branch',function(){
        $(".wait").fadeIn();           
        var id = $(this).attr("updateAttr");
        
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_store_branch'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('.update-store-branch-name').val(data.name);
                $('#updateStoreBranchModal').modal('show');
                $('.update-store-name').hide();
                storeName();
            }
        });
        return false;
    });

    function storeName(){
        var id = $('.object-id').val();
        $(".wait-category").fadeIn();
        $(".wait").fadeIn();
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'html',
            data: {
                'id' : id,
                'action' : 'view_store_name'
            },
            success: function( data ) {
                $(".wait-category").fadeOut();
                $(".wait").fadeOut();
                $('.update-store-name').show();
                $('.update-store-name').html(data);
            }
        });
    }

    // SAVE STORE
    $(".save-store-branch").click(function(){
        var id = $('.object-id').val();
        var name = $('.update-store-branch-name').val();
        var store = $('.update-select-store').val();

        if(!name){
            // alert(store);
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            $(".update-failed").hide();
            // $(".update-load").fadeIn();
            // $(".save-store-branch").prop('disabled', true);
            $(".wait").fadeIn();
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'name' : name,
                    'store' : store,
                    'action' : 'update_store_branch'
                },
                success: function( data ) {
                    console.log( data.message );
                    // $(".update-load").hide();
                    // $(".save-store-branch").prop('disabled', false);
                    $(".wait").fadeOut();
                    if(data.message=="exist"){
                        $(".update-failed").fadeOut();
                        $(".update-failed").fadeIn();
                        $(".error-message").text("Name already exists!")
                    }else if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // DELETE STORE BRANCH
    $('.table tbody').on('click', '.delete-store-branch',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("deleteAttr");
        if(confirm("Are you sure you want to delete this row?")){
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'action' : 'delete_store_branch'
                },
                success: function( data ) {
                    console.log( data.message );
                    if (data.message=='success') {
                        $(".wait").fadeOut(function(){
                            alert("Successfully deleted");
                        });
                        location.reload();
                    }else{
                        $(".wait").fadeIn(function(){
                            alert("Failed to delete");
                        });
                    }
                }
            });
            return false;
        }else{
            $(".wait").hide();
        }
    });

    // ==================================PRODUCT==========================
    // VIEW PRODUCT
    $('.table tbody').on('click', '.view-product',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("viewAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_product'
            },
            success: function( data ) {
                // console.log( data.firstname );
                if (data.quantity==true) {
                     $('.display-quantity').text("quantity purchase");
                } else {
                     $('.display-quantity').text("single purchase");
                }

                $(".wait").fadeOut();
                $('.display-image').attr('src',data.image);
                $('.display-user').text(data.userFname+" "+data.userLname);
                $('.display-store').text(data.store);
                $('.display-branch').text(data.branch);
                $('.display-brand').text(data.brand);
                $('.display-model').text(data.model);
                $('.display-transaction').text(data.transaction);
                $('.display-purchase').text(data.date);
                $('.display-category').text(data.category);
                $('.display-serial').text(data.serial);
                $('.display-quantity').text(data.quantity);
                $('.display-status').text(data.status);
                $('#viewProductModal').modal('show');
            }
        });
        return false;
    });


    // ==============================ARTICLE=================

    // ADD NEW ARTICLE
    $(".add-article").click(function(){
        $("#articleContent-html").trigger("click");
        var title = $(".add-article-title").val();
        var image = document.getElementById('add_article_image').files[0];
        // var content = $(".add-article-content").val();
        var content = $('#articleContent').val();
        var file = $('#add_article_image').get(0).files.length;

        var form_data = new FormData();
        form_data.append('title',title);
        form_data.append('image',image);
        form_data.append('content',content);
        form_data.append('action','add_article');
         
        if(!title || !content || file==0){
            $(".add-failed").fadeOut();
            $(".add-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            $(".alert-danger").hide();
            // $(".add-load").fadeIn();
            // $(".add-article").prop('disabled', true);
            $(".wait").fadeIn();
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                contentType: false,
                cache: false,
                processData:false,
                data: form_data,
                success: function( data ) {
                    console.log( data.message );
                    // $(".add-load").hide();
                    // $(".add-article").prop('disabled', false);
                    $(".wait").fadeIn();
                    if(data.message=="not_image"){
                        $(".wait").fadeOut(function(){
                            alert("Upload image only");
                        });
                    }else if(data.message=="found"){
                        $(".wait").fadeOut();
                        $(".add-failed").fadeOut();
                        $(".add-failed").fadeIn();
                        $(".error-message").text("Article name already exist!")
                    }else if(data.message=="success"){
                        $(".add-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully added");
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to add");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // VIEW ARTICLE
    $('.table tbody').on('click', '.view-article',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("viewAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_article'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.display-title').text(data.title);
                $('.display-image').attr('src',data.image);
                $('.display-date').text(data.date);
                $('.display-content').html(data.content);
                $('.display-views').text(data.views);
                $('#viewArticleModal').modal('show');
            }
        });
        return false;
    });

    // UPDATE ARTICLE
    $('.table tbody').on('click', '.update-article',function(){
        $(".wait").fadeIn();           
        var id = $(this).attr("updateAttr");
        
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_article'
            },
            success: function( data ) {
                $('#updateArticleContent-html').trigger('click');
                // $('#updateArticleContent-tmce').trigger('click');
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('.update-article-title').val(data.title);
                $('.update-article-date').val(data.date2);
                $('#updateArticleContent').val(data.content);
                // alert(data.content);
                $('#updateArticleModal').modal('show');
            }
        });
        return false;
    });

     // SAVE ARTICLE
    $(".save-article").click(function(){
        $('#updateArticleContent-html').trigger('click');
        var id = $('.object-id').val();
        var title = $('.update-article-title').val();
        var date = $('.update-article-date').val();
        var content = $('#updateArticleContent').val();
        var data = $('#update_article_image').val();
        var image = document.getElementById('update_article_image').files[0];

        var form_data = new FormData();
        form_data.append('id',id);
        form_data.append('title',title);
        form_data.append('date',date);
        form_data.append('content',content);
        form_data.append('image',image);
        form_data.append('data',data);
        form_data.append('action','update_article');


        if(!title || !date || !content){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            $(".update-failed").hide();
            $(".wait").fadeIn();
            // $(".update-load").fadeIn();
            // $(".save-article").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                contentType: false,
                cache: false,
                processData:false,
                data: form_data,
                success: function( data ) {
                    console.log( data.message );
                    $(".wait").fadeOut();
                    // $(".update-load").hide();
                    // $(".save-article").prop('disabled', false);
                    if(data.message=="exist"){
                        $(".update-failed").fadeOut();
                        $(".update-failed").fadeIn();
                        // $(".wait").fadeOut();
                        $(".error-message").text("Title already exists!")
                    }else if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // DELETE ARTICLE
    $('.table tbody').on('click', '.delete-article',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("deleteAttr");
        if(confirm("Are you sure you want to delete this row?")){
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'action' : 'delete_article'
                },
                success: function( data ) {
                    console.log( data.message );
                    if (data.message=='success') {
                        $(".wait").fadeOut(function(){
                            alert("Successfully deleted");
                        });
                        location.reload();
                    }else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to delete");
                        });
                    }
                }
            });
            return false;
        }else{
            $(".wait").hide();
        }
    });

// =====================================SERVICE CENTER==================
    // ADD NEW SERVICE CENTER

    $(".add-service-center").click(function(){
        var name = $(".add-sc-name").val();
        var wdf = $(".add-sc-wdf").val();
        var wdt = $(".add-sc-wdt").val();
        var whf = $(".add-sc-whf").val();
        var wht = $(".add-sc-wht").val();
        var address = $(".add-sc-address").val();
        var number = $(".add-sc-number").val();
        var email = $(".add-sc-email").val();
        var latitude = $(".add-sc-latitude").val();
        var longitude = $(".add-sc-longitude").val();
        // alert(id+" "+store);

        if(!name || !wdf || !wdt || !whf || !wht || !address || !number || !email || !latitude || !longitude){
            $(".add-failed").fadeOut();
            $(".add-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else if(!email.match(/^([\w-\.]+@([\w-]+\.)+[\w-]{3,4})?$/)){
            $(".add-failed").fadeOut();
            $(".add-failed").fadeIn();
            $(".error-message").text("Enter a valid email address");
        }else{
            $(".alert-danger").hide();
            // $(".add-load").fadeIn();
            // $(".add-service-center").prop('disabled', true);
            $(".wait").fadeIn();
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'name' : name,
                    'wdf' : wdf,
                    'wdt' : wdt,
                    'whf' : whf,
                    'wht' : wht,
                    'address' : address,
                    'number' : number,
                    'email' : email,
                    'latitude' : latitude,
                    'longitude' : longitude,
                    'action' : 'add_service_center'
                },
                success: function( data ) {
                    console.log( data.message );
                    // $(".add-load").hide();
                    // $(".add-service-center").prop('disabled', false);
                    $(".wait").fadeOut();
                    if(data.message=="found"){
                        $(".add-failed").fadeOut();
                        $(".add-failed").fadeIn();
                        $(".error-message").text("Name already exist!")
                    }else if(data.message=="success"){
                        $(".add-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully added");
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to add");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // VIEW SERVICE CENTER
    $('.table tbody').on('click', '.view-service-center',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("viewAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_service_center'
            },
            success: function( data ) {
                // console.log( data.firstname );
                $(".wait").fadeOut();
                $('.display-name').text(data.name);
                $('.display-address').text(data.address);
                $('.display-number').text(data.number);
                $('.display-email').text(data.email);
                $('.display-latitude').val(data.latitude);
                $('.display-longitude').val(data.longitude);
                $('.display-wd').text(data.wdf+" - "+data.wdt);
                $('.display-wh').text(data.whf+" - "+data.wht);
                $('#viewServiceCenterModal').modal('show');
                // LOCATION PICKER FOR VIEWING
                viewMap(data.latitude,data.longitude);

            }
        });
        return false;
    });

    function viewMap(latitude,longitude){
        
        $('.display-map').locationpicker({
            location: {
                latitude: latitude,
                longitude: longitude
            },
            radius: 0,
            zoom: 15,
            inputBinding: {
                latitudeInput: $('.display-latitude'),
                longitudeInput: $('.display-longitude')
                // radiusInput: $('#us2-radius'),
                //locationNameInput: $('.display-map-address')
            },
            enableAutocomplete: true,  
            // markerDraggable: false
        });
        $('#viewServiceCenterModal').on('shown.bs.modal', function () {
            $('.display-map').locationpicker('autosize');
        });
    }

    // UPDATE SERVICE CENTER
    $('.table tbody').on('click', '.update-service-center',function(){
        $(".wait").fadeIn();           
        var id = $(this).attr("updateAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_service_center'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('.update-sc-name').val(data.name);
                $('.update-sc-address').val(data.address);
                $('.update-sc-number').val(data.number);
                $('.update-sc-email').val(data.email);
                $('.update-sc-latitude').val(data.latitude);
                $('.update-sc-longitude').val(data.longitude);
                $('.update-sc-wdf').append('<option value="'+data.wdf+'" selected>'+data.wdf+'</option>');
                $('.update-sc-wdt').append('<option value="'+data.wdt+'" selected>'+data.wdt+'</option>');
                $('.update-sc-whf').val(data.whf);
                $('.update-sc-wht').val(data.wht);
                $('#updateServiceCenterModal').modal('show');

                $('#update-map').locationpicker({
                    location: {
                        latitude: data.latitude,
                        longitude: data.longitude
                    },
                    radius: 0,
                    zoom: 15,
                    inputBinding: {
                        latitudeInput: $('#update-map-latitude'),
                        longitudeInput: $('#update-map-longitude'),
                        // radiusInput: $('#us2-radius'),
                        locationNameInput: $('#update-map-address')
                    },
                    enableAutocomplete: true, 
                    });
                    $('#updateServiceCenterModal').on('shown.bs.modal', function () {
                        $('#update-map').locationpicker('autosize');
                    });
            }
        });
        return false;
    });

    

    // SAVE SERVICE CENTER
    $(".save-service-center").click(function(){
        var id = $('.object-id').val();
        var name = $('.update-sc-name').val();
        var address = $('.update-sc-address').val();
        var number = $('.update-sc-number').val();
        var email = $('.update-sc-email').val();
        var latitude = $('.update-sc-latitude').val();
        var longitude = $('.update-sc-longitude').val();
        var wdf = $('.update-sc-wdf').val();
        var wdt = $('.update-sc-wdt').val();
        var whf = $('.update-sc-whf').val();
        var wht = $('.update-sc-wht').val();
        // var category = $('.add-brand-category').val();
        if(!name || !wdf || !wdt || !whf || !wht || !address || !number || !email || !latitude || !longitude){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            $(".update-failed").hide();
            // $(".update-load").fadeIn();
            // $(".save-service-center").prop('disabled', true);
            $(".wait").fadeIn();
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'name' : name,
                    'wdf' : wdf,
                    'wdt' : wdt,
                    'whf' : whf,
                    'wht' : wht,
                    'address' : address,
                    'number' : number,
                    'email' : email,
                    'latitude' : latitude,
                    'longitude' : longitude,
                    'action' : 'update_service_center'
                },
                success: function( data ) {
                    console.log( data.message );
                    // $(".update-load").hide();
                    // $(".save-service-center").prop('disabled', false);
                    $(".wait").fadeOut();
                    if(data.message=="exist"){
                        $(".update-failed").fadeOut();
                        $(".update-failed").fadeIn();
                        $(".error-message").text("Name already exists!")
                    }else if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // DELETE SERVICE CENTER
    $('.table tbody').on('click', '.delete-service-center',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("deleteAttr");
        if(confirm("Are you sure you want to delete this row?")){
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'action' : 'delete_service_center'
                },
                success: function( data ) {
                    console.log( data.message );
                    if (data.message=='success') {
                        $(".wait").fadeOut(function(){
                            alert("Successfully deleted");
                        });
                        location.reload();
                    }else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to delete");
                        });
                    }
                }
            });
            return false;
        }else{
            $(".wait").hide();
        }
    });

// ============SERVICE CENTER APPOINTMENT===============
// UPDATE SERVICE CENTER APPONTMENT
    $('.table tbody').on('click', '.update-appointment',function(){          
        var id = $(this).attr("updateAttr");
        var status = $(this).attr("statusAttr");
        if(status=="accepted"){
            if(confirm("Do you want to accept this appointment?")){
                $('.appointment-object-id').val(id);           
                $('#addScoreModal').modal('show');
                        
            }else{
                $('.wait').hide();
            }
        // if selected is declined
        }else if(status=="declined"){
           if(confirm("Do you want to decline this appointment?")){
                $('.wait').fadeIn();
                var points = "";
                $.ajax({
                    url: ajax_object.ajax_url,
                    type: 'POST',   
                    dataType: 'json',
                    data: {
                        'id' : id,
                        'points' : points,
                        'status' : status,
                        'action' : 'update_service_center_appointment'
                    },
                    success: function( data ) {

                        if(data.message=='success'){
                            $(".wait").fadeOut(function(){
                                alert("Successfully updated");
                            });
                            location.reload();
                        }else if(data.message=='failed'){
                            $(".wait").fadeOut(function(){
                                alert("Failed to update");
                            });
                        }else{
                            $(".wait").fadeOut(function(){
                                alert("Failed to update");
                            });
                        }
                        // $('.object-id').val(id);
                        // $('.update-sca-status').val(data.status);
                        // $('.update-sca-points').val(data.points);
                        // $('#updateSCAModal').modal('show');
                    }
                });
                return false;
                        
            }else{
                $('.wait').hide();
            }
        }else{
            // 
        }
        
        
    });

    // SAVE POINTS UPON ACCEPT
    $('.save-points-accept').click(function(){
        var id = $('.appointment-object-id').val();
        var points = $(".add-points-accept").val();
        var status = "accepted";
            if(!points){
                $(".update-failed").fadeOut();
                $(".update-failed").fadeIn();
                $(".error-message").text("Points must not be empty");
            }else if(points<0){
                $(".update-failed").fadeOut();
                $(".update-failed").fadeIn();
                $(".error-message").text("Points must not contain negative");
            }else{
                $(".wait").fadeIn();
                $.ajax({
                    url: ajax_object.ajax_url,
                    type: 'POST',   
                    dataType: 'json',
                    data: {
                        'id' : id,
                        'points' : points,
                        'status' : status,
                        'action' : 'update_service_center_appointment'
                    },
                    success: function( data ) {

                        if(data.message=='success'){
                            $(".wait").fadeOut(function(){
                                alert("Successfully updated");
                            });
                            location.reload();
                        }else if(data.message=='failed'){
                            $(".wait").fadeOut(function(){
                                 alert("Failed to update");
                            });
                        }else{
                            $(".wait").fadeOut(function(){
                                alert("Failed to update");
                            });
                        }
                   
                    }
                });
            }
        
            return false; 

    });

    // VIEW SERVICE CENTER APPOINTMENT
    $('.table tbody').on('click', '.view-appointment',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("viewAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_service_center_appointment'
            },
            success: function( data ) {
                // console.log( data.firstname );
                $(".wait").fadeOut();
                $('.display-name').text(data.name);
                $('.display-user').text(data.user);
                $('.display-email').text(data.email);
                $('.display-number').text(data.number);
                $('.display-status').text(data.status);
                $('.display-points').text(data.points);
                $('.display-problem').text(data.problem);
                $('#viewServiceCenterAppointmentModal').modal('show');
            }
        });
        return false;
    });

     // VIEW APPOINTMENT PRODUCT
    $('.table tbody').on('click', '.view-product-appointment',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("viewProductAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'html',
            data: {
                'id' : id,
                'action' : 'view_product_appointment'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $(".product-appointment").html(data);
                $('#viewProductAppointmentModal').modal('show');
            }
        });
        return false;
    });

    // SAVE SERVICE CENTER APPOINTMENT
    $(".save-sca").click(function(){
        var id = $('.object-id').val();
        var status = $('.update-sca-status').val();
        var points = $('.update-sca-points').val();
        if(!status || !points){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            $(".update-failed").hide();
            $(".wait").fadeIn();
            // $(".update-load").fadeIn();
            // $(".save-sca").addClass("disabled");
            // $(".save-sca").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'status' : status,
                    'points' : points,
                    'action' : 'update_service_center_appointment'
                },
                success: function( data ) {
                    console.log( data.message );
                    // $(".update-load").hide();
                    // $(".save-sca").removeClass("disabled");
                    // $(".save-sca").prop('disabled', false);
                    $(".wait").fadeOut();
                    if(data.message=="exist"){
                        $(".update-failed").fadeOut();
                        $(".update-failed").fadeIn();
                        $(".error-message").text("Brand name already exists!")
                    }else if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").hide(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").hide(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // DELETE SERVICE CENTER APPOINTMENT
    $('.table tbody').on('click', '.delete-appointment',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("deleteAttr");
        if(confirm("Are you sure you want to delete this row?")){
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'action' : 'delete_service_center_appointment'
                },
                success: function( data ) {
                    console.log( data.message );
                    if (data.message=='success') {
                        $(".wait").fadeOut(function(){
                            alert("Successfully deleted");
                        });
                        location.reload();
                    }else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to delete");
                        });

                    }
                }
            });
            return false;
        }else{
            $(".wait").hide();
        }
    });

// ============REDEMPTION===============
// UPDATE REDEMPTION
    $('.table tbody').on('click', '.update-redemption',function(){
        $(".wait").fadeIn();           
        var id = $(this).attr("updateAttr");
        var type = $(this).attr("typeAttr");
        var status = $(this).attr("redemptionStatusAttr");
        // alert(type);
        if(confirm("Do you want to change this status to "+status+"?")){
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'type' : type,
                    'status' : status,
                    'action' : 'update_redemption'
                },
                success: function( data ) {
                    console.log( data.message );
                    $(".wait").fadeOut();
                    if(data.message=="success"){
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated");
                        });
                        location.reload();
                    }
                    else if(data.message=="failed"){
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                    }
                }
            });
            return false;
        }else{
            $(".wait").hide();
        }    
        
    });

    // SAVE REDEMPTION
    $(".save-redemption").click(function(){
        var id = $('.object-id').val();
        var type = $('.redemption-type').val();
        var status = $('.update-redemption-status').val();
        var points = $('.update-redemption-points').val();
        if(!status || !points){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            $(".update-failed").hide();
            $(".update-load").fadeIn();
            $(".save-redemption").addClass("disabled");
            $(".save-redemption").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'type' : type,
                    'status' : status,
                    'points' : points,
                    'action' : 'update_redemption'
                },
                success: function( data ) {
                    console.log( data.message );
                    // alert(data.message);
                    $(".update-load").hide();
                    $(".save-redemption").removeClass("disabled");
                    $(".save-redemption").prop('disabled', false);
                    if(data.message=="failed"){
                        alert("Successfully updated");
                    }else if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".update-load").hide(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".update-load").hide(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // DELETE REDEMPTION
    $('.table tbody').on('click', '.delete-redemption',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("deleteAttr");
        if(confirm("Are you sure you want to delete this row?")){
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'action' : 'delete_redemption'
                },
                success: function( data ) {
                    console.log( data.message );
                    if (data.message=='success') {
                        $(".wait").fadeOut(function(){
                            alert("Successfully deleted");
                        });
                        location.reload();
                    }else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to delete");
                        });
                    }
                }
            });
            return false;
        }else{
            $(".wait").hide();
        }
    });

// =================REWARD PRODUCT============

//SET INPUTS FOR PRODUCT AND VOUCHER
    
    $('.reward-product-type').change(function () {
        var selected = $('.reward-product-type').val();
        if (selected=="product"){
           $('.reward-type').html("<label>Brand:</label><input type='text' class='form-control add-reward-product-type'>");
        }else if (selected=="voucher"){
           $('.reward-type').html("<label>Voucher:</label><input type='number' class='form-control add-reward-product-type'>");
        }else{
           $('.reward-type').html("");
        }
    });

// AVAILABLE
    $('.add-reward-product-available').val("0");
    $('.available-label').text("Unavailable");
    $(".add-reward-product-available").click(function(){
        if ($('.add-reward-product-available').is(":checked")) {  
            $('.add-reward-product-available').val("1");
            $('.available-label').text("Available");
        } else {
            $('.add-reward-product-available').val("0");
            $('.available-label').text("Unavailable");
        }
    });
    
 // ADD NEW REWARD PRODUCT
    $(".add-reward-product").click(function(){
        var name = $(".add-reward-product-name").val();
        var points = $(".add-reward-product-points").val();
        var type = $(".reward-product-type").val();
        var description = $(".add-reward-product-description").val();
        var reward = $(".add-reward-product-type").val();
        var available = $('.add-reward-product-available').val();
        var image = document.getElementById('add_brand_product_image').files[0];

        var form_data = new FormData();
        form_data.append('name',name);
        form_data.append('points',points);
        form_data.append('type',type);
        form_data.append('description',description);
        form_data.append('reward',reward);
        form_data.append('available',available);
        form_data.append('image',image);
        form_data.append('action','add_reward_product');

        if(!name || !points || type=="select" || !description || !reward){
            $(".add-failed").fadeOut();
            $(".add-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            // alert(type +reward+available);
            $(".alert-danger").hide();
            // $(".add-load").fadeIn();
            // $(".add-reward-product").prop('disabled', true);
            $(".wait").fadeIn();
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                contentType: false,
                cache: false,
                processData:false,
                data: form_data,
                success: function( data ) {
                    console.log( data.message );
                    // $(".add-load").hide();
                    // $(".add-reward-product").prop('disabled', false);
                    $(".wait").fadeOut();
                    if(data.message=="not_image"){
                        $(".wait").fadeOut(function(){
                            alert("Upload image only");
                        });
                    }else if(data.message=="found"){
                        $(".add-failed").fadeOut();
                        $(".add-failed").fadeIn();
                        $(".error-message").text("Name already exist!")
                    }else if(data.message=="success"){
                        $(".add-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully added");
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to add");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // VIEW REWARD PRODUCT
    $('.table tbody').on('click', '.view-reward-product',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("viewAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_reward_product'
            },
            success: function( data ) {
                // console.log( data.firstname );
                $(".wait").fadeOut();
                $('.display-image').attr('src',data.image);
                $('.display-name').text(data.name);
                $('.display-description').text(data.description);
                $('.display-type').text(data.type);
                if(data.type=="product"){
                    $('.display-reward-type').html("<span class='display-info'>Brand:</span> <b>"+data.brand+"</b>");
                }else{
                    $('.display-reward-type').html("<span class='display-info'>Voucher amount:</span> <b>"+data.voucher+"</b>");
                }
                $('.display-points').text(data.points);
                if(data.available==true){
                    $('.display-available').text("Available");
                }else{
                    $('.display-available').text("Unavailable");
                }
                
                $('#viewRewardProductModal').modal('show');
            }
        });
        return false;
    });

    // UPDATE REWARD PRODUCT
    $('.table tbody').on('click', '.update-reward-product',function(){
        $(".wait").fadeIn();           
        var id = $(this).attr("updateAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_reward_product'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('.update-reward-product-name').val(data.name);
                $('.update-reward-product-description').val(data.description);
                if(data.type=="product"){
                    $('.update-reward-product-type').val('product');
                    $('.show-reward-type').html("<label>Brand:</label><input type='text' value='"+data.brand+"' class='form-control update-reward-type'>");
                }else{
                    $('.update-reward-product-type').val('voucher');
                    $('.show-reward-type').html("<label>Voucher:</label><input type='number' value='"+data.voucher+"' class='form-control update-reward-type'>");
                }
                $('.update-reward-product-points').val(data.points);
                if(data.available==true){
                    $('.update-reward-product-available').attr('checked', true);
                    $('.update-available-label').text('Available');
                }else{
                    $('.update-reward-product-available').attr('checked', false);
                    $('.update-available-label').text('Unavailable');
                }
                $('#updateRewardProductModal').modal('show');
            }
        });
        return false;
    });

    //SET INPUTS FOR PRODUCT AND VOUCHER
    $('.update-reward-product-type').change(function () {
        var selected = $('.update-reward-product-type').val();
        if (selected=="product"){
           $('.show-reward-type').html("<label>Brand:</label><input type='text' class='form-control update-reward-type'>");
        }else if (selected=="voucher"){
           $('.show-reward-type').html("<label>Voucher:</label><input type='number' class='form-control update-reward-type'>");
        }else{
           $('.show-reward-type').html("");
        }
    });

    // AVAILABLE
    $('.update-reward-product-available').val("0");
    $('.available-label').text("Unavailable");
    $(".update-reward-product-available").click(function(){
        if ($('.update-reward-product-available').is(":checked")) {  
            $('.update-reward-product-available').val("1");
            $('.update-available-label').text("Available");
        } else {
            $('.update-reward-product-available').val("0");
            $('.update-available-label').text("Unavailable");
        }
    });

    // SAVE REWARD PRODUCT
    $(".save-reward-product").click(function(){
        var id = $('.object-id').val();
        var name = $(".update-reward-product-name").val();
        var points = $(".update-reward-product-points").val();
        var type = $(".update-reward-product-type").val();
        var description = $(".update-reward-product-description").val();
        var reward = $(".update-reward-type").val();
        var available = $('.update-reward-product-available').val();
        var image = document.getElementById('update_reward_product_image').files[0];
        var data = $('#update_reward_product_image').val();
        
        var form_data = new FormData();
        form_data.append('id',id);        
        form_data.append('name',name);        
        form_data.append('points',points);        
        form_data.append('type',type);        
        form_data.append('description',description);        
        form_data.append('reward',reward);        
        form_data.append('available',available);        
        form_data.append('image',image);        
        form_data.append('data',data);        
        form_data.append('action','update_reward_product');        

        if(!name || !points || !description || !reward){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            $(".update-failed").hide();
            $(".wait").fadeIn();
            // $(".update-load").fadeIn();
            // $(".save-reward-product").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,
                data: form_data,
                success: function( data ) {
                    console.log( data.message );
                    $(".wait").fadeOut();
                    // $(".update-load").hide();
                    // $(".save-reward-product").prop('disabled', false);
                    if(data.message=="not_image"){
                        $(".wait").fadeOut(function(){
                            alert("Upload image only");
                        });
                    }else if(data.message=="exist"){
                        $(".update-failed").fadeOut();
                        $(".update-failed").fadeIn();
                        $(".error-message").text("Reward product name already exist!")
                    }else if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated");
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // DELETE REWARD PRODUCT
    $('.table tbody').on('click', '.delete-reward-product',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("deleteAttr");
        if(confirm("Are you sure you want to delete this row?")){
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'action' : 'delete_reward_product'
                },
                success: function( data ) {
                    console.log( data.message );
                    if (data.message=='success') {
                        $(".wait").fadeOut(function(){
                            alert("Successfully deleted");
                        });
                        location.reload();
                    }else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to delete");
                        });

                    }
                }
            });
            return false;
        }else{
            $(".wait").hide();
        }
    });


    // =============================SURVEY===================
    
    // ACTIVE
    // $('.add-survey-active').val("0");
    // $('.add-survey-label').text("No");
    // $(".add-survey-active").click(function(){
    //     if ($('.add-survey-active').is(":checked")) {  
    //         $('.add-survey-active').val("1");
    //         $('.add-survey-label').text("Yes");
    //     } else {
    //         $('.add-survey-active').val("0");
    //         $('.add-survey-label').text("No");
    //     }
    // });

    // ADD SURVEY CHOICES
    var count = 1;
    $('.add-survey-choices').click(function(){
        if(count <= 4){
            count++;
            $('.append-question-choices').append("<tr id='row"+count+"'><td width='5'>"+count+".&nbsp;</td><td><input type='text' name='add-survey-label[]' class='form-control add-survey-label' placeholder='label'></td><td><input type='text' name='add-survey-value[]' class='form-control add-survey-value' placeholder='value'></td></tr>");
        }
        return false;
    });
    // REMOVE CHOICES
    $('.remove-survey-choices').click(function(){
        // var remove_btn = $(this).attr("id");
        if(count!=1){
            $('#row'+count).remove();
            count--;
        }
       
        return false;
    });

    // ACTIVE UPDATE
    $('.update-survey-active').val("0");
    $('.update-survey-label').text("No");
    $(".update-survey-active").click(function(){
        if ($('.update-survey-active').is(":checked")) {  
            $('.update-survey-active').val("1");
            $('.update-survey-label').text("Active");
        } else {
            $('.update-survey-active').val("0");
            $('.update-survey-label').text("Inactive");
        }
    });

    // ADD NEW SURVEY
    $(".add-survey").click(function(){
        var title = $(".add-survey-title").val();
        var points = $(".add-survey-points").val();
        // alert(label);
        if(!title || !points){
            $(".add-failed").fadeOut();
            $(".add-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            // alert(type +reward+available);
            $(".alert-danger").hide();
            $(".wait").fadeIn();
            // $(".add-load").fadeIn();
            // $(".add-survey").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'title' : title,
                    'points' : points,
                    // 'active' : active,
                    'action' : 'add_survey'
                },
                success: function( data ) {
                    console.log( data.message );
                    $(".wait").fadeOut();
                    // $(".add-load").hide();
                    // $(".add-survey").prop('disabled', false);
                    if(data.message=="found"){
                        $(".add-failed").fadeOut();
                        $(".add-failed").fadeIn();
                        $(".error-message").text("Survey title already exist!")
                    }else if(data.message=="success"){
                        $(".add-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully added");
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to add");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });//ADD SURVEY 

    // ADD NEW SURVEY QUESTION
    $(".add-question").click(function(){
        var surveyId = $(".survey-id").val();
        var question = $('.add-survey-question').val();
        var label = $('input[name="add-survey-label[]"]').map(function(){return $(this).val();}).get();
        var value = $('input[name="add-survey-value[]"]').map(function(){return $(this).val();}).get();

        // check if label is empty
        var checkLabel = false;
        $(".add-survey-label").each(function() {
            var lb = $(this).val();
            if (!lb) {
                checkLabel = true;
            }
        });

        // check if value is empty
        var checkValue = false;
        $(".add-survey-value").each(function() {
            var vl = $(this).val();
            if (!vl) {
                checkValue = true;
            }
        });

        if (!question || checkLabel || checkValue) {
            $(".add-failed").fadeOut();
            $(".add-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            // alert(type +reward+available);
            $(".alert-danger").hide();
            $('.wait').fadeIn();
            // $(".add-load").fadeIn();
            // $(".add-survey").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'surveyId' : surveyId,
                    'question' : question,
                    'label' : label,
                    'value' : value,
                    // 'active' : active,
                    'action' : 'add_survey_question'
                },
                success: function( data ) {
                    console.log( data.message );
                    $('.wait').fadeOut();
                    // $(".add-load").hide();
                    // $(".add-survey").prop('disabled', false);
                    if(data.message=="exist"){
                        $(".add-failed").fadeOut();
                        $(".add-failed").fadeIn();
                        $(".error-message").text("Question already exist!")
                    }else if(data.message=="success"){
                        $(".add-failed").fadeOut();
                        $('.wait').fadeOut(function(){
                            alert("Successfully added");
                        });
                        surveyQuestionsTable(surveyId);
                        $('#addQuestionModal').modal('hide');
                        // location.reload();
                        // clear label fields
                        $(".add-survey-label").each(function() {
                            $(this).val('');
                        });
                        // clear value fields
                        $(".add-survey-value").each(function() {
                            $(this).val('');
                        });
                        // clear question field
                        $('.add-survey-question').val('');

                    }
                    else{
                        $(".wait").hide("slow",function(){
                            alert("Failed to add");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });// ADD SURVEY QUESTION MODAL

    function surveyQuestionsTable(id){
        $(".wait").fadeIn();
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'html',
            data: {
                'id' : id,
                'action' : 'view_survey_question'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.table-responsive').hide();
                $(".table-question").show();
                $(".table-question").html(data);
                // $('#viewSurveyQuestionModal').modal('show');
                surveyQuestion();
            }
        });
    }

    // VIEW SURVEY ANSWER
    $('.table tbody').on('click', '.view-survey',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("viewAttr");
        $('.survey-id').val(id);
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'html',
            data: {
                'id' : id,
                'action' : 'view_survey_question'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.table-responsive').hide();
                $(".table-question").show();
                $(".table-question").html(data);
                // $('#viewSurveyQuestionModal').modal('show');
                surveyQuestion();
            }
        });
        return false;
    });

    // UPDATE AND DELETE OF QUESTION
    function surveyQuestion(){
        $('.btn-back').click(function(){
            $('.table-responsive').show();
            $(".table-question").hide();
        });

        // VIEW SELECTED CHOICES OF A QUESTION
        $('.table tbody').on('click', '.view-survey-choices',function(){
            var questionArray = $(this).attr('getQuestionArrayNum');
            var surveyId = $(this).attr('getSurveyId');
            $(".wait").fadeIn();
          
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'html',
                data: {
                    'questionArray' : questionArray,
                    'surveyId' : surveyId,
                    'action' : 'view_survey_choices'
                },
                success: function( data ) {
                    $(".wait").fadeOut();
                    $(".display-survey-question").html(data);
                    $('#viewSurveyQuestionModal').modal('show');
                   
                }
            });
            return false;

        });

        // UPDATE SELECTED QUESTION AND CHOICES
        $('.table tbody').on('click', '.update-survey-question-choices',function(){
            var questionArray = $(this).attr('getQuestionArrayNum');
            var surveyId = $(this).attr('getSurveyId');
            $(".wait").fadeIn();
          
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'html',
                data: {
                    'questionArray' : questionArray,
                    'surveyId' : surveyId,
                    'action' : 'update_survey_question_and_choices'
                },
                success: function( data ) {
                    $(".wait").fadeOut();
                    $(".display-survey-question-choices").html(data);
                    $('#updateSurveyQuestionChoicesModal').modal('show');

                    // ADD NEW CHOICES
                    $('.update-add-survey-choices').click(function(){
                        var add = $('.update-append-question-choices').find('tr').length-1;
                        
                        if(add<=4){
                            add++;
                            $('.update-append-question-choices').append("<tr id='row_tr"+add+"' >"+
                                "<td><input type='text' name='update-survey-label[]' class='form-control' placeholder='label'></td>"+
                                "<td><input type='text' name='update-survey-value[]' class='form-control' placeholder='value'></td>"+
                                "<td><button id="+add+" class='btn btn-danger update-remove-survey-choices'><span class='glyphicon glyphicon-remove'></span> Remove</button></td>"+
                                "</tr>"); 
                        }
                        
                        return false;
                    });

                    // REMOVE CHOICES
                    $(document).on('click','.update-remove-survey-choices',function(){
                        var remove = $(this).attr('id');
                        $('#row_tr'+remove).remove();
                        return false;
                    });
                   
                }
            });
            return false;

        });

        $('.table tbody').on('click', '.delete-survey-question',function(){
            var surveyId = $(this).attr('getSurveyId');
            var questionIndex = $(this).attr('getQuestionArrayNum')-1;
            // alert(surveyId);
            // alert(questionIndex);
            $(".wait").fadeIn();
          if(confirm("Are you sure you want to delete this row?")){
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'questionIndex' : questionIndex,
                    'surveyId' : surveyId,
                    'action' : 'delete_survey_question'
                },
                success: function( data ) {

                    if (data.message=='success') {
                        $(".wait").fadeOut(function(){
                            alert("Successfully deleted");
                        });
                        // location.reload();
                        surveyQuestionsTable(surveyId);

                    }else if(data.message=='failed'){
                        $(".wait").fadeOut(function(){
                            alert("Unable to delete.\nThere are participants who answered this question");
                        });
                    }else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to delete");
                        });
                    }
                }

            });
            return false;
            }else{
                $(".wait").hide();
            }
        });
    }

    // SAVE UPDATED QUESTION AND CHOICES OF A SURVEY
        $('.save-survey-question-choices').click(function(){
            var surveyId = $('.question-survey-id').val();
            var questionIndex = $(".question-index").val();
            var question = $('.update-survey-question').val();
            var label = $('input[name="update-survey-label[]"]').map(function(){return $(this).val();}).get();
            var value = $('input[name="update-survey-value[]"]').map(function(){return $(this).val();}).get();
            
            // check if label is empty
            var checkLabel = false;
            $('input[name="update-survey-label[]"]').each(function() {
                var lb = $(this).val();
                if (!lb) {
                    checkLabel = true;
                }
            });
            // check if value is empty
            var checkValue = false;
            $('input[name="update-survey-value[]"]').each(function() {
                var vl = $(this).val();
                if (!vl) {
                    checkValue = true;
                }
            });
            
            if(!question || checkLabel || checkValue){
                $(".update-failed").fadeOut();
                $(".update-failed").fadeIn();
                $(".error-message").text("Please fill out all the fields");
            }else{
                $(".update-failed").hide();
                $('.wait').fadeIn();
                // $(".update-load").fadeIn();
                // $(".save-survey-question-choices").prop('disabled', true);
                $.ajax({
                    url: ajax_object.ajax_url,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'surveyId' : surveyId,
                        'questionIndex' : questionIndex,
                        'question' : question,
                        'label' : label,
                        'value' : value,
                        'action' : 'save_survey_question_and_choices'
                    },
                    success: function( data ) {
                        console.log( data.message );
                        $('.wait').fadeOut();
                        // $(".update-load").hide();
                        // $(".save-survey-question-choices").prop('disabled', false);
                        if(data.message=="success"){
                            $(".update-failed").fadeOut();
                            $('.wait').fadeOut(function(){
                                alert("Successfully updated");
                                surveyQuestionsTable(surveyId);
                                $('#updateSurveyQuestionChoicesModal').modal('hide');
                            });
                            // location.reload();
                        }
                        else{
                            $(".wait").fadeOut(function(){
                                alert("Failed to update");
                            });
                            
                        }
                    }
                });
            }
            return false;  //PREVENT FORM TO LOAD
        });

    // VIEW SURVEY PARTICIPANTS
    $('.table tbody').on('click', '.view-participants',function(){
        $(".wait").fadeIn();
        var participants = $(this).attr("viewPartAttr");
        var id = $(this).attr("getSurveyIdAttr");
        // alert(participants);
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'html',
            data: {
                'id' : id,
                'participants' : participants,
                'action' : 'view_survey_participants'
            },
            success: function( data ) {
                // console.log( data.firstname );
                $(".wait").fadeOut();
                $(".survey-participants").html(data);
                $('#viewSurveyParticipantsModal').modal('show');
                viewUserDetails();
                viewUserAnswers();
            }
        });
        return false;
    });

    // VIEW USER DETAILS IN PARTICIPANTS
    function viewUserDetails(){
        $('.table tbody').on('click', '.view-user-details',function(){
        $(".wait").fadeIn();
        var objectId = $(this).attr("viewUserAttr");
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'objectId' : objectId,
                    'action' : 'view_user'
                },
                success: function( data ) {
                    // console.log( data.firstname );
                    $(".wait").fadeOut();
                    $('.display-username').text(data.username);
                    $('.display-firstname').text(data.firstname);
                    $('.display-image').attr('src',data.image);
                    $('.display-lastname').text(data.lastname);
                    $('.display-birth').text(data.birth);
                    $('.display-age').text(data.age);
                    $('.display-gender').text(data.gender);
                    $('.display-email').text(data.email);
                    $('.display-address').text(data.address);
                    $('.display-province').text(data.province);
                    $('.display-city').text(data.city);
                    $('.display-barangay').text(data.barangay);
                    $('.display-points').text(data.points);
                    $('.display-number').text(data.number);
                    $('.display-items').text(data.items);
                    $('.display-redeem').text(data.redeem);
                    $('#viewUserModal').modal('show');
                }
            });
            return false;
        });
    }

    // VIEW USER ANSWERS 
    function viewUserAnswers(){
        $('.table tbody').on('click', '.view-user-answers',function(){
        $(".wait").fadeIn();
        var userId = $(this).attr("viewUserAttr");
        var surveyId = $(this).attr("viewSurveyIdAttr");
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'html',
                data: {
                    'userId' : userId,
                    'surveyId' : surveyId,
                    'action' : 'view_survey_answer'
                },
                success: function( response ) {
                    // console.log( data.firstname );
                    $(".wait").fadeOut();
                    $('.survey-answer').html(response);
                    $('#viewSurveyAnswerModal').modal('show');
                }
            });
            return false;
        });
    }

    // UPDATE SURVEY
    $('.table tbody').on('click', '.update-survey',function(){
        $(".wait").fadeIn();           
        var id = $(this).attr("updateAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_survey'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('.update-survey-title').val(data.title);
                $('.update-survey-points').val(data.points);
                if(data.active==true){
                    $('.update-survey-active').attr('checked', true);
                    $('.update-survey-label').text('Active');
                }else{
                    $('.update-survey-active').attr('checked', false);
                    $('.update-survey-label').text('Inactive');
                }
                $('#updateSurveyModal').modal('show');
            }
        });
        return false;
    });

     // SAVE SURVEY
    $(".save-survey").click(function(){
        var id = $('.object-id').val();
        var title = $('.update-survey-title').val();
        var points = $('.update-survey-points').val();
        var active = $('.update-survey-active').val();
        if(!title || !points ){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            $(".update-failed").hide();
            $(".wait").fadeIn();
            // $(".update-load").fadeIn();
            // $(".save-survey").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'title' : title,
                    'points' : points,
                    'active' : active,
                    'action' : 'update_survey'
                },
                success: function( data ) {
                    console.log( data.message );
                    // $(".update-load").hide();
                    // $(".save-survey").prop('disabled', false);
                    $(".wait").fadeOut();
                    if(data.message=="exist"){
                        $(".update-failed").fadeOut();
                        $(".update-failed").fadeIn();
                        $(".error-message").text("Name already exist!")
                    }else if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });


    // DELETE SURVEY
    $('.table tbody').on('click', '.delete-survey',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("deleteAttr");
        if(confirm("Are you sure you want to delete this row?")){
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'action' : 'delete_survey'
                },
                success: function( data ) {
                    console.log( data.message );
                    if (data.message=='success') {
                        $(".wait").fadeOut(function(){
                            alert("Successfully deleted");
                        });
                        location.reload();
                    }else if (data.message=='failed') {
                        $(".wait").fadeOut(function(){
                            alert("Unable to delete.\nThere are associated participants or questions with this survey.");
                        });
                    }else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to delete");
                        });
                    }
                }
            });
            return false;
        }else{
            $(".wait").hide();
        }
    });

    // ===============================SETTINGS===========
    // ADD SETTINGS
    $(".add-settings").click(function(){
        var terms = $(".add-settings-terms").val();
        var policy = $(".add-settings-policy").val();
        var warranty = $(".add-settings-warranty").val();

        if(!terms || !policy || !warranty ){
            $(".add-failed").fadeOut();
            $(".add-failed").fadeIn();
            $(".error-message").text("Please fill out all the fields");
        }else{
            // alert(type +reward+available);
            $(".alert-danger").hide();
            $(".add-load").fadeIn();
            $(".add-settings").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'terms' : terms,
                    'policy' : policy,
                    'warranty' : warranty,
                    'action' : 'add_settings'
                },
                success: function( data ) {
                    console.log( data.message );
                    $(".add-load").hide();
                    $(".add-settings").prop('disabled', false);
                    if(data.message=="found"){
                        $(".add-failed").fadeOut();
                        $(".add-failed").fadeIn();
                        $(".error-message").text("Terms already exist!")
                    }else if(data.message=="success"){
                        $(".add-failed").fadeOut();
                        $(".add-load").hide(function(){
                            alert("Successfully added");
                        });
                        location.reload();
                    }
                    else{
                        $(".loading").hide("slow",function(){
                            alert("Failed to add");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // VIEW SETTINGS
    $('.table tbody').on('click', '.view-settings',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("viewAttr");
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_settings'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.display-terms').text(data.terms);
                $('.display-policy').text(data.policy);
                $('.display-warranty').text(data.warranty);
                $('#viewSettingsModal').modal('show');
            }
        });
        return false;
    });

    // UPDATE VERSION SETTINGS
    $('.edit-version-settings').on('click',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("updateAttr");
        // alert(field);
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_settings'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('.update-version-settings').val(data.version);
                $('#updateVersionSettingsModal').modal('show');
            }
        });
        return false;
    });

    // UPDATE APP STORE SETTINGS
    $('.edit-appstore-settings').on('click',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("updateAttr");
        // alert(field);
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_settings'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('.update-appstore-settings').val(data.appstore);
                $('#updateAppStoreSettingsModal').modal('show');
            }
        });
        return false;
    });

    // UPDATE GOOGLE PLAY SETTINGS
    $('.edit-googleplay-settings').on('click',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("updateAttr");
        // alert(field);
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_settings'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('.update-googleplay-settings').val(data.googleplay);
                $('#updateGooglePlaySettingsModal').modal('show');
            }
        });
        return false;
    });

    // UPDATE ABOUT THE APP SETTINGS
    $('.edit-about-settings').on('click',function(){
        $(".wait").fadeIn();
        $("#aboutTheApp-tmce").trigger("click");
        var id = $(this).attr("updateAttr");
        // alert(field);
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_settings'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('#updateAboutAppSettingsModal').modal('show');
            }
        });
        return false;
    });

    // UPDATE HOW IT WORKS SETTINGS
    $('.edit-how-settings').on('click',function(){
        $(".wait").fadeIn();
        $("#howItWorks-tmce").trigger("click");
        var id = $(this).attr("updateAttr");
        // alert(field);
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_settings'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('#updateHowWorksSettingsModal').modal('show');
            }
        });
        return false;
    });

    // UPDATE TERMS AND CONDITIONS SETTINGS
    $('.edit-terms-settings').on('click',function(){
        $(".wait").fadeIn();
        $("#termsConditions-tmce").trigger("click");
        var id = $(this).attr("updateAttr");
        // alert(field);
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_settings'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('#updateTermsConditionsSettingsModal').modal('show');
            }
        });
        return false;
    });

    // UPDATE POLICY SETTINGS
    $('.edit-policy-settings').on('click',function(){
        $(".wait").fadeIn();
        $("#privacyPolicy-tmce").trigger("click");
        var id = $(this).attr("updateAttr");
        // alert(field);
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_settings'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('#updatePolicySettingsModal').modal('show');
            }
        });
        return false;
    });

    // UPDATE WARRANTY SETTINGS
    $('.edit-warranty-settings').on('click',function(){
        $(".wait").fadeIn();
        $("#warranty-tmce").trigger("click");
        var id = $(this).attr("updateAttr");
        // alert(field);
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_settings'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('#updateWarrantySettingsModal').modal('show');
            }
        });
        return false;
    });

    // UPDATE CONTACT SETTINGS
    $('.edit-contact-settings').on('click',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("updateAttr");
        // alert(field);
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                'id' : id,
                'action' : 'view_settings'
            },
            success: function( data ) {
                $(".wait").fadeOut();
                $('.object-id').val(id);
                $('.update-contact-settings').val(data.number);
                $('#updateContactSettingsModal').modal('show');
            }
        });
        return false;
    });

    // SAVE VERSION SETTINGS
    $(".save-version-settings").click(function(){
        var id = $('.object-id').val();
        var version = $('.update-version-settings').val();
        var field = "latestVersion";
        // alert(id+val)
        if(!version ){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out the field");
        }else{
            $(".update-failed").hide();
            $('.wait').fadeIn();
            // $(".update-load").fadeIn();
            // $(".save-settings").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'content' : version,
                    'field' : field,
                    'action' : 'update_content_settings'
                },
                success: function( data ) {
                    console.log( data.message );
                    $('.wait').fadeOut();
                    // $(".update-load").hide();
                    // $(".save-settings").prop('disabled', false);
                    if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // SAVE APP STORE SETTINGS
    $(".save-appstore-settings").click(function(){
        var id = $('.object-id').val();
        var url = $('.update-appstore-settings').val();
        var field = "appStoreUrl";
        // alert(id+val)
        if(!url ){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out the field");
        }else{
            $(".update-failed").hide();
            $('.wait').fadeIn();
            // $(".update-load").fadeIn();
            // $(".save-settings").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'content' : url,
                    'field' : field,
                    'action' : 'update_content_settings'
                },
                success: function( data ) {
                    console.log( data.message );
                    $('.wait').fadeOut();
                    // $(".update-load").hide();
                    // $(".save-settings").prop('disabled', false);
                    if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // SAVE GOOGLE PLAY SETTINGS
    $(".save-googleplay-settings").click(function(){
        var id = $('.object-id').val();
        var url = $('.update-googleplay-settings').val();
        var field = "googlePlayUrl";
        // alert(id+val)
        if(!url ){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out the field");
        }else{
            $(".update-failed").hide();
            $('.wait').fadeIn();
            // $(".update-load").fadeIn();
            // $(".save-settings").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'content' : url,
                    'field' : field,
                    'action' : 'update_content_settings'
                },
                success: function( data ) {
                    console.log( data.message );
                    $('.wait').fadeOut();
                    // $(".update-load").hide();
                    // $(".save-settings").prop('disabled', false);
                    if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // SAVE ABOUT SETTINGS
    $(".save-about-settings").click(function(){
        $("#aboutTheApp-html").trigger("click");
        var id = $('.object-id').val();
        var about = $('#aboutTheApp').val();
        var field = "aboutTheApp";
        // alert(about)
        if(!about ){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out the field");
        }else{
            $(".update-failed").hide();
            $('.wait').fadeIn();
            // $(".update-load").fadeIn();
            // $(".save-settings").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'content' : about,
                    'field' : field,
                    'action' : 'update_content_settings'
                },
                success: function( data ) {
                    console.log( data.message );
                    $('.wait').fadeOut();
                    // $(".update-load").hide();
                    // $(".save-settings").prop('disabled', false);
                    if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });


    // SAVE HOW IT WORKS SETTINGS
    $(".save-how-settings").click(function(){
        $("#howItWorks-html").trigger("click");
        var id = $('.object-id').val();
        var how = $('#howItWorks').val();
        var field = "howItWorks";
        // alert(about)
        if(!how ){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out the field");
        }else{
            $(".update-failed").hide();
            $('.wait').fadeIn();
            // $(".update-load").fadeIn();
            // $(".save-settings").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'content' : how,
                    'field' : field,
                    'action' : 'update_content_settings'
                },
                success: function( data ) {
                    console.log( data.message );
                    $('.wait').fadeOut();
                    // $(".update-load").hide();
                    // $(".save-settings").prop('disabled', false);
                    if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // SAVE TERMS AND CONDITIONS SETTINGS
    $(".save-terms-settings").click(function(){
        $("#termsConditions-html").trigger("click");
        var id = $('.object-id').val();
        var terms = $('#termsConditions').val();
        var field = "termsAndCondition";
        // alert(about)
        if(!terms ){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out the field");
        }else{
            $(".update-failed").hide();
            $('.wait').fadeIn();
            // $(".update-load").fadeIn();
            // $(".save-settings").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'content' : terms,
                    'field' : field,
                    'action' : 'update_content_settings'
                },
                success: function( data ) {
                    console.log( data.message );
                    $('.wait').fadeOut();
                    // $(".update-load").hide();
                    // $(".save-settings").prop('disabled', false);
                    if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // SAVE PRIVACY POLICY SETTINGS
    $(".save-policy-settings").click(function(){
        $("#privacyPolicy-html").trigger("click");
        var id = $('.object-id').val();
        var policy = $('#privacyPolicy').val();
        var field = "privacyPolicy";
        // alert(about)
        if(!policy ){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out the field");
        }else{
            $(".update-failed").hide();
            $('.wait').fadeIn();
            // $(".update-load").fadeIn();
            // $(".save-settings").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'content' : policy,
                    'field' : field,
                    'action' : 'update_content_settings'
                },
                success: function( data ) {
                    console.log( data.message );
                    $('.wait').fadeOut();
                    // $(".update-load").hide();
                    // $(".save-settings").prop('disabled', false);
                    if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // SAVE WARRANTY SETTINGS
    $(".save-warranty-settings").click(function(){
        $("#warranty-html").trigger("click");
        var id = $('.object-id').val();
        var warranty = $('#warranty').val();
        var field = "warranty";
        // alert(about)
        if(!warranty ){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out the field");
        }else{
            $(".update-failed").hide();
            $('.wait').fadeIn();
            // $(".update-load").fadeIn();
            // $(".save-settings").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'content' : warranty,
                    'field' : field,
                    'action' : 'update_content_settings'
                },
                success: function( data ) {
                    console.log( data.message );
                    $('.wait').fadeOut();
                    // $(".update-load").hide();
                    // $(".save-settings").prop('disabled', false);
                    if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // SAVE CONTACT SETTINGS
    $(".save-contact-settings").click(function(){
        var id = $('.object-id').val();
        var contact = $('.update-contact-settings').val();
        var field = "contactNumber";
        // alert(id+val)
        if(!contact ){
            $(".update-failed").fadeOut();
            $(".update-failed").fadeIn();
            $(".error-message").text("Please fill out the field");
        }else{
            $(".update-failed").hide();
            $('.wait').fadeIn();
            // $(".update-load").fadeIn();
            // $(".save-settings").prop('disabled', true);
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'content' : contact,
                    'field' : field,
                    'action' : 'update_content_settings'
                },
                success: function( data ) {
                    console.log( data.message );
                    $('.wait').fadeOut();
                    // $(".update-load").hide();
                    // $(".save-settings").prop('disabled', false);
                    if(data.message=="success"){
                        $(".update-failed").fadeOut();
                        $(".wait").fadeOut(function(){
                            alert("Successfully updated")
                        });
                        location.reload();
                    }
                    else{
                        $(".wait").fadeOut(function(){
                            alert("Failed to update");
                        });
                        
                    }
                }
            });
        }
        return false;  //PREVENT FORM TO LOAD
    });

    // DELETE SETTINGS
    $('.table tbody').on('click', '.delete-settings',function(){
        $(".wait").fadeIn();
        var id = $(this).attr("deleteAttr");
        if(confirm("Are you sure you want to delete this row?")){
            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'id' : id,
                    'action' : 'delete_settings'
                },
                success: function( data ) {
                    console.log( data.message );
                    if (data.message=='success') {
                        $(".wait").fadeOut(function(){
                            alert("Successfully deleted");
                        });
                        location.reload();
                    }else{
                        alert("Failed to delete");
                    }
                }
            });
            return false;
        }else{
            $(".wait").hide();
        }
    });

});// JQuery


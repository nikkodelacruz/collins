<?php get_header(); ?>

<div class="container">
	<!-- ADD MODAL -->
  <div class="modal fade" id="addServiceCenterModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">ADD SERVICE CENTER</h4>
        </div>
        <div class="modal-body">
			<p><span class="loading add-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
			<div class="alert alert-danger alert-dismissable fade in add-failed">
	    		<a href="#" class="close close-alert">&times;</a>
	    		<strong>Failed! </strong> <span class="error-message"></span>
	  		</div>
			<form action="" method="post" >
				<div class="form-group">
				  <label>Service center:</label>
				  <input type="text" class="form-control add-sc-name">
				</div>
				<div class="form-group">
				  <label>Working days:</label>
				  <div class="form-inline">
				  	<small>From:</small>
				  	<select class="form-control add-sc-wdf">
				  		<option value="Sunday">Sunday</option>
				  		<option value="Monday">Monday</option>
				  		<option value="Tuesday">Tuesday</option>
				  		<option value="Wednesday">Wednesday</option>
				  		<option value="Thursday">Thursday</option>
				  		<option value="Friday">Friday</option>
				  		<option value="Saturday">Saturday</option>
				  	</select>
				  	<small> To:</small>
				  	<select class="form-control add-sc-wdt">
				  		<option value="Sunday">Sunday</option>
				  		<option value="Monday">Monday</option>
				  		<option value="Tuesday">Tuesday</option>
				  		<option value="Wednesday">Wednesday</option>
				  		<option value="Thursday">Thursday</option>
				  		<option value="Friday">Friday</option>
				  		<option value="Saturday">Saturday</option>
				  	</select>
				  </div>
				 </div>
				 <div class="form-group">
					 <label>Working hours:</label>
					 <div class="form-inline">
					 	<small>From:</small>
					 	<input type="text" class="form-control add-sc-whf" readonly>
					 	<small> To:</small>
					 	<input type="text" class="form-control add-sc-wht" readonly>
					 </div>
					 
				 </div>
				<div class="form-group">
				  <label>Address:</label>
				  <input type="text" class="form-control add-sc-address">
				</div>
				<div class="form-group">
				  <label>Contact number:</label>
				  <input type="text" class="form-control add-sc-number">
				</div>
				<div class="form-group">
				  <label>Email:</label>
				  <input type="email" class="form-control add-sc-email">
				</div>
				<div class="form-group">
				  <label>Location:</label>
				  <p><input type="text" id="map-address" class="form-control"></p>
				  <div id="map" class="form-control" style="height: 400px;"></div>
				  <div class="form-inline">
				  <div class="row">
				  	<div class="col-sm-6">Latitude: <input type="text" id="map-latitude" class="form-control add-sc-latitude"></div>
				  	<div class="col-sm-6">Longitude: <input type="text" id="map-longitude" class="form-control add-sc-longitude"></div>
				  </div>
				  </div>
				</div>
				
				<input type="submit" class="btn btn-info add-service-center" value="ADD">
			</form>
        </div>
        
      </div>
    </div>
  </div><!--ADD Modal -->

 <!-- update MODAL -->
  <div class="modal fade" id="updateServiceCenterModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE SERVICE CENTER</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post" >
				<p><span class="loading update-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<div class="form-group">
				  <input type="hidden" class="object-id">
				  <label>Service center:</label>
				  <input type="text" class="form-control update-sc-name">
				</div>
				<div class="form-group">
				  <label>Working days:</label>
				  <div class="form-inline">
				  	<small>From:</small>
				  	<select class="form-control update-sc-wdf">
				  		<option value="Sunday">Sunday</option>
				  		<option value="Monday">Monday</option>
				  		<option value="Tuesday">Tuesday</option>
				  		<option value="Wednesday">Wednesday</option>
				  		<option value="Thursday">Thursday</option>
				  		<option value="Friday">Friday</option>
				  		<option value="Saturday">Saturday</option>
				  	</select>
				  	<small> To:</small>
				  	<select class="form-control update-sc-wdt">
				  		<option value="Sunday">Sunday</option>
				  		<option value="Monday">Monday</option>
				  		<option value="Tuesday">Tuesday</option>
				  		<option value="Wednesday">Wednesday</option>
				  		<option value="Thursday">Thursday</option>
				  		<option value="Friday">Friday</option>
				  		<option value="Saturday">Saturday</option>
				  	</select>
				  </div>
				 </div>
				 <div class="form-group">
					 <label>Working hours:</label>
					 <div class="form-inline">
					 	<small>From:</small>
					 	<input type="text" class="form-control update-sc-whf" readonly>
					 	<small> To:</small>
					 	<input type="text" class="form-control update-sc-wht" readonly>
					 </div>
				 </div>
				<div class="form-group">
				  <label>Address:</label>
				  <input type="text" class="form-control update-sc-address">
				</div>
				<div class="form-group">
				  <label>Contact number:</label>
				  <input type="text" class="form-control update-sc-number">
				</div>
				<div class="form-group">
				  <label>Email:</label>
				  <input type="email" class="form-control update-sc-email">
				</div>
				<div class="form-group">
				  <label>Location:</label>
				  <p><input type="text" id="update-map-address" class="form-control"></p>
				  <div id="update-map" class="form-control" style="height: 400px;"></div>
				  <div class="row">
				  	<div class="col-sm-6">Latitude: <input type="text" id="update-map-latitude" class="form-control update-sc-latitude"></div>
				  	<div class="col-sm-6">Longitude: <input type="text" id="update-map-longitude" class="form-control update-sc-longitude"></div>
				  </div>
				</div>
				<input type="submit" class="btn btn-info save-service-center" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div><!--update Modal -->

  <!-- VIEW MODAL -->
  <div class="modal fade" id="viewServiceCenterModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-warning">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">Service Center Information</h2>
        </div>
        <div class="modal-body">
			<!-- CONTENT -->
			<ul class="list-group">
			  <li class="list-group-item">
			  	<span class="display-info">Service center name:</span> <b class="display-name"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Address:</span> <b class="display-address"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Contact number:</span> <b class="display-number"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Email:</span> <b class="display-email"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Working days:</span> <b class="display-wd"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Working hours:</span> <b class="display-wh"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Location:</span>
			  	<input type="text" class="form-control display-map-address"> <br>
			  	<div class="display-map" class="form-control" style="height: 400px;"></div>
				Longitude: <input type="text" class="form-control display-latitude" readonly>
				Latitude: <input type="text" class="form-control display-longitude" readonly>
			  </li>
			</ul>
        </div>
        
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View Modal -->

</div>

<!-- DISPLAY TABLE CONTENT -->
<div class="container-fluid">
	<p><button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addServiceCenterModal"><span class="glyphicon glyphicon-plus addd"></span> ADD NEW SERVICE CENTER</button></p>
	<div class="wait" >
		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span> Please wait...</span>
	</div>
	<div class="table-responsive">
		<table class="table table-striped table-hover table-fixed text-center">
			<?php displayServiceCenterTable(); ?>
	 	</table>
	</div>
</div>

<?php get_footer(); ?>
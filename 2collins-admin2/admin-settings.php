
<br>
<div class="container">

  <!-- UPDATE VERSION MODAL -->
  <div class="modal fade" id="updateVersionSettingsModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE VERSION</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
		  		<input type="hidden" class="object-id">
				<div class="form-group">
				  <label>Latest Version:</label>
				  <input type="text" class="form-control update-version-settings">
				</div>
				<input type="submit" class="btn btn-info save-version-settings" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div>
  <!--UPDATE VERSION MODAL -->

  <!-- UPDATE APP STORE MODAL -->
  <div class="modal fade" id="updateAppStoreSettingsModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE APP STORE URL</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
		  		<input type="hidden" class="object-id">
				<div class="form-group">
				  <label>App Store URL:</label>
				  <input type="text" class="form-control update-appstore-settings">
				</div>
				<input type="submit" class="btn btn-info save-appstore-settings" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div>
  <!--UPDATE APP STORE MODAL -->

  <!-- UPDATE GOOGLE PLAY MODAL -->
  <div class="modal fade" id="updateGooglePlaySettingsModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE GOOGLE PLAY URL</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
		  		<input type="hidden" class="object-id">
				<div class="form-group">
				  <label>Google Play URL:</label>
				  <input type="text" class="form-control update-googleplay-settings">
				</div>
				<input type="submit" class="btn btn-info save-googleplay-settings" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div>
  <!--UPDATE GOOGLE PLAY MODAL -->

  <!-- UPDATE ABOUT APP MODAL -->
  <div class="modal modal-history fade" id="updateAboutAppSettingsModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE ABOUT THE APPLICATION</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<input type="hidden" class="object-id">
				<?php
					$settings = array( 'media_buttons' => false,'editor_height' => 325 ); //no media
					$content = displayAboutAppSettings(); //no content
					$editor_id = 'aboutTheApp'; //element id

					wp_editor( $content, $editor_id ,$settings);
				?>
	
				<input type="submit" class="btn btn-info save-about-settings" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div>
  <!--UPDATE ABOUT APP MODAL -->

  <!-- UPDATE HOW IT WORKS MODAL -->
  <div class="modal modal-history fade" id="updateHowWorksSettingsModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE HOW IT WORKS</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<input type="hidden" class="object-id">
				<?php
					$settings = array( 'media_buttons' => false,'editor_height' => 325 ); //no media
					$content = displayHowItWorksSettings(); //no content
					$editor_id = 'howItWorks'; //element id

					wp_editor( $content, $editor_id ,$settings);
				?>
	
				<input type="submit" class="btn btn-info save-how-settings" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div>
  <!--UPDATE HOW IT WORKS MODAL -->

  <!-- UPDATE TERMS AND CONDITIONS MODAL -->
  <div class="modal modal-history fade" id="updateTermsConditionsSettingsModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE TERMS AND CONDITIONS</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<input type="hidden" class="object-id">
				<?php
					$settings = array( 'media_buttons' => false,'editor_height' => 325 ); //no media
					$content = displayTermsConditionsSettings(); //no content
					$editor_id = 'termsConditions'; //element id

					wp_editor( $content, $editor_id ,$settings);
				?>
	
				<input type="submit" class="btn btn-info save-terms-settings" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div>
  <!--UPDATE TERMS AND CONDITIONS MODAL -->

  <!-- UPDATE PRIVACY POLICY MODAL -->
  <div class="modal modal-history fade" id="updatePolicySettingsModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE PRIVACY POLICY</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<input type="hidden" class="object-id">
				<?php
					$settings = array( 'media_buttons' => false,'editor_height' => 325 ); //no media
					$content = displayPrivacyPolicySettings(); //no content
					$editor_id = 'privacyPolicy'; //element id

					wp_editor( $content, $editor_id ,$settings);
				?>
	
				<input type="submit" class="btn btn-info save-policy-settings" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div>
  <!--UPDATE PRIVACY POLICY MODAL -->

  <!-- UPDATE WARRANTY MODAL -->
  <div class="modal modal-history fade" id="updateWarrantySettingsModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE WARRANTY</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<input type="hidden" class="object-id">
				<?php
					$settings = array( 'media_buttons' => false,'editor_height' => 325 ); //no media
					$content = displayWarrantySettings(); //no content
					$editor_id = 'warranty'; //element id

					wp_editor( $content, $editor_id ,$settings);
				?>
	
				<input type="submit" class="btn btn-info save-warranty-settings" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div>
  <!--UPDATE WARRANTY MODAL -->

   <!-- UPDATE CONTACT MODAL -->
  <div class="modal fade" id="updateContactSettingsModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE CONTACT NUMBER</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
		  		<input type="hidden" class="object-id">
				<div class="form-group">
				  <label>Contact number:</label>
				  <input type="number" class="form-control update-contact-settings">
				</div>
				<input type="submit" class="btn btn-info save-contact-settings" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div>
  <!--UPDATE CONTACT MODAL -->

</div>

<!-- DISPLAY TABLE CONTENT -->
<div class="container-fluid">
	<p class="wait" >
		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
		<span> Please wait...</span>
	</p>

	<!-- <div class="well"> -->
		<?php displaySettings(); ?>
	<!-- </div> -->
	
	
</div>


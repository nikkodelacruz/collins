

<br>
<div class="container">
	<!-- ADD MODAL -->
  <div class="modal fade" id="addBrandModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">ADD BRAND</h4>
        </div>
        <div class="modal-body">
			<p><span class="loading add-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
			<div class="alert alert-danger alert-dismissable fade in add-failed">
	    		<a href="#" class="close close-alert">&times;</a>
	    		<strong>Failed! </strong> <span class="error-message"></span>
	  		</div>
			<form action="" method="post" >
				<div class="form-group">
				  <label>Brand name:</label>
				  <input type="text" class="form-control add-brand-name">
				</div>
				<div class="form-group">
				  <label>Categories:</label>
				  <input type="text" class="form-control add-brand-category">
				</div>
				<input type="submit" class="btn btn-info add-brand" value="ADD">
			</form>
        </div>
        
      </div>
    </div>
  </div><!--ADD Modal -->
  <!-- UPDATE MODAL -->
  <div class="modal fade" id="updateBrandModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE INFORMATION</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<p><span class="loading update-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<div class="form-group">
					<input type="hidden" class="object-id">
				  	<label>Brand name:</label>
				  	<input type="text" class="form-control update-brand-name">
				</div>
				<div class="form-group">
				  	<label>Categories:</label>
				  	<input type="text" class="form-control update-brand-category">
				</div>
				<input type="submit" class="btn btn-info save-brand" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div><!--View Modal -->
</div>

<!-- DISPLAY TABLE CONTENT -->
<div class="container-fluid">
	<p><button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addBrandModal"><span class="glyphicon glyphicon-plus addd"></span> ADD NEW BRAND</button></p>
	<div class="wait" >
		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span> Please wait...</span>
	</div>
	<div class="table-responsive">
		<table class="table table-striped table-hover table-fixed text-center">
			<?php displayBrandTable(); ?>
	 	</table>
	</div>
</div>

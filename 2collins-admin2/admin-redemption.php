
<br>
<div class="container">
	<!-- ADD MODAL -->
  <div class="modal fade" id="addBrandModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">ADD REDEMPTION</h4>
        </div>
        <div class="modal-body">
			<p><span class="loading add-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
			<div class="alert alert-danger alert-dismissable fade in add-failed">
	    		<a href="#" class="close close-alert">&times;</a>
	    		<strong>Failed! </strong> <span class="error-message"></span>
	  		</div>
			<form action="" method="post" >
				<div class="form-group">
				  <label>Brand name:</label>
				  <input type="text" class="form-control add-brand-name">
				</div>
				<div class="form-group">
				  <label>Categories:</label>
				  <input type="text" class="form-control add-brand-category">
				</div>
				<input type="submit" class="btn btn-info add-brand" value="ADD">
			</form>
        </div>
        
      </div>
    </div>
  </div><!--ADD Modal -->

  <!-- UPDATE MODAL -->
  <div class="modal fade" id="updateRedemptionModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE INFORMATION</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<p><span class="loading update-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<div class="form-group">
					<input type="hidden" class="object-id">
					<input type="hidden" class="redemption-type">
				  	<label>Status:</label>
				  	<select class="form-control update-redemption-status">
				  		<option value="pending">Pending</option>
				  		<option value="accepted">Accepted</option>
				  		<option value="declined">Declined</option>
				  	</select>
				</div>
				<div class="form-group">
				  	<label>Points:</label>
				  	<input type="number" class="form-control update-redemption-points">
				</div>
				<input type="submit" class="btn btn-info save-redemption" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div><!--View Modal -->

  <!-- VIEW USER DETAILS MODAL -->
  <div class="modal fade" id="viewUserModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-warning">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">USER'S DETAILS</h2>
        </div>
        <div class="modal-body">
			<!-- CONTENT -->
        
			<ul class="list-group">
			  <li class="text-center">
			 	<img class="display-image img-rounded img-thumbnail" src="" alt="no image" width="200" height="200">
			  </li>
			  <br>
			  <li class="list-group-item">
			  	<span class="display-info" >Username:</span> <b class="display-username"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Firstname:</span> <b class="display-firstname"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Lastname:</span> <b class="display-lastname"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Birth month:</span> <b class="display-birth"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Age:</span> <b class="display-age"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Gender:</span> <b class="display-gender"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Email:</span> <b class="display-email"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Address:</span> <b class="display-address"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Contact number:</span> <b class="display-number"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Province:</span> <b class="display-province"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">City:</span> <b class="display-city"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Barangay:</span> <b class="display-barangay"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Points:</span> <b class="display-points"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Number of items:</span> <b class="display-items"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Pending Redeem Points:</span> <b class="display-redeem"></b>
			  </li>
			</ul>
        </div>
        
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View USER DETAILS Modal -->

<!-- VIEW PRODUCT MODAL -->
  <div class="modal fade" id="viewRewardProductModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-warning">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">Reward Product Information</h2>
        </div>
        <div class="modal-body">
			<!-- CONTENT -->
			<ul class="list-group">
			  <li class="list-group-item text-center">
			 	<img class="display-image img-responsive" src="" alt="no image">
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Reward product name:</span> <b class="display-name"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Description:</span> <p class="display-description"></p>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Reward Type:</span> <b class="display-type"></b>
			  </li>
			  <li class="list-group-item">
			  	<div class="display-reward-type">
			  		
			  	</div>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Points:</span> <b class="display-points"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Status:</span> <b class="display-available"></b>
			  </li>
			</ul>
        </div>
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View PRODUCT Modal -->
	

</div>

<!-- DISPLAY TABLE CONTENT -->
<div class="container-fluid">
	<!-- <p><button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addBrandModal"><span class="glyphicon glyphicon-plus addd"></span> ADD NEW REDEMPTION</button></p> -->
	<div class="wait" >
		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span> Please wait...</span>
	</div>

	<p>
		<label>Filter:</label>
		<select class="filterType">
			<option value="">All Types</option>
			<option value="product">Product</option>
			<option value="voucher">Voucher</option>
		</select>
		<select class="filterRedemptStatus">
			<option value="">All Status</option>
			<option selected="selected" value="pending">Pending</option>
		 	<option value="accepted">Accepted</option>
		 	<option value="declined">Declined</option>
		</select>
	</p>
	<div class="table-responsive">
		<table class="table asd table-striped table-hover table-fixed text-center">
			<?php displayRedemptionTable(); ?>
	 	</table>
	</div>
</div>




<br>
<div class="container-fluid">
  <!-- VIEW DETAILS MODAL -->
  <div class="modal fade" id="viewUserModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-warning">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">USER'S DETAILS</h2>
        </div>
        <div class="modal-body">
			<!-- CONTENT -->
        
			<ul class="list-group">
			  <li class="text-center">
			 	<img class="display-image img-rounded img-thumbnail" src="" alt="no image" width="200" height="200">
			  </li>
			  <br>
			  <li class="list-group-item">
			  	<span class="display-info" >Username:</span> <b class="display-username"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Firstname:</span> <b class="display-firstname"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Lastname:</span> <b class="display-lastname"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Birth month:</span> <b class="display-birth"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Age:</span> <b class="display-age"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Gender:</span> <b class="display-gender"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Email:</span> <b class="display-email"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Address:</span> <b class="display-address"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Contact number:</span> <b class="display-number"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Province:</span> <b class="display-province"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">City:</span> <b class="display-city"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Barangay:</span> <b class="display-barangay"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Points:</span> <b class="display-points"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Number of items:</span> <b class="display-items"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Pending Redeem Points:</span> <b class="display-redeem"></b>
			  </li>
			</ul>
        </div>
        
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View DETAILS Modal -->

<!-- VIEW HISTORY MODAL -->
  <div class="modal modal-history fade" id="viewUserHistoryModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">USER'S HISTORY</h2>
        </div>
        <div class="modal-body user-history table-responsive">
		<!-- CONTENT -->
       
        </div>
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View HISTORY Modal -->

<!-- VIEW PRODUCT MODAL -->
  <div class="modal modal-products fade" id="viewUserProductsModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-info">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">USER'S REGISTERED PRODUCTS</h2>
        </div>
        <div class="modal-body user-products table-responsive">
		<!-- CONTENT -->
        </div>
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View PRODUCT Modal -->

  <!-- UPDATE POINTS MODAL -->
  <div class="modal fade" id="updateUserPointsModal" role="dialog" >
    <div class="modal-dialog modal-sm" style="margin-top: 10%;">
      <div class="modal-content panel-default">
        <div class="modal-body">
			<form action="" method="post">
				<p><span class="loading update-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<div class="form-group">
					<input type="hidden" class="object-id">
					<label>Current points:</label>
				  	<input type="number" class="form-control user-points" readonly>
				  	<br>
				  	<label>Points to add or deduct:</label>
				  	<input type="number" class="form-control edit-user-points">
				</div>
				<input type="submit" class="btn btn-info add-user-points" value="ADD">
				<input type="submit" class="btn btn-info deduct-user-points" value="DEDUCT">
				<input type="submit" class="btn btn-danger close-points-update" data-dismiss="modal" value="CLOSE">
			</form>
        </div>
      </div>
    </div>
  </div> <!-- UPDATE POINTS MODAL -->


</div>

<!-- DISPLAY TABLE CONTENT -->
<div class="container-fluid">
<p class="wait" ><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span> Please wait...</span></p>
	<div class="table-responsive">
		<table id="tab" class="table table-striped table-hover">
			<?php displayUserTable(); ?>
	 	</table>
	</div>
</div>




<br>
<div class="container">
	<!-- ADD MODAL -->
  <div class="modal fade" id="addBrandProductModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">ADD BRAND PRODUCT</h4>
        </div>
        <!--  -->
        <div class="modal-body">
			<p><span class="loading add-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
			<div class="alert alert-danger alert-dismissable fade in add-failed">
	    		<a href="#" class="close close-alert">&times;</a>
	    		<strong>Failed! </strong> <span class="error-message"></span>
	  		</div>
			<form action="" method="post" enctype="multipart/form-data" >
				<div class="form-group">
				  <label>Brand product name:</label>
				  <input type="text" class="form-control add-brand-product-name">
				</div>
				<div class="form-group">
				  <label>Item code:</label>
				  <input type="text" class="form-control add-brand-product-code">
				</div>
				<div class="form-group">
				  <label>Description:</label>
				  <textarea class="form-control add-brand-product-description" rows="3"></textarea>
				</div>
				<div class="form-group">
				  <label>Colors:</label>
				  <input type="text" class="form-control add-brand-product-colors">
				</div>
				<div class="form-group">
				  <label>Image:</label>
				  <input type="file" id="add-brand-product-image" class="form-control add-brand-product-image" accept="image/*">
				</div>
				<div class="form-group">
				  <label>Price:</label>
					<div class="input-group">
					  <span class="input-group-addon">&#8369;</span>
					  <input type="text" class="form-control add-brand-product-price">
					</div>
				</div>
				<div class="form-group">
				  <label>Brand:</label>
				  <?php displayBrandName() ?>
				</div>
				<div class="form-group">
				  <label>Category:</label>
				  <i class="fa fa-spinner fa-spin wait-category"></i>
				  <div class="select-brand-category">
				  
				  </div>
				</div>
				<div class="form-group">
				  <label>Quantity:</label><br>
				  <label class="radio-inline">
				      <input type="radio" value="true" name="quantity">Quantity purchase
				  </label>
				  <label class="radio-inline">
				      <input type="radio" value="false" name="quantity">Single purchase
				  </label>
				</div>
				<div class="form-group">
				  <label>Points:</label>
				  <input type="number" class="form-control add-brand-product-points" >
				</div>
				<input type="submit" class="btn btn-info add-brand-product" value="ADD">
			</form>
        </div>
        
      </div>
    </div>
  </div><!--ADD Modal -->

  <!-- VIEW MODAL -->
  <div class="modal fade" id="viewBrandProductModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-warning">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h2 class="modal-title">Brand Product Information</h2>
        </div>
        <div class="modal-body">
			<!-- CONTENT -->
			<ul class="list-group">
			  <li class="list-group-item text-center">
			 	<img class="display-image img-responsive" src="" alt="no image">
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info" >Item code:</span> <b class="display-item"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info" >Description:</span> <p class="display-description"></p>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info" >Colors:</span> <b class="display-colors"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Brand:</span> <b class="display-brand"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Name:</span> <b class="display-name"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Category:</span> <b class="display-category"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Price:</span> <b>&#8369;</b><b class="display-price"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Points:</span> <b class="display-points"></b>
			  </li>
			  <li class="list-group-item">
			  	<span class="display-info">Quantity:</span> <b class="display-quantity"></b>
			  </li>
			</ul>
        </div>
        
        <div class="modal-footer panel-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><!--View Modal -->

  <!-- UPDATE MODAL -->
  <div class="modal fade" id="updateBrandProductModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content panel-default">
        <div class="modal-header btn-success">
          <button type="button" class="close" data-dismiss="modal">
          	<span class="glyphicon glyphicon-remove"></span>
          </button>
          <h4 class="modal-title">UPDATE INFORMATION</h4>
        </div>
        <div class="modal-body">
			<form action="" method="post">
				<p><span class="loading update-load"><i class="fa fa-spinner fa-spin"></i> Please wait...</span></p>
				<div class="alert alert-danger alert-dismissable fade in update-failed">
		    		<a href="#" class="close close-alert" >&times;</a>
		    		<strong>Failed! </strong> <span class="error-message"></span>
		  		</div>
				<div class="form-group">
				  <input type="hidden" class="object-id">
				  <label>Brand product name:</label>
				  <input type="text" class="form-control update-brand-product-name">
				</div>
				<div class="form-group">
				  <label>Item code:</label>
				  <input type="text" class="form-control update-brand-product-code">
				</div>
				<div class="form-group">
				  <label>Description:</label>
				  <textarea class="form-control update-brand-product-description" rows="3"></textarea>
				</div>
				<div class="form-group">
				  <label>Colors:</label>
				  <input type="text" class="form-control update-brand-product-colors">
				</div>
				<div class="form-group">
				  <label>Image:</label>
				  <input type="file" id="update-brand-product-image" class="form-control update-brand-product-image" accept="image/*">
				</div>
				<div class="form-group">
				  <label>Price:</label>
					<div class="input-group">
					  <span class="input-group-addon">&#8369;</span>
					  <input type="text" class="form-control update-brand-product-price">
					</div>
				</div>
				<div class="form-group">
				  <label>Brand:</label>
				  <i class="fa fa-spinner fa-spin wait-category"></i>
				  <input type="hidden" class="show-category">
				  <div class="update-brand-name-and-category">
				  	
				  </div>
				</div>
				<!-- <div class="form-group">
				  
				  
				  <div class="update-brand-category">
				  
				  </div>
				</div> -->
				<div class="form-group">
				  <label>Quantity:</label><br>
				  <label class="radio-inline">
				      <input type="radio" value="1" name="update-quantity">Quantity purchase
				  </label>
				  <label class="radio-inline">
				      <input type="radio" value="0" name="update-quantity">Single purchase
				  </label>
				</div>
				<div class="form-group">
				  <label>Points:</label>
				  <input type="number" class="form-control update-brand-product-points" >
				</div>
				<input type="submit" class="btn btn-info save-brand-product" value="UPDATE">
			</form>
        </div>
      </div>
    </div>
  </div><!--View Modal -->
</div>

<!-- DISPLAY TABLE CONTENT -->
<div class="container-fluid">
	<p><button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addBrandProductModal"><span class="glyphicon glyphicon-plus addd"></span> ADD NEW BRAND PRODUCT</button></p>
	<div class="wait" >
		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span> Please wait...</span>
	</div>
	<p>
		<label>Filter:</label>
		<select class="filterBrandName">
			<?php filterBrandName(); ?>
		</select>	
	</p>
	<div class="table-responsive">

		<table class="table table-striped table-hover table-fixed text-center">
			<?php displayBrandProductTable(); ?>
	 	</table>
	</div>
</div>

<?php 

// if ( ! function_exists( 'enqueue_back_end' ) ) {


    // ADMIN BACKEND FOR MOBILE
    function enqueue_back_end() {
    	//function to call style.css
        wp_enqueue_style('style',get_stylesheet_directory_uri() . '/collins-admin/admin-style.css');
        
        wp_enqueue_script('custom_javascript',get_template_directory_uri() . '/collins-admin/js/script.js',  array('jquery'), null, true);
        //include CDN jquery
       // wp_enqueue_script( 'ajax-script', plugins_url( ' js/ajax.js', __FILE__ ), array('jquery') );

       //  // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
       //  wp_localize_script( 'ajax-script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) ); 
        // AJAX


        wp_enqueue_script('ajax-script', get_template_directory_uri() . '/collins-admin/js/ajax.js', array('jquery'),null, true );
        wp_localize_script( 'ajax-script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) ); 
       
       // wp_enqueue_script('custom_script');
        // //include CDN jquery
        wp_deregister_script('jquery');
        wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', array(), null, true);

        // Jquery validator
        wp_enqueue_script('jquery','https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js',array(),null,true);

        wp_enqueue_script('jquery','https://jqueryvalidation.org/files/lib/jquery.js',array(),null,true);

        // include CDN bootstrap
        wp_deregister_script('bootstrap');
        wp_enqueue_script('bootstrap','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',array(), null, true);
        // bootstrap css
        wp_enqueue_style('bootstrap_css','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
        // include CDN fontawesome
        wp_enqueue_style('fontawesome','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');


        // DATATABLE CDN
        // wp_enqueue_style('datatable_css','//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css');
        wp_enqueue_style('datatable3','https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css');
        wp_deregister_script('data_table');
        wp_enqueue_script('data_table','//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js', array(), null, true);
        wp_deregister_script('data_table_bootstrap');
        wp_enqueue_script('data_table_bootstrap','https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js', array(), null, true);

        // TIME PICKER
        wp_enqueue_script( 'timepicker_js', get_bloginfo( 'template_url' ) . '/collins-admin/js/src/jquery.ptTimeSelect.js', array( 'jquery' ), '', true );
        wp_enqueue_style( 'timepicker_css' ,get_bloginfo( 'template_url' ).'/collins-admin/js/src/jquery.ptTimeSelect.css','',null );

        // DATE AND TIME PICKER
        wp_enqueue_style( 'datetimepicker_css', '//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css',  '', null );
        wp_enqueue_script( 'moment_js' ,'//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js',array( 'jquery' ),'',true );
        wp_enqueue_script( 'datetimepicker_js' ,'//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js',array( 'jquery' ),'',true );

        // LOCATIO PICKER
        wp_enqueue_script( 'googlemap' ,'http://maps.google.com/maps/api/js?key=AIzaSyC6R27D7FqK39y7nz85mVtSjTbbpBsox_8&sensor=false&libraries=places',array( 'jquery' ),'',true );
        // wp_enqueue_script( 'location_library' ,'dist/locationpicker.jquery.min.js',array( 'jquery' ),'',true );
        wp_enqueue_script( 'location_library', get_bloginfo( 'template_url' ) . '/collins-admin/js/dist/locationpicker.jquery.min.js', array( 'jquery' ), '', true );

    }

    add_action( 'admin_enqueue_scripts', 'enqueue_back_end' );
    // add_action( 'wp_enqueue_scripts', 'enqueue_back_end' );

// }

// ADDING ADMIN MENU
function admin_collins_menu() {
    add_menu_page('Admin', 'User Admin', 'manage_options', 'collins_menu', 'display_user', 'dashicons-businessman', 2 );
    add_submenu_page('collins_menu', 'Admin', 'Users', 'manage_options', 'collins_menu', 'display_user' );
    add_submenu_page('collins_menu', 'Brands', 'Brands', 'manage_options', 'collins_menu_brand', 'display_brand' );
    add_submenu_page('collins_menu', 'Brand Products', 'Brand Products', 'manage_options', 'collins_menu_brand_product', 'display_brand_product' );
    add_submenu_page('collins_menu', 'Stores', 'Stores', 'manage_options', 'collins_menu_store', 'display_store' );
    add_submenu_page('collins_menu', 'Store Branches', 'Store Branches', 'manage_options', 'collins_menu_store_branch', 'display_store_branch' );
    add_submenu_page('collins_menu', 'Service Centers', 'Service Centers', 'manage_options', 'collins_menu_service_center', 'display_service_center' );
    add_submenu_page('collins_menu', 'Surveys', 'Surveys', 'manage_options', 'collins_menu_survey', 'display_survey' );
    add_submenu_page('collins_menu', 'Articles', 'Articles', 'manage_options', 'collins_menu_article', 'display_article' );
    add_submenu_page('collins_menu', 'Service Center Appointments', 'Service Center Appointments', 'manage_options', 'collins_menu_appointments', 'display_appointment' );
    add_submenu_page('collins_menu', 'Products', 'Products', 'manage_options', 'collins_menu_product', 'display_product' );
    add_submenu_page('collins_menu', 'Redemptions', 'Redemptions', 'manage_options', 'collins_menu_redemptions', 'display_redemption' );
    add_submenu_page('collins_menu', 'Reward Products', 'Reward Products', 'manage_options', 'collins_menu_reward', 'display_reward_product' );
    add_submenu_page('collins_menu', 'Settings', 'Settings', 'manage_options', 'collins_menu_settings', 'display_settings' );
}
add_action( 'admin_menu', 'admin_collins_menu' );


function display_user(){
    echo "<h1><span class='label label-default'>Users</span></h1>";
    require_once ( 'admin-users.php' );
}

function display_brand(){
    echo "<h1><span class='label label-default'>Brands</span></h1>";
    require_once ( 'admin-brand.php' );
}

function display_brand_product(){
    echo "<h1><span class='label label-default'>Brand Products</span></h1>";
    require_once ( 'amdin-brand-product.php' );
}

function display_store(){
    echo "<h1><span class='label label-default'>Stores</span></h1>";
    require_once ( 'admin-store.php' );
}

function display_store_branch(){
    echo "<h1><span class='label label-default'>Store Branches</span></h1>";
    require_once ( 'admin-store-branch.php' );
}

function display_service_center(){
    echo "<h1><span class='label label-default'>Service Centers</span></h1>"; 
    require_once ( 'admin-service-center.php' );
}

function display_survey(){
    echo "<h1><span class='label label-default'>Surveys</span></h1>";
    require_once ( 'admin-survey.php' );
}

function display_article(){
    echo "<h1><span class='label label-default'>Articles</span></h1>";
    require_once ( 'admin-article.php' );
}

function display_appointment(){
    echo "<h1><span class='label label-default'>Service Center Appointments</span></h1>";
    require_once ( 'admin-service-center-appointment.php' );
}

function display_product(){
    echo "<h1><span class='label label-default'>Products</span></h1>";
    require_once ( 'admin-product.php' );
}

function display_redemption(){
    echo "<h1><span class='label label-default'>Redemptions</span></h1>";
    require_once ( 'admin-redemption.php' );
}

function display_reward_product(){
    echo "<h1><span class='label label-default'>Reward Products</span></h1>";
    require_once ( 'admin-reward-product.php' );
}

function display_settings(){
    echo "<h1><span class='label label-default'>Settings</span></h1>";
    require_once ( 'admin-settings.php' );
}

// PARSE CRUD==================================================
// AJAX FUNCTIONS 
include 'ParseQueryFunctions.php';  

// ==============================================USERS=========================

// DISPLAY USER INFOMATION
function view_user() {
    $objectId = $_POST['objectId'];
    $result = viewUser($objectId);
    $response = array(
                    'dateCreated'=>$result['dateCreated'],
                    'dateUpdated'=>$result['dateUpdated'],
                    'image'=>$result['image'],
                    'username'=>$result['username'], 
                    'firstname'=>$result['firstname'],
                    'lastname'=>$result['lastname'],
                    'birth'=>$result['birth'],
                    'age'=>$result['age'],
                    'gender'=>$result['gender'],
                    'email'=>$result['email'],
                    'address'=>$result['address'],  
                    'province'=>$result['province'],   
                    'city'=>$result['city'],
                    'barangay'=>$result['barangay'],
                    'points'=>$result['points'],
                    'number'=>$result['number'],
                    'items'=>$result['items'], 
                    'redeem'=>$result['redeem'] 
                );
    wp_send_json($response);
}
add_action( 'wp_ajax_view_user', 'view_user' );
add_action( 'wp_ajax_nopriv_view_user', 'view_user' );
    
// DISPLAY USER HISTORY
function view_user_history() {
    $id = $_POST['id'];
    $result = viewUserHistoryTable($id);
    echo $result;
    die();
}
add_action( 'wp_ajax_view_user_history', 'view_user_history' );
add_action( 'wp_ajax_nopriv_view_user_history', 'view_user_history' );

// UPDATE POINTS
function update_user_points() {
    $id = $_POST['id'];
    $points = $_POST['points'];
    $type = $_POST['type'];

    $result = updateUserPoints($id,$points,$type);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_update_user_points', 'update_user_points' );
add_action( 'wp_ajax_nopriv_update_user_points', 'update_user_points' );

// DISPLAY USER PODUCTS
function view_user_products() {
    $id = $_POST['id'];
    $result = viewUserProductsTable($id);
    echo $result;
    die();
    
}
add_action( 'wp_ajax_view_user_products', 'view_user_products' );
add_action( 'wp_ajax_nopriv_view_user_products', 'view_user_products' );

// ===========================================BRANDS================================
// DISPLAY BRAND
function view_brand() {
    $id = $_POST['id'];
    $result = viewBrand($id);
    $response = array(
                    'name'=>$result['name'],
                    'category'=>$result['category'] 
                );
    wp_send_json($response);
}
add_action( 'wp_ajax_view_brand', 'view_brand' );
add_action( 'wp_ajax_nopriv_view_brand', 'view_brand' );

// ADD NEW BRAND
function add_brand() {
    $brand = $_POST['name'];
    $category = $_POST['category'];
    $result = addBrand($brand,$category);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_add_brand', 'add_brand' );
add_action( 'wp_ajax_nopriv_add_brand', 'add_brand' );

// UPDATE BRAND
function update_brand() {
    $id = $_POST['id']; 
    $name = $_POST['name'];
    $category = $_POST['category'];
    $result = updateBrand($id,$name,$category);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_update_brand', 'update_brand' );
add_action( 'wp_ajax_nopriv_update_brand', 'update_brand' );

// DELETE BRAND
function delete_brand() {
    $id = $_POST['id'];
    $result = deleteBrand($id);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_delete_brand', 'delete_brand' );
add_action( 'wp_ajax_nopriv_delete_brand', 'delete_brand' );

// ===============================BRAND PRODUCTS=========

// DISPLAY USER INFOMATION
function view_brand_product() {
    $objectId = $_POST['id'];
    $result = viewBrandProduct($objectId);
    $response = array(
                    'dateCreated'=>$result['dateCreated'],
                    'dateUpdated'=>$result['dateUpdated'],
                    'image'=>$result['image'], 
                    'item'=>$result['item'], 
                    'name'=>$result['name'], 
                    'description'=>$result['description'],
                    'colors'=>$result['colors'],
                    'brand'=>$result['brand'],
                    'category'=>$result['category'],
                    'price'=>$result['price'],
                    'points'=>$result['points'],
                    'quantity'=>$result['quantity'] 
                );
    wp_send_json($response);
}
add_action( 'wp_ajax_view_brand_product', 'view_brand_product' );
add_action( 'wp_ajax_nopriv_view_brand_product', 'view_brand_product' );

// SELECT OPTION FOR BRAND CATEGORY
function view_brand_category() {
    $id = $_POST['id'];
    $result = displayBrandCategory($id);
    echo $result;
    die();  
}
add_action( 'wp_ajax_view_brand_category', 'view_brand_category' );
add_action( 'wp_ajax_nopriv_view_brand_category', 'view_brand_category' );

// DISPLAY BRAND NAME AND CATEGORY IN UPDATE
function view_brand_name_and_category() {
    $id = $_POST['id'];
    $category = $_POST['category'];
    $result = displayBrandNameUpdate($id,$category);
    echo $result;
    die();  
}
add_action( 'wp_ajax_view_brand_name_and_category', 'view_brand_name_and_category' );
add_action( 'wp_ajax_nopriv_view_brand_name_and_category', 'view_brand_name_and_category' );

// DISPLAY BRAND CATEGORY IN UPDATE
function view_brand_name_category() {
    $id = $_POST['id'];
    $category = $_POST['category'];
    $result = displayBrandCategoryUpdate($id,$category);
    echo $result;
    die();  
}
add_action( 'wp_ajax_view_brand_name_category', 'view_brand_name_category' );
add_action( 'wp_ajax_nopriv_view_brand_name_category', 'view_brand_name_category' );

// ADD NEW BRAND PRODUCT
function add_brand_product() { 
    $image = 'image';
  
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    // CHECK IF IMAGE
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        $result['status']='not_image';
    }else{
        // $image_attributes[1];//GET THE IMAGE WIDTH
        // $image_attributes[2];//GET THE IMAGE HEIGHT
        $name = $_POST['name'];
        $code = $_POST['code'];
        $description = $_POST['description'];
        $colors = $_POST['colors'];
        $price = $_POST['price'];
        $id = $_POST['brand'];// brand id
        $category = $_POST['category'];
        $points = $_POST['points'];
        $quantity = $_POST['quantity']; 
        $result = addBrandProduct($id,$name,$code,$description,$colors,$price,$category,$points,$quantity,$image);
    }

    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_add_brand_product', 'add_brand_product' );
add_action( 'wp_ajax_nopriv_add_brand_product', 'add_brand_product' );

// UPDATE BRAND PRODUCT
function update_brand_product() { 
    $image = 'image';
    
    $data = $_POST['data'];

    // if no image
    if(!$data){
        $id = $_POST['id'];
        $name = $_POST['name'];
        $code = $_POST['code'];
        $description = $_POST['description'];
        $colors = $_POST['colors'];
        $price = $_POST['price'];
        $brand = $_POST['brand'];// brand id
        $category = $_POST['category'];
        $points = $_POST['points'];
        $quantity = $_POST['quantity']; 

        $result = updateBrandProduct($id,$name,$code,$description,$colors,$price,$brand,$category,$points,$quantity,"no_image");
                
    }else{
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        // CHECK IF IMAGE
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
         $result['status']='not_image';
        }else{
            $id = $_POST['id'];
            $name = $_POST['name'];
            $code = $_POST['code'];
            $description = $_POST['description'];
            $colors = $_POST['colors'];
            $price = $_POST['price'];
            $brand = $_POST['brand'];// brand id
            $category = $_POST['category'];
            $points = $_POST['points'];
            $quantity = $_POST['quantity']; 

            $result = updateBrandProduct($id,$name,$code,$description,$colors,$price,$brand,$category,$points,$quantity,$image);
        }
    }
    
    
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_update_brand_product', 'update_brand_product' );
add_action( 'wp_ajax_nopriv_update_brand_product', 'update_brand_product' );

// DELETE BRAND PRODUCT
function delete_brand_product() {
    $id = $_POST['id'];
    $result = deleteBrandProduct($id);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_delete_brand_product', 'delete_brand_product' );
add_action( 'wp_ajax_nopriv_delete_brand_product', 'delete_brand_product' );

// ==========================================STORES======================
// ADD NEW STORE
function add_store() {
    $store = $_POST['name'];
    $result = addStore($store);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_add_store', 'add_store' );
add_action( 'wp_ajax_nopriv_add_store', 'add_store' );

// VIEW STORE
function view_store() {
    $id = $_POST['id'];
    $result = viewStore($id);
    $response = array('name' => $result['name']);
    wp_send_json($response);
}
add_action( 'wp_ajax_view_store', 'view_store' );
add_action( 'wp_ajax_nopriv_view_store', 'view_store' );

// UPDATE STORE
function update_store() {
    $id = $_POST['id']; 
    $brand = $_POST['name'];
    $result = updateStore($id,$brand);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_update_store', 'update_store' );
add_action( 'wp_ajax_nopriv_update_store', 'update_store' );

// DELETE STORE
function delete_store() {
    $id = $_POST['id'];
    $result = deleteStore($id);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_delete_store', 'delete_store' );
add_action( 'wp_ajax_nopriv_delete_store', 'delete_store' );

// ==================STRORE BRANCH=================================
// ADD NEW STORE BRANCH
function add_store_branch() {
    $id = $_POST['id'];
    $name = $_POST['name'];
    $result = addStoreBranch($id,$name);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_add_store_branch', 'add_store_branch' );
add_action( 'wp_ajax_nopriv_add_store_branch', 'add_store_branch' );

// VIEW STORE BRANCH
function view_store_branch() {
    $id = $_POST['id'];
    $result = viewStoreBranch($id);
    $response = array(
                    'name' => $result['name'], 
                    'store'=> $result['store'], 
                    'storeId'=> $result['storeId']
                    );
    wp_send_json($response);
}
add_action( 'wp_ajax_view_store_branch', 'view_store_branch' );
add_action( 'wp_ajax_nopriv_view_store_branch', 'view_store_branch' );

// SELECT STORE NAME UPDATE
function view_store_name() {
    $id = $_POST['id'];
    $result = displayStoreNameUpdate($id);
    echo $result;
    die();  
}
add_action( 'wp_ajax_view_store_name', 'view_store_name' );
add_action( 'wp_ajax_nopriv_view_store_name', 'view_store_name' );

// UPDATE STORE BRANCH
function update_store_branch() {
    $id = $_POST['id']; 
    $name = $_POST['name'];
    $store = $_POST['store'];
    $result = updateStoreBranch($id,$name,$store);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_update_store_branch', 'update_store_branch' );
add_action( 'wp_ajax_nopriv_update_store_branch', 'update_store_branch' );

// DELETE STORE BRANCH
function delete_store_branch() {
    $id = $_POST['id'];
    $result = deleteStoreBranch($id);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_delete_store_branch', 'delete_store_branch' );
add_action( 'wp_ajax_nopriv_delete_store_branch', 'delete_store_branch' );


// ==================ARTICLE=================================
// ADD NEW ATICLE
function add_article() {
    $image = 'image';
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    // CHECK IF IMAGE
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    if($imageFileType != "jpg" && 
        $imageFileType != "JPG" && 
        $imageFileType != "png" && 
        $imageFileType != "PNG" && 
        $imageFileType != "jpeg" && 
        $imageFileType != "JPEG" && 
        $imageFileType != "gif" ) {
        $result['status']='not_image';
    }else{
        $title = $_POST['title'];
        $content = $_POST['content']; 
        $result = addArticle($title,$image,$content);
    }
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_add_article', 'add_article' );
add_action( 'wp_ajax_nopriv_add_article', 'add_article' );

// VIEW ARTICLE
function view_article() {
    $id = $_POST['id'];
    $result = viewArticle($id);
    $response = array(
                    'title' => $result['title'],
                    'image' => $result['image'],
                    'date' => $result['date'],
                    'date2' => $result['date2'],
                    'content' => $result['content'],
                    'views' => $result['views']
                    );
    wp_send_json($response);
}
add_action( 'wp_ajax_view_article', 'view_article' );
add_action( 'wp_ajax_nopriv_view_article', 'view_article' );

// UPDATE ARTICLE
function update_article() {
    $id = $_POST['id']; 
    $title = $_POST['title'];
    $date = $_POST['date'];
    $content = $_POST['content']; 
    $image = 'image';
    $data = $_POST['data'];

    // if no image
    if(!$data){
        $result = updateArticle($id,$title,$date,$content,"no_image");
    }else{
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        // CHECK IF IMAGE
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $result['status']='not_image';
        }else{
            $result = updateArticle($id,$title,$date,$content,$image);

        }
    }
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_update_article', 'update_article' );
add_action( 'wp_ajax_nopriv_update_article', 'update_article' );

// DELETE STORE BRANCH
function delete_article() {
    $id = $_POST['id'];
    $result = deleteArticle($id);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_delete_article', 'delete_article' );
add_action( 'wp_ajax_nopriv_delete_article', 'delete_article' );



// ==================SERVICE CENTER=================================
// ADD NEW SERVICE CENTER
function add_service_center() {
    $name = $_POST['name'];
    $wdf = $_POST['wdf'];
    $wdt = $_POST['wdt'];
    $whf = $_POST['whf'];
    $wht = $_POST['wht'];
    $address = $_POST['address'];
    $number = $_POST['number'];
    $email = $_POST['email'];
    $latitude = $_POST['latitude'];
    $longitude = $_POST['longitude'];
    $result = addServiceCenter($name,$wdf,$wdt,$whf,$wht,$address,$number,$email,$latitude,$longitude);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_add_service_center', 'add_service_center' );
add_action( 'wp_ajax_nopriv_add_service_center', 'add_service_center' );

// VIEW SERVICE CENTER
function view_service_center() {
    $id = $_POST['id'];
    $result = viewServiceCenter($id);
    $response = array(
                    'name' => $result['name'], 
                    'address'=> $result['address'],
                    'number'=> $result['number'],
                    'email'=> $result['email'],
                    'latitude'=> $result['latitude'],
                    'longitude'=> $result['longitude'],
                    'wdf'=> $result['wdf'],
                    'wdt'=> $result['wdt'],
                    'whf'=> $result['whf'],
                    'wht'=> $result['wht']
                    );
    wp_send_json($response);
}
add_action( 'wp_ajax_view_service_center', 'view_service_center' );
add_action( 'wp_ajax_nopriv_view_service_center', 'view_service_center' );

// UPDATE SERVICE CENTER
function update_service_center() {
    $id = $_POST['id']; 
    $name = $_POST['name'];
    $wdf = $_POST['wdf'];
    $wdt = $_POST['wdt'];
    $whf = $_POST['whf'];
    $wht = $_POST['wht'];
    $address = $_POST['address'];
    $number = $_POST['number'];
    $email = $_POST['email'];
    $latitude = $_POST['latitude'];
    $longitude = $_POST['longitude'];
    $result = updateServiceCenter($id,$name,$wdf,$wdt,$whf,$wht,$address,$number,$email,$latitude,$longitude);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_update_service_center', 'update_service_center' );
add_action( 'wp_ajax_nopriv_update_service_center', 'update_service_center' );

// DELETE SERVICE CENTER
function delete_service_center() {
    $id = $_POST['id'];
    $result = deleteServiceCenter($id);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_delete_service_center', 'delete_service_center' );
add_action( 'wp_ajax_nopriv_delete_service_center', 'delete_service_center' );

// ====================SERVICE CENTER APPOINTMENT======================
// VIEW SERVICE CENTER APPOINTMENT
function view_service_center_appointment() {
    $id = $_POST['id'];
    $result = viewServiceCenterAppointent($id);
    $response = array(
                    'name' => $result['name'], 
                    'user' => $result['user'], 
                    'email' => $result['email'], 
                    'number' => $result['number'], 
                    'status' => $result['status'], 
                    'points'=> $result['points'],
                    'problem'=> $result['problem']
                    );
    wp_send_json($response);
}
add_action( 'wp_ajax_view_service_center_appointment', 'view_service_center_appointment' );
add_action( 'wp_ajax_nopriv_view_service_center_appointment', 'view_service_center_appointment' );

function view_product_appointment(){
    $id = $_POST['id'];
    $result = viewProductAppointment($id);
    echo $result;
    die();
}
add_action( 'wp_ajax_view_product_appointment', 'view_product_appointment' );
add_action( 'wp_ajax_nopriv_view_product_appointment', 'view_product_appointment' );

// UPDATE SERVICE CENTER APPOINTMENT
function update_service_center_appointment() {
    $id = $_POST['id']; 
    $status = $_POST['status'];
    $points = $_POST['points'];
    $result = updateServiceCenterAppointment($id,$status,$points);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_update_service_center_appointment', 'update_service_center_appointment' );
add_action( 'wp_ajax_nopriv_update_service_center_appointment', 'update_service_center_appointment' );

// DELETE SERVICE CENTER APPOINTMENT
function delete_service_center_appointment() {
    $id = $_POST['id'];
    $result = deleteServiceCenterAppointent($id);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_delete_service_center_appointment', 'delete_service_center_appointment' );
add_action( 'wp_ajax_nopriv_delete_service_center_appointment', 'delete_service_center_appointment' );

// ===================PRODUCT==========================
// VIEW PRODUCT
function view_product() {
    $id = $_POST['id'];
    $result = viewProduct($id);
    $response = array(
                    'image' => $result['image'],
                    'userFname' => $result['userFname'],
                    'userLname' => $result['userLname'],
                    'store' => $result['store'],
                    'brand' => $result['brand'],
                    'branch' => $result['branch'],
                    'model' => $result['model'],
                    'transaction' => $result['transaction'],
                    'date' => $result['date'],
                    'category' => $result['category'],
                    'serial' => $result['serial'],
                    'quantity' => $result['quantity'],
                    'status' => $result['status']
                    );
    wp_send_json($response);
}
add_action( 'wp_ajax_view_product', 'view_product' );
add_action( 'wp_ajax_nopriv_view_product', 'view_product' );

// ============================REDEMPTION=======================
// VIEW REDEMPTION
function view_redemption() {
    $id = $_POST['id'];
    $result = viewRedemption($id);
    $response = array(
                    'type' => $result['type'],
                    'status' => $result['status'],
                    'points' => $result['points']
                    );
    wp_send_json($response);
}
add_action( 'wp_ajax_view_redemption', 'view_redemption' );
add_action( 'wp_ajax_nopriv_view_redemption', 'view_redemption' );

// UPDATE REDEMPTION
function update_redemption() {
    $id = $_POST['id']; 
    $type = $_POST['type'];
    $status = $_POST['status'];
    $result = updateRedemption($id,$type,$status);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_update_redemption', 'update_redemption' );
add_action( 'wp_ajax_nopriv_update_redemption', 'update_redemption' );

// DELETE REDEMPTION
function delete_redemption() {
    $id = $_POST['id'];
    $result = deleteRedemption($id);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_delete_redemption', 'delete_redemption' );
add_action( 'wp_ajax_nopriv_delete_redemption', 'delete_redemption' );


// ======================REWARD PRODUCT============
// ADD NEW REWARD PRODUCT
function add_reward_product() {
    $image = 'image';
  
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    // CHECK IF IMAGE
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        $result['status']='not_image';
    }else{
        $name = $_POST['name'];
        $points = $_POST['points'];
        $type = $_POST['type'];
        $description = $_POST['description'];
        $reward = $_POST['reward'];
        $available = $_POST['available'];
        $result = addRewardProduct($name,$points,$type,$description,$reward,$available,$image);
    }
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_add_reward_product', 'add_reward_product' );
add_action( 'wp_ajax_nopriv_add_reward_product', 'add_reward_product' );

// VIEW REWARD PRODUCT
function view_reward_product() {
    $id = $_POST['id'];
    $result = viewRewardProduct($id);
    $response = array(
                    'image' => $result['image'],
                    'name' => $result['name'],
                    'description' => $result['description'],
                    'points' => $result['points'],
                    'type' => $result['type'],
                    'brand' => $result['brand'],
                    'voucher' => $result['voucher'],
                    'available' => $result['available']
                    );
    wp_send_json($response);
}
add_action( 'wp_ajax_view_reward_product', 'view_reward_product' );
add_action( 'wp_ajax_nopriv_view_reward_product', 'view_reward_product' );

// UPDATE REWARD PRODUCT
function update_reward_product() {

    $image = 'image';
    $data = $_POST['data'];

    if(!$data){
        $id = $_POST['id'];
        $name = $_POST['name'];
        $points = $_POST['points'];
        $type = $_POST['type'];
        $description = $_POST['description'];
        $reward = $_POST['reward'];
        $available = $_POST['available'];
        $result = updateRewardProduct($id,$name,$points,$type,$description,$reward,$available,"no_image");
    }else{
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        // CHECK IF IMAGE
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
         $result['status']='not_image';
        }else{
            $id = $_POST['id'];
        $name = $_POST['name'];
        $points = $_POST['points'];
        $type = $_POST['type'];
        $description = $_POST['description'];
        $reward = $_POST['reward'];
        $available = $_POST['available'];
        $result = updateRewardProduct($id,$name,$points,$type,$description,$reward,$available,$image);
        }

    }

   $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_update_reward_product', 'update_reward_product' );
add_action( 'wp_ajax_nopriv_update_reward_product', 'update_reward_product' );


// DELETE REWARD PRODUCT
function delete_reward_product() {
    $id = $_POST['id'];
    $result = deleteRewardProduct($id);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_delete_reward_product', 'delete_reward_product' );
add_action( 'wp_ajax_nopriv_delete_reward_product', 'delete_reward_product' );


// ======================ADD SURVEY==========
// ADD NEW SURVEY
function add_survey() {
    $title = $_POST['title'];
    $points = $_POST['points'];
    // $active = $_POST['active'];
    $result = addSurvey($title,$points);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_add_survey', 'add_survey' );
add_action( 'wp_ajax_nopriv_add_survey', 'add_survey' );

// ADD NEW SURVEY QUESTION
function add_survey_question() {
    $surveyId = $_POST['surveyId'];
    $question = $_POST['question'];
    $label = $_POST['label'];
    $value = $_POST['value'];
    // $active = $_POST['active'];
    $result = addSurveyQuestion($surveyId,$question,$label,$value);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_add_survey_question', 'add_survey_question' );
add_action( 'wp_ajax_nopriv_add_survey_question', 'add_survey_question' );

// VIEW SURVEY
function view_survey() {
    $id = $_POST['id'];
    $result = viewSurvey($id);
    $response = array(
                    'title' => $result['title'],
                    'points' => $result['points'],
                    'active' => $result['active'],
                    );
    wp_send_json($response);
}
add_action( 'wp_ajax_view_survey', 'view_survey' );
add_action( 'wp_ajax_nopriv_view_survey', 'view_survey' );

// DISPLAY SURVEY PARTICIPANTS
function view_survey_participants() {
    $id = $_POST['id'];
    $participants = $_POST['participants'];
    $result = viewSurveyParticipantsTable($id,$participants);
    echo $result;
    die();
}
add_action( 'wp_ajax_view_survey_participants', 'view_survey_participants' );
add_action( 'wp_ajax_nopriv_view_survey_participants', 'view_survey_participants' );

// DISPLAY SURVEY QUESTION
function view_survey_question() {
    $id = $_POST['id'];
    $result = viewSurveyQuestionTable($id);
    echo $result;
    die();
}
add_action( 'wp_ajax_view_survey_question', 'view_survey_question' );
add_action( 'wp_ajax_nopriv_view_survey_question', 'view_survey_question' );

// VIEW SURVEY CHOICES OF SPECIFIC SURVEY
function view_survey_choices() {
    $arrayNum = $_POST['questionArray'];
    $surveyId = $_POST['surveyId'];

    $result = viewSurveyChoices($arrayNum,$surveyId);
    echo $result;
    die();

}
add_action( 'wp_ajax_view_survey_choices', 'view_survey_choices' );
add_action( 'wp_ajax_nopriv_view_survey_choices', 'view_survey_choices' );

// UPDATE QUESTION AND CHOICES OF SELECTED QUESTION
function update_survey_question_and_choices() {
    $arrayNum = $_POST['questionArray'];
    $surveyId = $_POST['surveyId'];

    $result = updateSurveyQuestionChoices($arrayNum,$surveyId);
    echo $result;
    die();

}
add_action( 'wp_ajax_update_survey_question_and_choices', 'update_survey_question_and_choices' );
add_action( 'wp_ajax_nopriv_update_survey_question_and_choices', 'update_survey_question_and_choices' );

// DISPLAY SURVEY ANSWER
function view_survey_answer() {
    $userId = $_POST['userId'];
    $surveyId = $_POST['surveyId'];
    $result = viewSurveyAnswerTable($userId,$surveyId);
    echo $result;
    die();
}
add_action( 'wp_ajax_view_survey_answer', 'view_survey_answer' );
add_action( 'wp_ajax_nopriv_view_survey_answer', 'view_survey_answer' );

// UPDATE SURVEY
function update_survey() {
    $id = $_POST['id'];
    $title = $_POST['title'];
    $points = $_POST['points'];
    $active = $_POST['active'];
    $result = updateSurvey($id,$title,$points,$active);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_update_survey', 'update_survey' );
add_action( 'wp_ajax_nopriv_update_survey', 'update_survey' );


// SAVE SURVEY QUESTION
function save_survey_question_and_choices() {
    $surveyId = $_POST['surveyId'];
    $questionIndex = $_POST['questionIndex'];
    $question = $_POST['question'];
    $label = $_POST['label'];
    $value = $_POST['value'];
    // $active = $_POST['active'];
    $result = saveSurveyQuestionChoices($surveyId,$questionIndex,$question,$label,$value);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_save_survey_question_and_choices', 'save_survey_question_and_choices' );
add_action( 'wp_ajax_nopriv_save_survey_question_and_choices', 'save_survey_question_and_choices' );


// DELETE A QUESTION IN A SURVEY
function delete_survey_question() {
    $surveyId = $_POST['surveyId'];
    $questionIndex = $_POST['questionIndex'];
    $result = deleteSurveyQuestionChoices($surveyId,$questionIndex);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_delete_survey_question', 'delete_survey_question' );
add_action( 'wp_ajax_nopriv_delete_survey_question', 'delete_survey_question' );

// DELETE SURVEY
function delete_survey() {
    $id = $_POST['id'];
    $result = deleteSurvey($id);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_delete_survey', 'delete_survey' );
add_action( 'wp_ajax_nopriv_delete_survey', 'delete_survey' );

// ======================ADD SETTINGS==========
// ADD NEW SETTINGS
// function add_settings() {
//     $terms = $_POST['terms'];
//     $policy = $_POST['policy'];
//     $warranty = $_POST['warranty'];
//     $result = addSettings($terms,$policy,$warranty);
//     $response = array('message' => $result['status']);
//     wp_send_json($response);
// }
// add_action( 'wp_ajax_add_settings', 'add_settings' );
// add_action( 'wp_ajax_nopriv_add_settings', 'add_settings' );

// VIEW SETTINGS
function view_settings() {
    $id = $_POST['id'];
    $result = viewSettings($id);
    $response = array(
                    'version' => $result['version'],
                    'appstore' => $result['appstore'],
                    'googleplay' => $result['googleplay'],
                    'about' => $result['about'],
                    'terms' => $result['terms'],
                    'policy' => $result['policy'],
                    'warranty' => $result['warranty'],
                    'number' => $result['number']
                    );
    wp_send_json($response);
}
add_action( 'wp_ajax_view_settings', 'view_settings' );
add_action( 'wp_ajax_nopriv_view_settings', 'view_settings' );

// UPDATE SETTINGS
function update_content_settings() {
    $id = $_POST['id'];
    $content = $_POST['content'];
    $field = $_POST['field'];
    $result = updateContentSettings($id,$content,$field);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_update_content_settings', 'update_content_settings' );
add_action( 'wp_ajax_nopriv_update_content_settings', 'update_content_settings' );

// DELETE SETTINGS
function delete_settings() {
    $id = $_POST['id'];
    $result = deleteSettings($id);
    $response = array('message' => $result['status']);
    wp_send_json($response);
}
add_action( 'wp_ajax_delete_settings', 'delete_settings' );
add_action( 'wp_ajax_nopriv_delete_settings', 'delete_settings' );



// SAVE IMAGE TO WORDPRESS
function add_image() {
    $image = 'image';
  
    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attachment_id = media_handle_upload( $image, 55 );
    if ( is_wp_error( $attachment_id ) ) {
        $result='error';
    } else {
        $image_attributes = wp_get_attachment_image_src( $attachment_id ,'full' );
        $result= $image_attributes[0];//GET THE IMAGE URL/SRC
        // $image_attributes[1];//GET THE IMAGE WIDTH
        // $image_attributes[2];//GET THE IMAGE HEIGHT
    }
    $response = array('message' => $result);
    wp_send_json($response);
}
add_action( 'wp_ajax_add_image', 'add_image' );
add_action( 'wp_ajax_nopriv_add_image', 'add_image' );

// $target_file = $target_dir . basename($_FILES["add_brand_product_image"]["name"]);
    // // CHECK IF IMAGE
    // $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    // && $imageFileType != "gif" ) {
 //     echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    // }

